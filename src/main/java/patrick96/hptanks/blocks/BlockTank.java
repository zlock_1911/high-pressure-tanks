/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.blocks;

import cpw.mods.fml.common.network.internal.FMLNetworkHandler;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import patrick96.hptanks.HPTanks;
import patrick96.hptanks.client.renderer.block.TankRenderer;
import patrick96.hptanks.lib.BlockInfo;
import patrick96.hptanks.tile.TileEntityTank;
import patrick96.hptanks.util.Coords;

public class BlockTank extends BlockHPT {
    
    public BlockTank() {
        super(Material.iron, BlockInfo.TANK_UNLOCALIZED_NAME);
        setHardness(5F);
        setResistance(10.0F);
        setStepSound(soundTypeMetal);
        setBlockTextureName(BlockInfo.TANK_TEXTURE);
        setHarvestLevel("pickaxe", 2);
    }
    
    @Override
    public boolean hasTileEntity(int metadata) {
        return true;
    }
    
    @Override
    public TileEntity createNewTileEntity(World world, int meta) {
        return new TileEntityTank();
    }
    
    @Override
    public boolean isOpaqueCube() {
        return false;
    }
    
    @Override
    public boolean renderAsNormalBlock() {
        return false;
    }
    
    @Override
    public int getRenderType() {
        return TankRenderer.renderId;
    }
    
    @Override
    public int getRenderBlockPass() {
        return 1;
    }
    
    @Override
    @SideOnly(Side.CLIENT)
    public boolean shouldSideBeRendered(IBlockAccess par1IBlockAccess, int par2, int par3, int par4, int par5) {
        return par1IBlockAccess.getBlock(par2, par3, par4) != this && super.shouldSideBeRendered(par1IBlockAccess, par2, par3, par4, par5);
    }
    
    @Override
    public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int par6, float par7, float par8, float par9) {
        if(super.onBlockActivated(world, x, y, z, player, par6, par7, par8, par9)) {
            return true;
        }
        
        if (world.isRemote) {
            return true;
        }
        
        TileEntity tile = world.getTileEntity(x, y, z);
        if (tile != null && tile instanceof TileEntityTank) {
            FMLNetworkHandler.openGui(player, HPTanks.instance, 1, world, x, y, z);
            return true;
        }
        
        return false;
    }
    
    @Override
    public int getLightValue(IBlockAccess world, int x, int y, int z) {
        TileEntity tile = world.getTileEntity(x, y, z);
        if (tile instanceof TileEntityTank) {
            return ((TileEntityTank) tile).getLightValue();
        }
        return 0;
    }
    
    @Override
    public void breakBlock(World par1World, int x, int y, int z, Block block, int meta) {
        Coords coords = new Coords(x, y, z);
        TileEntity tile = coords.getTileEntity(par1World);
        if (tile instanceof TileEntityTank) {
            ((TileEntityTank) tile).onBlockBroken();
        }
        
        super.breakBlock(par1World, x, y, z, block, meta);
    }
    
    @Override
    public boolean hasComparatorInputOverride() {
        return true;
    }
}
