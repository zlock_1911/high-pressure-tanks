/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */

package patrick96.hptanks.plugins.nei;

import codechicken.lib.gui.GuiDraw;
import codechicken.nei.NEIClientConfig;
import codechicken.nei.recipe.GuiRecipe;
import codechicken.nei.recipe.TemplateRecipeHandler;
import cpw.mods.fml.relauncher.ReflectionHelper;
import net.minecraft.client.gui.inventory.GuiContainer;
import org.lwjgl.opengl.GL11;

import java.awt.*;
import java.lang.reflect.Field;

import static codechicken.lib.gui.GuiDraw.*;

// TODO add the question mark to the recipes with getOverlayIdentifier
public abstract class BaseRecipeHandler extends TemplateRecipeHandler {
    protected FakeGui fakeGui = new FakeGui();

    public BaseRecipeHandler() {}

    @Override
    public boolean keyTyped(GuiRecipe gui, char keyChar, int keyCode, int recipe) {
        if (keyCode == NEIClientConfig.getKeyBinding("gui.recipe") && transferRects(gui, recipe, false)) {
            return true;
        } else if (keyCode == NEIClientConfig.getKeyBinding("gui.usage") && transferRects(gui, recipe, true)) {
            return true;
        }

        return super.keyTyped(gui, keyChar, keyCode, recipe);
    }

    @Override
    public boolean mouseClicked(GuiRecipe gui, int button, int recipe) {

        if (button == 0 && transferRects(gui, recipe, false)) {
            return true;
        } else if (button == 1 && transferRects(gui, recipe, true)) {
            return true;
        }

        return super.mouseClicked(gui, button, recipe);
    }

    public abstract boolean transferRects(GuiRecipe gui, int recipe, boolean usage);

    /**
     * Fetches the absolute position of the top left corner of the gui
     * Basically just the guiLeft and guiTop values
     * @param gui The gui of which the position should be accessed
     * @return the Position
     */
    public Point getGuiPosition(GuiRecipe gui) {
        Field guiLeft = ReflectionHelper.findField(GuiContainer.class, "guiLeft", "field_147003_i");
        Field guiTop = ReflectionHelper.findField(GuiContainer.class, "guiTop", "field_147009_r");
        try {
            return new Point(guiLeft.getInt(gui), guiTop.getInt(gui));
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        return new Point();
    }

    public Point getRelativeMousePosition(GuiRecipe gui) {
        Point pos = GuiDraw.getMousePosition();
        Point guiPos = getGuiPosition(gui);
        pos.translate(-guiPos.x, -guiPos.y);
        return pos;
    }

    public int getOffsetX() {
        return 5;
    }

    public int getOffsetY() {
        return 3;
    }

    public int getGuiHeight() {
        return 68;
    }

    public int getGuiWidth() {
        return 176;
    }

    public Rectangle getOffsetRectangle(int x, int y, int width, int height) {
        return new Rectangle(x - getOffsetX(), y - getOffsetY(), width, height);
    }

    public Point getOffsetPoint(int x, int y) {
        return new Point(x - getOffsetX(), y - getOffsetX());
    }

    @Override
    public int recipiesPerPage() {
        return 2;
    }

    @Override
    public void drawBackground(int recipe) {
        GL11.glColor4f(1, 1, 1, 1);
        changeTexture(getGuiTexture());
        drawTexturedModalRect(0, 0, getOffsetX(), getOffsetY(), getGuiWidth() - 2 * getOffsetX(), getGuiHeight() - getOffsetY());
    }

    public CachedRecipe getCachedRecipe(int recipe) {
        return arecipes.get(recipe);
    }
}
