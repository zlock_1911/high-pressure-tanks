/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.client.renderer.models;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;

import java.util.ArrayList;

public class ModelFluidPump extends ModelBase {
    
    ArrayList<ModelRenderer> parts = new ArrayList<ModelRenderer>();
    ModelRenderer pumpHead;
    private float rot = 0;
    
    public ModelFluidPump() {
        /*textureWidth = 64;
        textureHeight = 32;
        
        ModelRenderer connector = new ModelRenderer(this, 0, 0);
        connector.addBox(-8, -4, -4, 
                        2, 8, 8);
        connector.addBox(6, -4, -4,
                        2, 8, 8);
        
        connector.setRotationPoint(8, 8, 8);
        
        parts.add(connector);
        
        pipe = new ModelRenderer(this, 0, 16);
        pipe.addBox(-6, -3, -3, 
                    12, 6, 6);
        pipe.setRotationPoint(8, 8, 8);
        
        parts.add(pipe);*/
    
    
        /*textureWidth = 64;
        textureHeight = 128;
        
        ModelRenderer pumpHead = new ModelRenderer(this, 0, 0);
        pumpHead.addBox(-8, -8, -8, 
                        16, 1, 16);
        
        pumpHead.setRotationPoint(8, 8, 8);
        
        ModelRenderer pumpHead2 = new ModelRenderer(this, 0, 17);
        pumpHead2.addBox(-7, -7, -7, 
                        14, 1, 14);
        
        ModelRenderer pumpHead3 = new ModelRenderer(this, 0, 32);
        pumpHead3.addBox(-6, -6, -6, 
                        12, 1, 12);
        
        ModelRenderer pumpHead4 = new ModelRenderer(this, 0, 45);
        pumpHead4.addBox(-5, -5, -5, 
                        10, 1, 10);
        
        ModelRenderer pumpHead5 = new ModelRenderer(this, 0, 56);
        pumpHead5.addBox(-4, -4, -4, 
                        8, 1, 8);
        
        ModelRenderer pumpHead6 = new ModelRenderer(this, 0, 65);
        pumpHead6.addBox(-3, -3, -3, 
                        6, 3, 6);

        pumpHead5.addChild(pumpHead6);
        pumpHead4.addChild(pumpHead5);
        pumpHead3.addChild(pumpHead4);
        pumpHead2.addChild(pumpHead3);
        pumpHead.addChild(pumpHead2);
        
        parts.add(pumpHead);
        
        
        ModelRenderer connector = new ModelRenderer(this, 0, 74);
        connector.addBox(-8, -4, -4, 
                        2, 8, 8);
        connector.addBox(6, -4, -4,
                        2, 8, 8);
        
        connector.setRotationPoint(8, 8, 8);
        
        parts.add(connector);
        
        pipe = new ModelRenderer(this, 0, 90);
        pipe.addBox(-6, -3, -3, 
                    12, 6, 6);
        pipe.setRotationPoint(8, 8, 8);
        
        parts.add(pipe);*/
        
        textureWidth = 64;
        textureHeight = 64;
        
        pumpHead = new ModelRenderer(this, 0, 0);
        pumpHead.addBox(-8, 3, -8, 
                        16, 5, 16);
        
        pumpHead.setRotationPoint(8, 8, 8);
                        
        ModelRenderer center = new ModelRenderer(this, 0, 21);
        center.addBox(-3, -3, -3, 
                    6, 6, 6);
        center.setRotationPoint(8, 8, 8);
        
        /*ModelRenderer connector = new ModelRenderer(this, 24, 21);
        connector.addBox(-4, -8, -4, 
                        8, 2, 8);
        connector.setRotationPoint(8, 8, 8);*/
        
        ModelRenderer pipe = new ModelRenderer(this, 0, 33);
        pipe.addBox(-3, -8, -3, 
                        6, 5, 6);
        pipe.setRotationPoint(8, 8, 8);
        
        //parts.add(pumpHead);
        parts.add(center);
        //parts.add(connector);
        parts.add(pipe);
    }
    
    public void render(int type, float mult) {
        for(ModelRenderer part : parts) {
            part.render(mult);
            
            
            /*part.rotateAngleY = 0;
            part.rotateAngleZ = 0;
            
            switch(type) {
                case 0:
                    part.render(mult);
                    break;
                    
                case 1:
                    part.rotateAngleY = (float) Math.PI / 2;
                    part.render(mult);
                    break;
                    
                case 2:
                    part.rotateAngleZ = (float) Math.PI / 2;
                    part.render(mult);
                    break;
            }*/
        }
        
        pumpHead.rotateAngleX = 0;
        pumpHead.rotateAngleY = 0;
        pumpHead.rotateAngleZ = 0;
        
        switch(type) {
            case 0:
                pumpHead.rotateAngleX = (float) Math.PI;
                break;
                
            case 1:
                break;
                
            case 2:
                pumpHead.rotateAngleX = (float) Math.PI / 2;
                pumpHead.rotateAngleY = (float) Math.PI;
                break;
                
            case 3:
                pumpHead.rotateAngleX = (float) Math.PI / 2;
                break;
                
            case 4:
                pumpHead.rotateAngleX = (float) Math.PI / 2;
                pumpHead.rotateAngleY = (float) Math.PI * 3 / 2;
                break;
                
            case 5:
                pumpHead.rotateAngleX = (float) Math.PI / 2;
                pumpHead.rotateAngleY = (float) Math.PI / 2;
                break;
        }
        
        pumpHead.render(mult);
    }
}
