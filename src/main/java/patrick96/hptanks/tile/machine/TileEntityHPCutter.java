/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */

package patrick96.hptanks.tile.machine;

import net.minecraft.item.ItemStack;
import patrick96.hptanks.HPTanks;
import patrick96.hptanks.blocks.BlockMachine;
import patrick96.hptanks.core.inventory.slot.SlotInputRestricted;
import patrick96.hptanks.core.inventory.slot.SlotOutput;
import patrick96.hptanks.core.inventory.slot.SlotRestricted;
import patrick96.hptanks.lib.GuiInfo;
import patrick96.hptanks.recipe.HPTRegistry;
import patrick96.hptanks.recipe.HPCutterRecipeInfo;
import patrick96.hptanks.recipe.MachineRecipeInfo;
import patrick96.hptanks.upgrades.UpgradeType;
import patrick96.hptanks.util.Utils;

import java.util.List;

// The HP here stands for High Precision and not High Pressure ;)
public class TileEntityHPCutter extends TileEntityMachine {

    public static final GuiInfo guiInfo = new GuiInfo(GuiInfo.HP_CUTTER_ID, "HighPrecisionCutter", true);

    protected ItemStack cutterLastCheck = null;

    public TileEntityHPCutter() {
        super(BlockMachine.MachineType.HP_CUTTER);

        inventoryHandler.addSlot(new SlotInputRestricted(this, inventoryHandler.getNextAvailableId(), 33, 46));
        inventoryHandler.addSlot(new SlotOutput(this, inventoryHandler.getNextAvailableId(), 93, 47));

        // Cutter Slot
        inventoryHandler.addSlot(new SlotRestricted(this, inventoryHandler.getNextAvailableId(), 33, 25));
    }

    @Override
    public void markDirty() {
        super.markDirty();

        if(!Utils.areItemStacksEqualIgnoreStackSize(cutterLastCheck, getCutterStack())) {
            /**
             * Since the getRecipeInfo handles the cutterConfig check we just need to call newInput here to update recipeInfo
             */
            newInput();
            cutterLastCheck = getCutterStack();
        }
    }

    @Override
    public float getEnergyPerTick() {
        HPCutterRecipeInfo.CutterConfiguration cutter = getCutterConfig();
        return cutter != null? super.getEnergyPerTick() * cutter.baseEnergyCoefficient : super.getEnergyPerTick();
    }

    @Override
    public int getProcessTime() {
        HPCutterRecipeInfo.CutterConfiguration config = getCutterConfig();
        return Math.round(super.getProcessTime() * (config != null? config.baseTimeCoefficient : 1));
    }

    @Override
    public boolean canStackBeInput(ItemStack stack) {
        return HPTRegistry.getHpCutterRecipeInfo(stack, this) != null;
    }

    public boolean canStackBeCutter(ItemStack stack) {
        return !Utils.isItemStackEmpty(stack) && HPTRegistry.isCutter(stack);
    }

    public ItemStack getCutterStack() {
        return getStackInSlot(getCutterSlotNumber());
    }

    public HPCutterRecipeInfo.CutterConfiguration getCutterConfig() {
        return HPTRegistry.getCutter(getCutterStack());
    }

    public int getCutterSlotNumber() {
        return 2;
    }

    @Override
    protected MachineRecipeInfo getRecipeInfo() {
        HPCutterRecipeInfo info = HPTRegistry.getHpCutterRecipeInfo(getInputStack(), this);
        HPCutterRecipeInfo.CutterConfiguration config = getCutterConfig();
        if(info != null && (config == null || config.getTier() < info.minimalCutterTier)) {
            return null;
        }
        return info;
    }

    @Override
    public void processFinished() {
        HPCutterRecipeInfo info = (HPCutterRecipeInfo) currentRecipe;
        ItemStack result = info.getResult();

        ItemStack cutter = getCutterStack();
        HPCutterRecipeInfo.CutterConfiguration cutterConfiguration = HPTRegistry.getCutter(cutter);

        if(!Utils.isItemStackEmpty(cutter) && cutterConfiguration != null) {
            if(cutterConfiguration.getDamage() == HPCutterRecipeInfo.CutterDamage.DAMAGE) {
                if(cutter.attemptDamageItem(1, HPTanks.RANDOM)) {
                    decrStackSize(getCutterSlotNumber(), 1);
                }
            }
            else if(cutterConfiguration.getDamage() == HPCutterRecipeInfo.CutterDamage.DESTROY) {
                decrStackSize(getCutterSlotNumber(), 1);
            }
        }

        decrStackSize(getInputSlotNumber(), 1);
        transferStackIntoOutput(result);

        ejectItem();
    }

    @Override
    public boolean isItemValidForSlot(int i, ItemStack itemStack) {
        return super.isItemValidForSlot(i, itemStack) || (i == getCutterSlotNumber() && canStackBeCutter(itemStack));
    }

    @Override
    public List<UpgradeType> getSupportedUpgrades() {
        List<UpgradeType> list = super.getSupportedUpgrades();
        list.add(UpgradeType.IC2);
        return list;
    }
}
