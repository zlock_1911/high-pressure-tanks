/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */

package patrick96.hptanks.plugins.nei;

import codechicken.nei.PositionedStack;
import codechicken.nei.guihook.GuiContainerManager;
import codechicken.nei.recipe.GuiCraftingRecipe;
import codechicken.nei.recipe.GuiRecipe;
import codechicken.nei.recipe.GuiUsageRecipe;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fluids.FluidRegistry;
import net.minecraftforge.fluids.FluidStack;
import org.lwjgl.opengl.GL11;
import patrick96.hptanks.blocks.BlockMachine;
import patrick96.hptanks.client.gui.GuiCVDFurnace;
import patrick96.hptanks.client.gui.rect.GuiRectangle;
import patrick96.hptanks.client.gui.rect.draw.GuiRectDrawType;
import patrick96.hptanks.client.gui.rect.draw.GuiRectTextured;
import patrick96.hptanks.client.util.GuiUtils;
import patrick96.hptanks.init.ModItems;
import patrick96.hptanks.recipe.CVDFurnaceRecipeInfo;
import patrick96.hptanks.recipe.HPTRegistry;
import patrick96.hptanks.recipe.MachineRecipeInfo;
import patrick96.hptanks.util.StringUtils;
import patrick96.hptanks.util.Utils;

import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CVDFurnaceRecipeHandler extends MachineRecipeHandler {

    @Override
    public void loadUsageRecipes(String inputId, Object... ingredients) {
        if(inputId.equals("liquid") && ingredients != null && ingredients.length > 0
                   && ingredients[0] instanceof FluidStack && ((FluidStack) ingredients[0]).getFluid() == FluidRegistry.WATER) {
            for(MachineRecipeInfo machineRecipeInfo : getRecipeList()) {
                CVDFurnaceRecipeInfo info = (CVDFurnaceRecipeInfo) machineRecipeInfo;
                if(info.getWaterUsed() > 0) {
                    addMachineRecipe(getCachedMachineRecipe(machineRecipeInfo));
                }
            }
        } else if(inputId.equals("catalyst") && ingredients != null && ingredients.length > 0 && ingredients[0] instanceof CVDFurnaceRecipeInfo.Catalyst) {
            for(MachineRecipeInfo machineRecipeInfo : getRecipeList()) {
                CVDFurnaceRecipeInfo info = (CVDFurnaceRecipeInfo) machineRecipeInfo;
                CVDFurnaceRecipeInfo.Catalyst catalyst = (CVDFurnaceRecipeInfo.Catalyst) ingredients[0];
                if(info.hasCatalyst(catalyst)) {
                    addMachineRecipe(getCachedMachineRecipe(machineRecipeInfo));
                }
            }
        } else {
            super.loadUsageRecipes(inputId, ingredients);
        }
    }

    @Override
    public void loadUsageRecipes(ItemStack ingredient) {
        super.loadUsageRecipes(ingredient);
        if(Utils.areItemStacksEqualIgnoreStackSize(ingredient, ModItems.ItemPlain.Wafer.getStack())) {
            for(MachineRecipeInfo info : getRecipeList()) {
                CachedMachineRecipe recipe = getCachedMachineRecipe(info);
                if(!arecipes.contains(recipe)) {
                    addMachineRecipe(recipe);
                }
            }
        }
    }

    @Override
    public void loadTransferRects() {
        /*
         This Transfer Rect is shown in both the NEI Crafting GUI and the normal GUI,
         though the NEI GUI doesn't show the catalyst part of the GUI so there is floating invisible transfer rect with tooltip to the right
         */
        transferRects.add(new RecipeTransferRect(getOffsetRectangle(202, 60, 18, 18), "catalyst"));
        super.loadTransferRects();
    }

    @Override
    public List<String> handleTooltip(GuiRecipe gui, List<String> currenttip, int recipe) {
        if(GuiContainerManager.shouldShowTooltip(gui) && currenttip.size() == 0) {
            CachedCVDFurnaceRecipe cachedRecipe = getCachedRecipe(recipe);
            Point pos = getRelativeMousePosition(gui);
            Point offset = gui.getRecipePosition(recipe);
            for(Map.Entry<CVDFurnaceRecipeInfo.CatalystConfiguration, Rectangle> catalystEntry : cachedRecipe.catalystRectangles.entrySet()) {
                Rectangle catalystRect = catalystEntry.getValue().getBounds();
                catalystRect.translate(offset.x, offset.y);
                if(catalystRect.contains(pos)) {
                    CVDFurnaceRecipeInfo.CatalystConfiguration config = catalystEntry.getKey();
                    currenttip.add(config.catalyst.localize() + (config.optional? " (" + StringUtils.localize("hpt.catalyst.optional") + ")" : ""));
                    currenttip.add(StringUtils.localize("hpt.catalyst.used") + ": " + StringUtils.prettyPrintNumber(config.used, 2, false));
                }
            }
        }

        return super.handleTooltip(gui, currenttip, recipe);
    }

    @Override
    public boolean transferRects(GuiRecipe gui, int recipe, boolean usage) {
        return super.transferRects(gui, recipe, usage) || transferRectCatalyst(gui, recipe, usage);
    }

    public boolean transferRectCatalyst(GuiRecipe gui, int recipe, boolean usage) {
        CachedCVDFurnaceRecipe cachedRecipe = getCachedRecipe(recipe);

        for(Map.Entry<CVDFurnaceRecipeInfo.CatalystConfiguration, Rectangle> catalyst : cachedRecipe.catalystRectangles.entrySet()) {
            Rectangle catalystRect = catalyst.getValue().getBounds();

            Point pos = getRelativeMousePosition(gui);
            Point offset = gui.getRecipePosition(recipe);
            catalystRect.translate(offset.x, offset.y);
            if(catalystRect.contains(pos)) {
                Object[] ingreds = new Object[] {catalyst.getKey().catalyst};
                if(usage) {
                    GuiUsageRecipe.openRecipeGui("catalyst", ingreds);
                } else {
                    GuiCraftingRecipe.openRecipeGui("catalyst", ingreds);
                }
                return true;
            }
        }
        return false;
    }

    @Override
    public Class<? extends GuiContainer> getGuiClass() {
        return GuiCVDFurnace.class;
    }

    @Override
    public BlockMachine.MachineType getMachineType() {
        return BlockMachine.MachineType.CVD_FURNACE;
    }

    @Override
    public Point getInputPosition() {
        return new Point(33, 46);
    }

    @Override
    public Point getOutputPosition() {
        return new Point(93, 47);
    }

    @Override
    public Rectangle getProgressArrowRectangle() {
        return getOffsetRectangle(57, 47, 22, 16);
    }

    public Point getFirstCatalystCorner() {
        return new Point(28, 68);
    }

    @Override
    public Rectangle getFluidRectangle() {
        return getOffsetRectangle(152, 8, 16, 59);
    }

    @Override
    public FluidStack getFluidStack(int recipe) {
        return new FluidStack(FluidRegistry.WATER, 1);
    }

    @Override
    public String getAdditionalFluidInformation(int recipe) {
        return "Used: " + getRecipeInfo(recipe).getWaterUsed() + "mB";
    }

    @Override
    public List<? extends MachineRecipeInfo> getRecipeList() {
        return HPTRegistry.cvdFurnaceRecipes;
    }

    @Override
    public void drawExtras(int recipe) {
        super.drawExtras(recipe);

        CachedCVDFurnaceRecipe cachedRecipe = getCachedRecipe(recipe);
        for(Map.Entry<CVDFurnaceRecipeInfo.CatalystConfiguration, Rectangle> catalyst : cachedRecipe.catalystRectangles.entrySet()) {
            CVDFurnaceRecipeInfo.Catalyst cat = catalyst.getKey().catalyst;
            Rectangle catalystRect = catalyst.getValue();
            fakeGui.drawString(cat.getShortName(), fakeGui.getLeft() + catalystRect.x + 2, fakeGui.getTop() + catalystRect.y + 2, 0xFF202020);
            fakeGui.drawFrame(catalystRect.x + 2, catalystRect.y + 18, 26, 10, 0xFF202020);
            GuiRectDrawType barType = new GuiRectTextured(82, 0, GuiUtils.getGuiTexture("Icons"));
            GL11.glColor4f(1, 1, 1, 1);
            new GuiRectangle(fakeGui, barType, catalystRect.x + 3, catalystRect.y + 19, (int) ((1F - cycleticks % 40 / 40F) * 24), 8).draw();
        }
    }

    @Override
    public int recipiesPerPage() {
        return 1;
    }

    @Override
    public CachedMachineRecipe getCachedMachineRecipe(MachineRecipeInfo info) {
        return new CachedCVDFurnaceRecipe(info);
    }

    @Override
    public CVDFurnaceRecipeInfo getRecipeInfo(int recipe) {
        return (CVDFurnaceRecipeInfo) super.getRecipeInfo(recipe);
    }

    @Override
    public CachedCVDFurnaceRecipe getCachedRecipe(int recipe) {
        return (CachedCVDFurnaceRecipe) super.getCachedRecipe(recipe);
    }

    public class CachedCVDFurnaceRecipe extends CachedMachineRecipe {

        public final Map<CVDFurnaceRecipeInfo.CatalystConfiguration, Rectangle> catalystRectangles = new HashMap<CVDFurnaceRecipeInfo.CatalystConfiguration, Rectangle>();

        public CachedCVDFurnaceRecipe(MachineRecipeInfo info) {
            super(info);
            CVDFurnaceRecipeInfo cvdFurnaceInfo = (CVDFurnaceRecipeInfo) info;
            Point firstCorner = getFirstCatalystCorner();
            int counter = 0;
            for(CVDFurnaceRecipeInfo.CatalystConfiguration configuration : cvdFurnaceInfo.getCatalysts()) {
                catalystRectangles.put(configuration, getOffsetRectangle(firstCorner.x + counter * 30, firstCorner.y, 30, 30));
                counter++;
            }
        }

        @Override
        public List<PositionedStack> getIngredients() {
            ArrayList<PositionedStack> list = new ArrayList<PositionedStack>();
            list.addAll(super.getIngredients());
            list.add(new PositionedStack(ModItems.ItemPlain.Wafer.getStack(), 60 - getOffsetX(), 25 - getOffsetY()));

            return list;
        }
    }
}
