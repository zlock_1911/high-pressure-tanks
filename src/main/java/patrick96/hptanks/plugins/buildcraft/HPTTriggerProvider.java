/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.plugins.buildcraft;

import buildcraft.api.statements.IStatementContainer;
import buildcraft.api.statements.ITriggerExternal;
import buildcraft.api.statements.ITriggerInternal;
import buildcraft.api.statements.ITriggerProvider;
import buildcraft.transport.TileGenericPipe;
import net.minecraft.tileentity.TileEntity;
import net.minecraftforge.common.util.ForgeDirection;
import net.minecraftforge.fluids.IFluidHandler;
import patrick96.hptanks.core.power.IPowerTile;
import patrick96.hptanks.tile.hpt.TileEntityTankComponent;

import java.util.Collection;
import java.util.LinkedList;

public class HPTTriggerProvider implements ITriggerProvider {
    
    @Override
    public Collection<ITriggerInternal> getInternalTriggers(IStatementContainer container) {
        return null;
    }

    @Override
    public Collection<ITriggerExternal> getExternalTriggers(ForgeDirection side, TileEntity tile) {
        LinkedList<ITriggerExternal> triggers = new LinkedList<ITriggerExternal>();

        if(tile != null && !(tile instanceof TileGenericPipe)) {
            if(tile instanceof IFluidHandler || (tile instanceof TileEntityTankComponent && ((TileEntityTankComponent) tile).isPartOfMultiBlock())) {
                triggers.add(ModuleBuildCraft.statements.triggerFluidBelow25);
                triggers.add(ModuleBuildCraft.statements.triggerFluidBelow50);
                triggers.add(ModuleBuildCraft.statements.triggerFluidBelow75);
            }

            if(tile instanceof IPowerTile) {
                triggers.add(ModuleBuildCraft.statements.triggerCapacitorEmpty);
                triggers.add(ModuleBuildCraft.statements.triggerCapacitorContains);
                triggers.add(ModuleBuildCraft.statements.triggerCapacitorSpace);
                triggers.add(ModuleBuildCraft.statements.triggerCapacitorFull);
                triggers.add(ModuleBuildCraft.statements.triggerCapacitorBelow25);
                triggers.add(ModuleBuildCraft.statements.triggerCapacitorBelow50);
                triggers.add(ModuleBuildCraft.statements.triggerCapacitorBelow75);
            }
        }

        return triggers;
    }
}
