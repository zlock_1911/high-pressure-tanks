/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */

package patrick96.hptanks.plugins.nei;

import net.minecraft.client.Minecraft;
import patrick96.hptanks.client.gui.HPTGui;
import patrick96.hptanks.client.gui.IGuiTile;
import patrick96.hptanks.client.gui.tabs.GuiTab;
import patrick96.hptanks.lib.GuiInfo;
import patrick96.hptanks.tile.TileEntityHPTBase;
import patrick96.hptanks.util.Coords;

import java.util.Map;

public class FakeGui extends HPTGui {

    public FakeGui() {
        super(null, new FakeTE());
        // When setting the display size to the gui screen size the guiLeft and guiTop will be 0
        setWorldAndResolution(Minecraft.getMinecraft(), xSize, ySize);
    }

    @Override
    public void setWorldAndResolution(Minecraft mc, int width, int height) {
        this.mc = mc;
        this.fontRendererObj = mc.fontRenderer;
        this.width = width;
        this.height = height;
        // Don't call initGui, since it changes the inventory of the player and causes crashes in NEI
        this.guiLeft = (this.width - this.xSize) / 2;
        this.guiTop = (this.height - this.ySize) / 2;
    }

    public static class FakeTE extends TileEntityHPTBase implements IGuiTile {

        public GuiInfo guiInfo = new GuiInfo(-1, "", false);

        @Override
        public Coords getCoords() {
            return new Coords(0, 0, 0);
        }

        @Override
        public GuiInfo getGuiInfo() {
            return guiInfo;
        }

        @Override
        public void handleGuiButtonClick(short buttonId) {}

        @Override
        public Map<GuiTab.TabSide, Integer> getOpenTabs() {
            return null;
        }

        @Override
        public void setOpenTabs(Map<GuiTab.TabSide, Integer> tabs) {}
    }
}