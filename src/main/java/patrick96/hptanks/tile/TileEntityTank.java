/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.tile;

import io.netty.buffer.ByteBuf;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.common.util.ForgeDirection;
import net.minecraftforge.fluids.*;
import patrick96.hptanks.client.gui.ButtonIds;
import patrick96.hptanks.client.gui.IGuiTile;
import patrick96.hptanks.client.gui.tabs.GuiTab.TabSide;
import patrick96.hptanks.core.fluids.*;
import patrick96.hptanks.core.inventory.IHPTInventory;
import patrick96.hptanks.core.inventory.slot.SlotInputRestricted;
import patrick96.hptanks.core.network.packets.PacketTileUpdate;
import patrick96.hptanks.lib.GuiInfo;
import patrick96.hptanks.lib.Reference;
import patrick96.hptanks.upgrades.UpgradeType;
import patrick96.hptanks.util.Utils;

import java.io.IOException;
import java.util.List;
import java.util.Map;
/**
 * TODO change this tank to a portable tank
 * Doesn't use energy
 * Tier Upgrades
 * Pressure Upgrades
 */
public class TileEntityTank extends TileEntityPowered implements IFluidTile, IBucketable, IBucketableInventory, IGuiTile {

    public static final GuiInfo guiInfo = new GuiInfo(GuiInfo.MINI_TANK_ID, "Control", true);

    public boolean doEject = false;
    
    public TileEntityTank() {
        super(Reference.BASIC_ENERGY_STORAGE_CAPACITY);

        tankManager = new TankManager(new FluidTank(getTankCapacity()));

        inventoryHandler.setName("tank");
        inventoryHandler.addSlot(new SlotInputRestricted(this, inventoryHandler.getNextAvailableId(), 0, 0));
        inventoryHandler.addSlot(new SlotInputRestricted(this, inventoryHandler.getNextAvailableId(), 0, 0));
    }
    
    @Override
    public void updateEntity() {
        super.updateEntity();
        
        if (worldObj.isRemote) {
            return;
        }
        
        if (doEject) {
            FluidUtils.ejectFluid(this, 100);
        }
        
        updateInventory();
    }
    
    @Override
    protected boolean shouldRequestInfoFromServer() {
        return true;
    }

    @Override
    public GuiInfo getGuiInfo() {
        return guiInfo;
    }

    @Override
    public boolean canExtractItem(int slot, ItemStack itemstack, int side) {
        return false;
    }
    
    @Override
    public boolean canInsertItem(int i, ItemStack itemstack, int j) {
        return false;
    }
    
    @Override
    public void handleGuiButtonClick(short buttonId) {
        if (buttonId == ButtonIds.BUTTON_AUTO_EJECT) {
            doEject = !doEject;
        }
    }
    
    public void updateInventory() {
        FluidUtils.bucketInventory(this);
    }

    @Override
    public boolean canInsertIntoFluidContainerOutputSlot(ItemStack item) {
        ItemStack stack = getStackInSlot(getFluidContainerOutputSlot());
        return Utils.canMergeItemStack(item, stack, getInventoryStackLimit());
    }

    public int getLightValue() {
        if (!getTankManager().isEmpty()) {
            return getTankManager().getTank().getFluid().getFluid().getLuminosity();
        }
        return 0;
    }
    
    @Override
    public boolean isItemValidForSlot(int i, ItemStack itemStack) {
        return false;
    }
    
    @Override
    public int fill(ForgeDirection from, FluidStack resource, boolean doFill) {
        if (FluidUtils.isFluidStackEmpty(resource) || (doEject && from != ForgeDirection.UNKNOWN)) {
            return 0;
        }
        
        int amount = 0;
        double energyConsumed, energyDrained;
        
        if (!doFill) {
            amount = getTankManager().fill(from, resource, false);
            energyConsumed = amount * Reference.TANK_ENERGY_PER_MB_IN;
            energyDrained = powerHandler.drainEnergy(energyConsumed, false);
            if (energyDrained != energyConsumed) {
                return 0;
            }
        } else if (fill(from, resource, false) > 0) {
            amount = getTankManager().fill(from, resource, true);
            energyConsumed = amount * Reference.TANK_ENERGY_PER_MB_IN;
            energyDrained = powerHandler.drainEnergy(energyConsumed, true);
        }
        
        if (amount > 0 && doFill) {
            worldObj.markBlockForUpdate(xCoord, yCoord, zCoord);
            markTileDirty();
        }
        
        return amount;
    }
    
    @Override
    public boolean canDrain(ForgeDirection from, Fluid fluid) {
        return true;
    }
    
    @Override
    public boolean canFill(ForgeDirection from, Fluid fluid) {
        return true;
    }
    
    @Override
    public void writeToNBT(NBTTagCompound compound) {
        super.writeToNBT(compound);
        compound.setBoolean("doEject", doEject);
    }
    
    @Override
    public void readFromNBT(NBTTagCompound compound) {
        super.readFromNBT(compound);
        doEject = compound.getBoolean("doEject");
    }
    
    @Override
    public int getTankCapacity() {
        return FluidContainerRegistry.BUCKET_VOLUME * 64;
    }
    
    @Override
    public void writePacketData(ByteBuf data) throws IOException {
        super.writePacketData(data);
        data.writeBoolean(doEject);
    }
    
    @Override
    public void handleUpdatePacket(PacketTileUpdate packet) throws IOException {
        super.handleUpdatePacket(packet);
        doEject = packet.input.readBoolean();
        // probably World.updateAllLightTypes
        worldObj.func_147451_t(xCoord, yCoord, zCoord);
        getCoords().markBlockForRenderUpdate(worldObj);
    }

    @Override
    public int getComparatorOverride(int side) {
        return FluidUtils.calcRedstoneFromTank(getTankManager().getTank());
    }

    @Override
    public List<UpgradeType> getSupportedUpgrades() {
        List<UpgradeType> list = super.getSupportedUpgrades();
        list.add(UpgradeType.FLUID_AUTO_EJECT);
        return list;
    }

    @Override
    public Map<TabSide, Integer> getOpenTabs() {
        return null;
    }

    @Override
    public void setOpenTabs(Map<TabSide, Integer> tabs) {}

    @Override
    public BucketType getBucketType() {
        return BucketType.BOTH;
    }

    @Override
    public IFluidHandler getFluidHandler() {
        return getTankManager();
    }

    @Override
    public IHPTInventory getInventory() {
        return this;
    }

    @Override
    public int getFluidContainerInputSlot() {
        return 0;
    }

    @Override
    public int getFluidContainerOutputSlot() {
        return 1;
    }
}
