/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.client.gui.rect;

import patrick96.hptanks.client.gui.HPTGui;
import patrick96.hptanks.client.gui.tooltip.GuiToolTipEnergy;
import patrick96.hptanks.core.power.IPowerTile;

public class GuiRectangleEnergy extends GuiRectangle {

    protected IPowerTile tile;
    
    protected float percentageToIgnore;
    
    public GuiRectangleEnergy(HPTGui gui, IPowerTile tile, int x, int y, int w, int h) {
        this(gui, tile, x, y, w, h, 0F);
    }
    public GuiRectangleEnergy(HPTGui gui, IPowerTile tile, int x, int y, int w, int h, float percentageToIgnore) {
        super(gui, x, y, w, h);
        
        this.tile = tile;
        
        this.percentageToIgnore = percentageToIgnore;
        
        setDynamicToolTip(new GuiToolTipEnergy(tile));
    }

    
    @Override
    public void draw() {
        super.draw();
        
        double energy = tile.getPowerHandler().getEnergyStored();
        double maxEnergy = tile.getPowerHandler().getMaxEnergyStored();
        gui.drawEnergyBar(this, energy, maxEnergy, percentageToIgnore);
    }
}
