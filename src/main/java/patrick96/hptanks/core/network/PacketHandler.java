/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.core.network;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.network.FMLNetworkEvent.ClientCustomPacketEvent;
import cpw.mods.fml.common.network.FMLNetworkEvent.CustomPacketEvent;
import cpw.mods.fml.common.network.FMLNetworkEvent.ServerCustomPacketEvent;
import cpw.mods.fml.common.network.NetworkRegistry.TargetPoint;
import cpw.mods.fml.common.network.internal.FMLProxyPacket;
import cpw.mods.fml.relauncher.Side;
import io.netty.buffer.ByteBuf;
import net.minecraft.client.network.NetHandlerPlayClient;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.network.NetHandlerPlayServer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import patrick96.hptanks.HPTanks;
import patrick96.hptanks.client.gui.IGuiTile;
import patrick96.hptanks.core.network.packets.*;
import patrick96.hptanks.tile.machine.TileEntityRubberFormer;
import patrick96.hptanks.util.Utils;

import java.io.IOException;

public class PacketHandler {
    
    @SubscribeEvent
    public void onServerPacket(ServerCustomPacketEvent event) {
        onPacket(event);
    }
    @SubscribeEvent
    public void onClientPacket(ClientCustomPacketEvent event) {
        onPacket(event);
    }
    
    public void onPacket(CustomPacketEvent event) {
        try {
            FMLProxyPacket packet = event.packet;
            ByteBuf data = packet.payload();
            byte id = data.readByte();
            EntityPlayer player;

            if(event.handler instanceof NetHandlerPlayServer) {
                player = ((NetHandlerPlayServer) event.handler).playerEntity;
            }
            else if(event.handler instanceof NetHandlerPlayClient) {
                player = Utils.getClientPlayer();
            }
            else {
                return;
            }
            
            switch (id) {
                
                case PacketIds.TILE_UPDATE:
                    PacketTileUpdate tilePacket = new PacketTileUpdate();
                    tilePacket.readData(data);
                    
                    onTileUpdate(player, tilePacket);
                    break;
                
                case PacketIds.BUTTON_CLICK:
                    PacketButton buttonPacket = new PacketButton();
                    buttonPacket.readData(data);
                    
                    onButtonClick(player, buttonPacket);
                    break;
                    
                case PacketIds.REQUEST_INFO:
                    PacketTileRequestUpdate requestPacket = new PacketTileRequestUpdate();
                    requestPacket.readData(data);
                    
                    onRequestData(player, requestPacket);
                    break;

                case PacketIds.RUBBER_FORMER_SELECTED_STACK_UPDATE:
                    PacketRubberFormerSelectedStack selectedStackPacket = new PacketRubberFormerSelectedStack();
                    selectedStackPacket.readData(data);

                    onRubberFormerSelectedStackChanged(player, selectedStackPacket);
                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void sendToPlayers(FMLProxyPacket packet, World world, int x, int y, int z, int range) {
        if (!world.isRemote) {
            HPTanks.channel.sendToAllAround(packet, new TargetPoint(world.provider.dimensionId, x, y, z, range));
        }
    }
    
    public static void sendToServer(FMLProxyPacket packet, World world) {
        if (world.isRemote) {
            HPTanks.channel.sendToServer(packet);
        }
    }
    
    public void onTileUpdate(EntityPlayer player, PacketTileUpdate packet) throws IOException {
        TileEntity tile = packet.getTile(player.worldObj);
        if (tile instanceof ISyncedTile) {
            ((ISyncedTile) tile).handleUpdatePacket(packet);
        }
    }
    
    public void onButtonClick(EntityPlayer player, PacketButton packet) {
        TileEntity tile = packet.getCoords().getTileEntity(player.worldObj);
        
        if (tile instanceof IGuiTile) {
            ((IGuiTile) tile).handleGuiButtonClick(packet.buttonId);
        }
    }
    
    private void onRequestData(EntityPlayer player, PacketCoords requestPacket) {
        TileEntity tile = requestPacket.getTile(player.worldObj);
        
        if(tile instanceof ISyncedTile) {
            ((ISyncedTile) tile).onRequestPacket();
        }
    }

    private void onRubberFormerSelectedStackChanged(EntityPlayer player, PacketRubberFormerSelectedStack packet) {
        TileEntity tile = packet.getTile(player.worldObj);

        if(tile instanceof TileEntityRubberFormer) {
            ((TileEntityRubberFormer) tile).onSelectedStackChanged(packet);
        }
    }
}
