/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */

package patrick96.hptanks.inventory;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import patrick96.hptanks.core.inventory.IHPTInventory;
import patrick96.hptanks.core.inventory.slot.SlotDisplay;
import patrick96.hptanks.recipe.HPTRegistry;
import patrick96.hptanks.recipe.RubberFormerRecipeInfo;
import patrick96.hptanks.tile.machine.TileEntityRubberFormer;
import patrick96.hptanks.util.Utils;

import java.util.ArrayList;
import java.util.List;

public class ContainerRubberFormer extends ContainerMachine implements IInventory {

    protected int currentScroll = 0;

    public final TileEntityRubberFormer rubberFormer;

    protected List<ItemStack> recipes = new ArrayList<ItemStack>();

    public ContainerRubberFormer(InventoryPlayer invPlayer, TileEntityRubberFormer machine) {
        super(invPlayer, machine);

        this.rubberFormer = machine;

        for(RubberFormerRecipeInfo info : HPTRegistry.rubberFormerRecipes) {
            // Checks if all conditions are met with already existing code
            if(HPTRegistry.getRubberFormerRecipe(info.getResult(), rubberFormer) != null) {
                recipes.add(info.getResult());
            }
        }

        ItemStack selectedStack = rubberFormer.getSelectedStack();
        if(selectedStack != null && isPartOfCurrentRecipes(selectedStack)) {
            int scroll = getIndexOfRecipe(selectedStack);
            currentScroll = recipes.size() >= scroll + getSizeInventory() ? scroll : 0;
        }
    }

    protected boolean isPartOfCurrentRecipes(ItemStack stack) {
        if(Utils.isItemStackEmpty(stack)) {
            return false;
        }

        for(ItemStack recipe : recipes) {
            if(ItemStack.areItemStacksEqual(recipe, stack)) {
                return true;
            }
        }

        return false;
    }

    protected int getIndexOfRecipe(ItemStack stack) {
        if(Utils.isItemStackEmpty(stack)) {
            return -1;
        }

        for(int i = 0; i < recipes.size(); i++) {
            ItemStack recipe = recipes.get(i);
            if(ItemStack.areItemStacksEqual(recipe, stack)) {
                return i;
            }
        }

        return -1;
    }

    @Override
    public ItemStack slotClick(int slotNum, int mouseButton, int modifier, EntityPlayer player) {
        if(inventorySlots.size() > slotNum && slotNum >= 0 && modifier == 0) {
            Slot slot = getSlot(slotNum);
            if(slot instanceof SlotDisplay) {
                // Clicks have to be handled client-side because the server know nothing about currentScroll
                if(!player.worldObj.isRemote) {
                    return null;
                }
                SlotDisplay display = (SlotDisplay) slot;
                rubberFormer.setSelectedStack(display.getStack());
            }
        }
        return super.slotClick(slotNum, mouseButton, modifier, player);
    }

    @Override
    public void addContainerSlots(IHPTInventory inv) {
        super.addContainerSlots(inv);
        for(int i = 0; i < getSizeInventory(); i++) {
            addSlotToContainer(new SlotDisplay(this, i, 174, 12 + i * 18));
        }
    }

    public void scrollTo(float currentScroll, int numScrollPositions) {
        this.currentScroll = (int) (currentScroll * numScrollPositions + 0.5D);
    }

    public int getCurrentScroll() {
        return currentScroll;
    }

    public Slot getSelectedStackSlot() {
        ItemStack selectedStack = rubberFormer.getSelectedStack();
        if(isPartOfCurrentRecipes(selectedStack)) {
            int position = getIndexOfRecipe(selectedStack);
            for(Object o : inventorySlots) {
                if(o instanceof SlotDisplay && ((SlotDisplay) o).getSlotIndex() == position - currentScroll) {
                    return (SlotDisplay) o;
                }
            }
        }

        return null;
    }

    /**
     * @return The amount of slots that are displayed in the gui, not the actual amount of recipes
     */
    @Override
    public int getSizeInventory() {
        return 8;
    }

    @Override
    public ItemStack getStackInSlot(int slot) {
        return currentScroll + slot >= recipes.size() ? null : recipes.get(slot + currentScroll);
    }

    @Override
    public ItemStack decrStackSize(int p_70298_1_, int p_70298_2_) {
        return null;
    }

    @Override
    public ItemStack getStackInSlotOnClosing(int p_70304_1_) {
        return null;
    }

    @Override
    public void setInventorySlotContents(int slot, ItemStack sta) {}

    @Override
    public String getInventoryName() {
        return "RubberFormerRecipes";
    }

    @Override
    public boolean hasCustomInventoryName() {
        return false;
    }

    @Override
    public int getInventoryStackLimit() {
        return 64;
    }

    @Override
    public void markDirty() {}

    @Override
    public boolean isUseableByPlayer(EntityPlayer player) {
        return false;
    }

    @Override
    public void openInventory() {}

    @Override
    public void closeInventory() {}

    @Override
    public boolean isItemValidForSlot(int slot, ItemStack stack) {
        return false;
    }
}
