/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */

package patrick96.hptanks.client.gui;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.entity.player.InventoryPlayer;
import org.lwjgl.opengl.GL11;
import patrick96.hptanks.client.gui.rect.GuiRectangleProgressArrow;
import patrick96.hptanks.tile.machine.TileEntityHPCutter;

public class GuiHPCutter extends GuiMachine {

    public final TileEntityHPCutter cutter;

    protected int deltaSinceLastLaserUpdate = 0;
    protected boolean[] laserPositions = new boolean[4];

    public GuiHPCutter(InventoryPlayer invPlayer, TileEntityHPCutter machine) {
        super(invPlayer, machine);
        cutter = machine;

        GuiRectangleProgressArrow progressArrow = new GuiRectangleProgressArrow(this, machine, 57, 47);

        rectManager.addRect(progressArrow);
        randomLaserPositions();
    }

    @Override
    public void drawScreen(int p_73863_1_, int p_73863_2_, float p_73863_3_) {
        deltaSinceLastLaserUpdate += getDelta();
        //System.out.println(getDelta());
        super.drawScreen(p_73863_1_, p_73863_2_, p_73863_3_);
    }

    private void randomLaserPositions() {
        for(int i = 0; i < laserPositions.length; i++) {
            laserPositions[i] = Minecraft.getMinecraft().theWorld.rand.nextFloat() > 0.6;
        }
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float opacity, int x, int y) {
        super.drawGuiContainerBackgroundLayer(opacity, x, y);
        if(deltaSinceLastLaserUpdate > 200 && machine.isActive()) {
            deltaSinceLastLaserUpdate = 0;
            randomLaserPositions();
        }
        Tessellator t = Tessellator.instance;
        GL11.glPushMatrix();
        GL11.glTranslatef(guiLeft, guiTop, 0);
        GL11.glEnable(GL11.GL_BLEND);
        GL11.glDisable(GL11.GL_TEXTURE_2D);
        OpenGlHelper.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA, 1, 0);
        GL11.glLineWidth(2);
        for(int i = 0; i < laserPositions.length; i++) {
            if(laserPositions[i] && machine.isActive()) {
                GL11.glColor4f(1F, 0, 0, 1F);
            } else {
                GL11.glColor4f(139 / 255F, 139 / 255F, 139 / 255F, 1F);
            }
            t.startDrawing(GL11.GL_LINES);
            //t.addVertex(50, 37 - 2.5 * i, 0);
            t.addVertex(50, 39 - i * (18 / 4D), 0);
            t.addVertex(63 + (i / 2D), 53, 0);
            //t.addVertex(59 + i * (14 / 4D), 53, 0);
            t.draw();
        }
        GL11.glEnable(GL11.GL_TEXTURE_2D);
        GL11.glDisable(GL11.GL_BLEND);
        GL11.glPopMatrix();
    }
}
