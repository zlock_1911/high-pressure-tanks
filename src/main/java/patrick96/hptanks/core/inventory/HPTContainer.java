/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.core.inventory;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import patrick96.hptanks.core.inventory.slot.SlotHPT;
import patrick96.hptanks.tile.TileEntityHPTBase;

public abstract class HPTContainer extends Container {
    
    public InventoryPlayer invPlayer;
    public IHPTInventory inv;
    public TileEntityHPTBase baseTile;
    
    public HPTContainer(InventoryPlayer invPlayer, IHPTInventory inv) {
        this(invPlayer);
        this.inv = inv;
        this.baseTile = (TileEntityHPTBase) inv;
        addContainerSlots(inv);
    }
  
    public HPTContainer() {}
    
    /*@Override
    public ItemStack slotClick(int par1, int par2, int par3, EntityPlayer par4EntityPlayer) {
        // TODO Don't use slots for upgrades but just handle left and right click of the player and add or subtract upgrades
        // this way the old rects can be used again
        System.out.println(invPlayer.getItemStack() == null? "MHM" : invPlayer.getItemStack().toString());
        return super.slotClick(par1, par2, par3, par4EntityPlayer);
    }*/
    
    public HPTContainer(InventoryPlayer invPlayer) {
        this.invPlayer = invPlayer;
        addInvSlots(invPlayer);
    }
    
    public void resetSlots() {
        inventoryItemStacks.clear();
        inventorySlots.clear();
        
        addInvSlots(invPlayer);
        addContainerSlots(inv);
        
        detectAndSendChanges();
    }

    public void addInvSlots(InventoryPlayer invPlayer) {
        addInvSlots(invPlayer, 8, 84);
    }
    
    public void addInvSlots(InventoryPlayer invPlayer, int left, int top) {
        for (int x = 0; x < 9; x++) {
            addSlotToContainer(new Slot(invPlayer, x, left + 18 * x, top + 58));
        }
        
        for (int y = 0; y < 3; y++) {
            for (int x = 0; x < 9; x++) {
                addSlotToContainer(new Slot(invPlayer, x + y * 9 + 9, left + 18 * x, top + y * 18));
            }
        }
    }
    
    public void addContainerSlots(IHPTInventory inv) {
        for(AdvancedSlot slot : inv.getInventoryHandler().getSlots().values()) {
            if(slot.getSlot() != null) {
                addSlotToContainer(slot.getSlot());
            }
        }
        // TODO write upgrade removal system w/o slots
        /*System.out.println("Adding Upgrades: " + ((TileEntity) inv).worldObj.isRemote + ", " + upgradeSlots.size());
        for(SlotUpgrade slot : upgradeSlots) {
            addSlotToContainer(slot);
        }*/
    }
    
    public boolean tryShift(int i, ItemStack stack) {
        Slot slot = (Slot) inventorySlots.get(i);
        boolean canShift = true;
        if(slot instanceof SlotHPT) {
            SlotHPT slotHpt = (SlotHPT) slot;
            canShift = slotHpt.canShift();
        }
        
        return canShift && slot.isItemValid(stack);
    }
    
    @Override
    public ItemStack transferStackInSlot(EntityPlayer player, int slot) {
        ItemStack stack = null;
        Slot slotObject = (Slot) inventorySlots.get(slot);
        if (slotObject != null && slotObject.getHasStack()) {
            if(slotObject instanceof SlotHPT && !((SlotHPT) slotObject).canShift()) {
                return null;
            }

            ItemStack stackInSlot = slotObject.getStack();
            stack = stackInSlot.copy();
            
            if(slot >= 36) {
                if(!mergeItemStack(stackInSlot, 0, 36, false)) {
                    return null;
                }
            }
            else {
                boolean succeeded = false;
                for(int i = 36; i < inventorySlots.size(); i++) {
                    if (tryShift(i, stackInSlot) && mergeItemStack(stackInSlot, i, i + 1, false)) {
                        break;
                    }
                }
            }
            
            if (stackInSlot.stackSize == 0) {
                slotObject.putStack(null);
            } else {
                slotObject.onSlotChanged();
            }
            
            if (stackInSlot.stackSize == stack.stackSize) {
                return null;
            }
            slotObject.onPickupFromSlot(player, stackInSlot);
        }
        
        return stack;
    }
    
}
