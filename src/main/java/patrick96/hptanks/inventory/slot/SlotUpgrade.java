/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.inventory.slot;

import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import patrick96.hptanks.core.inventory.slot.SlotInputRestricted;
import patrick96.hptanks.items.ItemUpgrade;
import patrick96.hptanks.upgrades.IUpgradeable;
import patrick96.hptanks.upgrades.UpgradeManager;
import patrick96.hptanks.upgrades.UpgradeType;
import patrick96.hptanks.upgrades.types.Upgrade;
import patrick96.hptanks.util.Utils;

public class SlotUpgrade extends SlotInputRestricted {

    public IUpgradeable upgradeable;
    public UpgradeType type = null;
    
    public SlotUpgrade(IInventory inventory, int id, int x, int y) {
        super(inventory, id, x, y);
    
        upgradeable = (IUpgradeable) inventory;
    }
    
    /**
     * @param type the type to set
     */
    public void setType(UpgradeType type) {
        this.type = type;
    }
    
    @Override
    public boolean isItemValid(ItemStack itemStack) {
        return UpgradeManager.canAcceptUpgrade(getType(itemStack), upgradeable);
    }
    
    public UpgradeType getType(ItemStack stack) {
        if(!Utils.isItemStackEmpty(stack) && stack.getItem() instanceof ItemUpgrade) {
            return ItemUpgrade.getUpgradeType(stack.getItemDamage());
        }
        
        return null;
    }
    
    @Override
    public ItemStack getStack() {
        Upgrade up = upgradeable.getUpgradeManager().getUpgrade(type);
        return up == null? null : up.getItemStack();
    }
    
    @Override
    public void putStack(ItemStack stack) {
        Upgrade up = ItemUpgrade.getUpgrade(stack);
        upgradeable.applyUpgrade(up);
        type = up.getType();
        onSlotChanged();
    }
    
    @Override
    public int getSlotStackLimit() {
        return Upgrade.getUpgrade(type).getLimit();
    }
    
    @Override
    public ItemStack decrStackSize(int amount) {
        return upgradeable.getUpgradeManager().removeUpgrade(type, amount).getItemStack();
    }
}
