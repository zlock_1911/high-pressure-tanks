/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */

package patrick96.hptanks.client.renderer.ctm;

import com.google.common.primitives.Ints;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.util.IIcon;

import java.util.ArrayList;
import java.util.List;

import static patrick96.hptanks.client.renderer.ctm.CTMConfiguration.*;

public class CTMGenerator {

    public static IIcon[] generateTextures(ICTM block, IIconRegister register) {
        if(!(register instanceof TextureMap)) {
            return new TextureCTM[0];
        }

        List<CTMConfiguration> configs = new ArrayList<CTMConfiguration>();

        List<List<Integer>> sides = new ArrayList<List<Integer>>();

        for(int i = 0; i <= 4; i++) {
            permutations(sides, new int[]{0, 1, 2, 3}, i);
        }

        for(List<Integer> permutation : sides) {
            CTMConfiguration config = new CTMConfiguration(block);

            for(int side : permutation) {
                config.set(side, Type.BORDER);
            }

            int[] freeCorners = findFreeCorners(config);
            List<List<Integer>> corners = new ArrayList<List<Integer>>();
            for(int i = 0; i <= freeCorners.length; i++) {
                permutations(corners, freeCorners, i);
            }

            for(List<Integer> cornerPermutation : corners) {
                CTMConfiguration cornerConfig = config.copy();

                for(int corner : cornerPermutation) {
                    cornerConfig.set(corner, Type.CORNER);
                }

                configs.add(cornerConfig);
            }
        }

        IIcon[] textures = new TextureCTM[configs.size()];
        TextureMap map = (TextureMap) register;

        for(int i = 0; i < configs.size(); i++) {
            CTMConfiguration config = configs.get(i);
            String name = config.getName();
            map.setTextureEntry(name, new TextureCTM(config));

            textures[i] = map.getTextureExtry(name);
        }

        return textures;
    }

    /**
     * Finds corners that aren't already occupied by Type.BORDER
     * @param config the config with the current border configuration
     * @return an array with the indices where corners are possible
     */
    private static int[] findFreeCorners(CTMConfiguration config) {
        List<Integer> result = new ArrayList<Integer>();
        Type[] sides = config.getSides();

        for(int i = 0; i < sides.length; i++) {
            if(sides[i] == Type.NONE && sides[i == 0? 3 : i - 1] == Type.NONE) {
                result.add(i);
            }
        }

        return Ints.toArray(result);
    }

    public static void permutations(List<List<Integer>> list, int[] arr, int count) {
        permutations(list, arr, new ArrayList<Integer>(), count);
    }

    public static void permutations(List<List<Integer>> list, int[] arr, List<Integer> out, int count) {
        if(arr.length == 0 || out.size() == count) {
            if(!doesContainList(list, out)) {
                list.add(out);
            }
            return;
        }

        for(int i = 0; i < arr.length; i++) {
            List<Integer> newOut = new ArrayList<Integer>();
            for(Integer num : out) {
                newOut.add(num);
            }

            newOut.add(arr[i]);

            permutations(list, getArrayExcept(arr, i), newOut, count);
        }
    }

    /**
     * Removes the item at index from the array arr
     * @param arr the array
     * @param index the index to remove
     * @return the trimmed array
     */
    private static int[] getArrayExcept(int[] arr, int index) {
        int[] newArray = new int[arr.length - 1];
        int in = 0;
        for(int i = 0; i < arr.length; i++) {
            if(i != index) {
                newArray[in] = arr[i];
                in++;
            }
        }

        return newArray;
    }

    /**
     * Checks list for any occurence of match regardless of the order of the items (no additional items may be in the list)
     * @param list a list of lists
     * @param match the list to match against
     * @return whether or not match is inside list ()
     */
    private static <T> boolean doesContainList(List<List<T>> list, List<T> match) {
        for(List<T> perm : list) {
            if(perm.size() != match.size()) {
                continue;
            }

            if(perm.containsAll(match)) {
                return true;
            }
        }
        return false;
    }


}
