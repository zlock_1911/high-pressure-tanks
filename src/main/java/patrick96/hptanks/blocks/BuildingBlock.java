/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */

package patrick96.hptanks.blocks;

import cpw.mods.fml.common.registry.GameRegistry;

/**
 * This is not actually a block class, but rather a container class for all building blocks associated with a block type (regular block, stairs, slabs)
 * It creates instances for these three blocks from a base texture
 */
public class BuildingBlock {

    protected BlockStairsHPT stairs;
    protected BlockSlabHPT slab;
    protected BlockSlabHPT doubleSlab;
    protected BlockHPT block;

    public BuildingBlock(BlockHPT blockBase) {

        block = blockBase;
        block.setHardness(3F);
        block.setResistance(10F);
        block.setHarvestLevel("pickaxe", 0);

        stairs = new BlockStairsHPT(block, 0);

        doubleSlab = new BlockSlabHPT(block, true, block.getMaterial());
        doubleSlab.setBlockTextureName(block.getTextureName());

        slab = new BlockSlabHPT(block, false, block.getMaterial());
        slab.setBlockTextureName(block.getTextureName());
    }

    public void register() {
        GameRegistry.registerBlock(block, block.unlocalizedNameRaw);
        GameRegistry.registerBlock(stairs, stairs.unlocalizedNameRaw);
        GameRegistry.registerBlock(doubleSlab, ItemBlockSlabHPT.class, doubleSlab.unlocalizedNameRaw, slab, doubleSlab, true);
        GameRegistry.registerBlock(slab, ItemBlockSlabHPT.class, slab.unlocalizedNameRaw, slab, doubleSlab, false);
    }

    public void addRecipes() {
        GameRegistry.addRecipe(slab.getStack(6), "BBB", 'B', block);
        GameRegistry.addRecipe(stairs.getStack(4), "B  ", "BB ", "BBB", 'B', block);
    }

    public BlockHPT getBlock() {
        return block;
    }

    public BlockStairsHPT getStairs() {
        return stairs;
    }

    public BlockSlabHPT getSlab() {
        return slab;
    }

    public BlockSlabHPT getDoubleSlab() {
        return doubleSlab;
    }
}
