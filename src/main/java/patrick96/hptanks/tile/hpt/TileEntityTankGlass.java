/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.tile.hpt;


public class TileEntityTankGlass extends TileEntityTankComponent implements IHPTWallComponent {
    
    // TODO fix light level not updating properly
    
    /*private int prevLightLevel = 0;
    private boolean updateNextTick = false;*/
    
    /*@Override
    public void updateEntity() {
        super.updateEntity();
        if(!worldObj.isRemote) {
            return;
        }
        worldObj.updateAllLightTypes(xCoord, yCoord, zCoord);
        if(updateNextTick) {
            worldObj.updateAllLightTypes(xCoord, yCoord, zCoord);
            updateNextTick = false;
        }
        
        int currLevel = calcLightLevel();
        if(currLevel != prevLightLevel) {
            updateNextTick = true;
        }
        
        prevLightLevel = currLevel;
    }*/
    
    /**
     * Gets the light level.
     * Only called on the server side
     * calculates the light level depending on the fluid height, the glass position and the luminosity of the fluid.
     * @return the light level
     */
    /*public int getLightLevel() {
        return isPartOfMultiBlock()? prevLightLevel : 0;
    }    
    protected int calcLightLevel() {
        int ret = 0;
        if(isPartOfMultiBlock()) {
            
            FluidStack fluidStack = getControl().getTank().getFluid();
            if(!FluidUtils.isFluidStackEmpty(fluidStack)) {
                int level = getControl().getFluidLevel();
                int distance = getDistanceFromBottom();
                
                Fluid fluid = fluidStack.getFluid();
                int luminosity = 0;
                if(fluid != null) {
                    luminosity = fluidStack.getFluid().getLuminosity();
                }
                
                if(luminosity > 0 && luminosity <= 15) {
                    int res = luminosity;
                    if(getFacingOut() != ForgeDirection.UP) {
                        res--;
                    }
                    
                    ret = res - Math.max(0, distance - level);
                    
                }
            }
        }
        
        return ret;
    }
    
    @Override
    public void handleUpdatePacket(PacketTileUpdate packet) throws IOException {
        super.handleUpdatePacket(packet);
        if(!isPartOfMultiBlock()) {
            updateNextTick = true;
        }
    }*/
}
