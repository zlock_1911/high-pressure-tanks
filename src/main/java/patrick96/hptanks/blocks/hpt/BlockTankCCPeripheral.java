/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.blocks.hpt;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.IIcon;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.common.util.ForgeDirection;
import patrick96.hptanks.lib.BlockInfo;
import patrick96.hptanks.tile.hpt.TileEntityTankCCPeripheral;
import patrick96.hptanks.tile.hpt.TileEntityTankValve;
import patrick96.hptanks.util.Coords;
import patrick96.hptanks.util.Helper;
import patrick96.hptanks.util.Utils;

public class BlockTankCCPeripheral extends BlockTankComponent {

    @SideOnly(Side.CLIENT)
    protected IIcon ccPortTop;
    @SideOnly(Side.CLIENT)
    protected IIcon ccPortActive;

    public BlockTankCCPeripheral() {
        super(BlockInfo.TANK_CC_PERIPHERAL_UNLOCALIZED_NAME);
        setBlockTextureName(BlockInfo.TANK_CC_PERIPHERAL_TEXTURE);
    }

    @Override
    public TileEntity createNewTileEntity(World world, int meta) {
        return new TileEntityTankCCPeripheral();
    }

    @Override
    @SideOnly(Side.CLIENT)
    public void registerBlockIcons(IIconRegister register) {
        super.registerBlockIcons(register);
        ccPortTop = register.registerIcon(Utils.getTexture(BlockInfo.TANK_CC_PERIPHERAL_TEXTURE + "Top"));
        ccPortActive = register.registerIcon(Utils.getTexture(BlockInfo.TANK_CC_PERIPHERAL_TEXTURE + "Active"));
    }

    @Override
    public IIcon getIcon(int side, int meta) {
        return side == 0 || side == 1? ccPortTop : ccPortActive;
    }

    @Override
    @SideOnly(Side.CLIENT)
    public IIcon getIcon(IBlockAccess blockAccess, int x, int y, int z, int side) {
        if(side == 0 || side == 1) {
            return ccPortTop;
        }

        Coords coords = new Coords(x, y, z);
        TileEntity tile = coords.getTileEntity(blockAccess);
        if (tile instanceof TileEntityTankCCPeripheral) {
            TileEntityTankCCPeripheral ccPort = (TileEntityTankCCPeripheral) tile;

            return ccPort.isPartOfMultiBlock()? ccPortActive : blockIcon;
        }

        return super.getIcon(blockAccess, x, y, z, side);
    }
}
