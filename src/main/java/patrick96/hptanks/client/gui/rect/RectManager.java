/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.client.gui.rect;

import org.lwjgl.opengl.GL11;
import patrick96.hptanks.client.util.MouseClickType;

import java.awt.*;
import java.util.ArrayList;
import java.util.Collection;

public class RectManager {

    public ArrayList<GuiRectangle> rectangles = new ArrayList<GuiRectangle>();
    
    public RectManager() {}
    
    /**
     * After that no new rectangles should be added
     */
    public void init() {
        for(GuiRectangle rect : rectangles) {
            rect.init();
        }
    }
    
    public RectManager addRect(GuiRectangle rect) {
        rectangles.add(rect);
        return this;
    }

    public RectManager removeRect(GuiRectangle rect) {
        rectangles.remove(rect);

        return this;
    }
    
    public RectManager addAllRects(Collection<GuiRectangle> rects) {
        rectangles.addAll(rects);
        
        return this;
    }

    public RectManager removeAllRects(Collection<GuiRectangle> rects) {
        rectangles.removeAll(rects);

        return this;
    }
    
    public GuiRectangle getRectAt(int x, int y) {
        for(GuiRectangle rect : rectangles) {
            GuiRectangle rectAt = rect.getRectAt(x, y);
            if(rectAt != null) {
                return rectAt;
            }
        }
        
        return null;
    }

    public boolean doesIntersect(Rectangle intersectRect) {
        for(GuiRectangle rect : rectangles) {
            if(intersectRect.intersects(new Rectangle(rect.getX(), rect.getY(), rect.getW(), rect.getH()))) {
                return true;
            }
            for(GuiRectangle rect2 : rect.children) {
                if(intersectRect.intersects(new Rectangle(rect2.getX(), rect2.getY(), rect2.getW(), rect2.getH()))) {
                    return true;

                }
            }
        }
        return false;
    }
    
    public void draw() {
        for(GuiRectangle rect : rectangles) {
            GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
            rect.draw();
        }
    }
    
    public void drawForeGround(int mouseX, int mouseY) {
        for(GuiRectangle rect : rectangles) {
            rect.drawForeground(mouseX, mouseY);
        }
        
        for(GuiRectangle rect: rectangles) {
            rect.mouseOver(mouseX, mouseY);
        }
    }
    
    public void mouseClicked(int x, int y, MouseClickType type) {
        GuiRectangle rectAt = getRectAt(x, y);
        if(rectAt != null && rectAt.shouldFireMouseClick(x, y, type)) {
            rectAt.mouseClicked(x, y, type);
        }
    }
    
}
