/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.util;

import io.netty.buffer.ByteBuf;
import net.minecraft.block.Block;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.common.util.ForgeDirection;

import java.io.IOException;

public class Coords {
    
    public int x = 0;
    public int y = 0;
    public int z = 0;
    
    private boolean initialized = false;
    
    public Coords(int xCoord, int yCoord, int zCoord) {
        init(xCoord, yCoord, zCoord);
    }
    
    public Coords(TileEntity tile) {
        init(tile.xCoord, tile.yCoord, tile.zCoord);
    }
    
    public Coords() {}
    
    public Coords init(int xCoord, int yCoord, int zCoord) {
        x = xCoord;
        y = yCoord;
        z = zCoord;
        
        initialized = true;
        
        return this;
    }
    
    public boolean initialized() {
        return initialized;
    }
    
    public Coords init(int[] coordsArray) {
        if (coordsArray == null || coordsArray.length != 3) {
            return this;
        }
        
        return init(coordsArray[0], coordsArray[1], coordsArray[2]);
    }
    
    public static Coords a(int[] coordsArray) {
        if (coordsArray == null || coordsArray.length != 3) {
            return new Coords();
        }
        
        return new Coords(coordsArray[0], coordsArray[1], coordsArray[2]);
    }
    
    public Coords x(int add) {
        return new Coords(x + add, y, z);
    }
    
    public Coords y(int add) {
        return new Coords(x, y + add, z);
    }
    
    public Coords z(int add) {
        return new Coords(x, y, z + add);
    }
    
    public Coords add(ForgeDirection dir) {
        if (dir != null) {
            return x(dir.offsetX).y(dir.offsetY).z(dir.offsetZ);
        }
        return this;
    }

    public Coords add(Coords coords) {
        if(coords != null) {
            return x(coords.x).y(coords.y).z(coords.z);
        }
        return this;
    }

    public Coords add(int x, int y, int z) {
        return add(new Coords(x, y, z));
    }
    
    public Coords diff(Coords coords) {
        int diffX = coords.x - x;
        int diffY = coords.y - y;
        int diffZ = coords.z - z;
        
        return new Coords(diffX, diffY, diffZ);
    }
    
    public Block getBlock(IBlockAccess world) {
        return initialized() ? world.getBlock(x, y, z) : null;
    }
    
    public int getBlockMetadata(IBlockAccess world) {
        return initialized() ? world.getBlockMetadata(x, y, z) : null;
    }
    
    public void setBlockMetadataWithNotify(World world, int meta, int flag) {
        if (initialized()) {
            world.setBlockMetadataWithNotify(x, y, z, meta, flag);
        }
    }
    
    /**
     * Adds the add parameter to the block metadata
     */
    public void changeBlockMetadataWithNotify(World world, int add, int flag) {
        setBlockMetadataWithNotify(world, getBlockMetadata(world) + add, flag);
    }
    
    public TileEntity getTileEntity(IBlockAccess world) {
        return initialized() ? world.getTileEntity(x, y, z) : null;
    }
    
    public void markBlockForUpdate(World world) {
        world.markBlockForUpdate(x, y, z);
    }
    
    public void markBlockForRenderUpdate(World world) {
        world.func_147479_m(x, y, z);
    }
    
    public void notifyBlockChange(World world, Block block) {
        world.notifyBlockChange(x, y, z, block);
    }
    
    public boolean equals(int xCoord, int yCoord, int zCoord) {
        return x == xCoord && y == yCoord && z == zCoord;
    }
    
    public boolean equals(Coords coords) {
        return equals(coords.x, coords.y, coords.z);
    }
    
    public Coords copy() {
        return initialized()? new Coords(x, y, z): new Coords();
    }
    
    public int[] toArray() {
        return new int[] { x, y, z };
    }
    
    @Override
    public String toString() {
        return initialized()? "X: " + x + ", Y: " + y + ", Z: " + z : "Not Initialized";
    }
    
    public String toString(String name) {
        return name + ": " + toString();
    }
    
    public void writeToNBT(NBTTagCompound compound, String name) {
        if (name == null || name.length() < 1) {
            return;
        }
        
        // If the coords aren't initialized nothing will be written to the Compound
        // The Coords#loadFromNBT will assume that it's empty
        if (initialized()) {
            NBTTagCompound coordsCompound = new NBTTagCompound();
            coordsCompound.setInteger("x", x);
            coordsCompound.setInteger("y", y);
            coordsCompound.setInteger("z", z);
            compound.setTag(name, coordsCompound);
        }
    }
    
    public Coords loadFromNBT(NBTTagCompound compound, String name) {
        
        if(compound.hasKey(name)) {
            NBTTagCompound coordsCompound = compound.getCompoundTag(name);
            
            if (coordsCompound.hasKey("x") && coordsCompound.hasKey("y") && coordsCompound.hasKey("z")) {
                x = coordsCompound.getInteger("x");
                y = coordsCompound.getInteger("y");
                z = coordsCompound.getInteger("z");
                initialized = true;
                return this;
            }
        }

        initialized = false;
        
        return this;
    }
    
    public Coords loadFromTileNBT(NBTTagCompound compound) {
        init(compound.getInteger("x"), compound.getInteger("y"), compound.getInteger("z"));
        return this;
    }
    
    public void writeData(ByteBuf data) throws IOException {
        data.writeInt(x);
        data.writeInt(y);
        data.writeInt(z);
        data.writeBoolean(initialized);
    }
    
    public void readData(ByteBuf data) throws IOException {
        x = data.readInt();
        y = data.readInt();
        z = data.readInt();
        
        initialized = data.readBoolean();
    }
    
    public Coords reset() {
        x = 0;
        y = 0;
        z = 0;
        initialized = false;
        return this;
    }
}
