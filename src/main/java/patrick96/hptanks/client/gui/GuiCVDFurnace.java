/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */

package patrick96.hptanks.client.gui;

import net.minecraft.entity.player.InventoryPlayer;
import patrick96.hptanks.client.gui.rect.*;
import patrick96.hptanks.recipe.CVDFurnaceRecipeInfo;
import patrick96.hptanks.tile.machine.TileEntityCVDFurnace;
import patrick96.hptanks.util.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class GuiCVDFurnace extends GuiMachine {

    public final TileEntityCVDFurnace cvdFurnace;
    private List<GuiRectangle> catalystRects = new ArrayList<GuiRectangle>();

    public GuiCVDFurnace(InventoryPlayer invPlayer, TileEntityCVDFurnace machine) {
        super(invPlayer, machine);

        this.xSize = 256;

        this.cvdFurnace = machine;

        GuiRectangleProgressArrow progressArrow = new GuiRectangleProgressArrow(this, machine, 57, 47);
        GuiRectangleFluid fluidRect = new GuiRectangleFluid(this, cvdFurnace.getTankManager(), 152, 8, GuiRectangleFluid.FluidScaleType.SMALL);

        rectManager.addRect(progressArrow);
        rectManager.addRect(fluidRect);
    }

    @Override
    public void drawScreen(int p_73863_1_, int p_73863_2_, float p_73863_3_) {
        int i = 0;
        for(Map.Entry<CVDFurnaceRecipeInfo.Catalyst, Float> cat : cvdFurnace.getCatalysts().entrySet()) {
            catalystRects.add(new GuiRectangleCatalyst(cat.getKey(), this, 180 + (i % 2) * 30, 90 + (int) Math.floor(i / 2F) * 30, 30, 30));
            i++;
        }
        rectManager.addAllRects(catalystRects);
        super.drawScreen(p_73863_1_, p_73863_2_, p_73863_3_);
        rectManager.removeAllRects(catalystRects);
        catalystRects.clear();
    }

    @Override
    protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY) {
        drawString(StringUtils.localize("hpt.catalyst"), 174, 7, 0x404040);
        super.drawGuiContainerForegroundLayer(mouseX, mouseY);
    }
}
