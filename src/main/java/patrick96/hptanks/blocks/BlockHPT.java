/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.blocks;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.common.util.ForgeDirection;
import patrick96.hptanks.HPTanks;
import patrick96.hptanks.core.fluids.FluidUtils;
import patrick96.hptanks.core.inventory.AdvancedSlot;
import patrick96.hptanks.core.inventory.IHPTInventory;
import patrick96.hptanks.tile.TileEntityHPTBase;
import patrick96.hptanks.upgrades.IUpgradeable;
import patrick96.hptanks.upgrades.types.Upgrade;
import patrick96.hptanks.util.Coords;
import patrick96.hptanks.util.Helper;
import patrick96.hptanks.util.Utils;

import java.util.Collection;

public class BlockHPT extends BlockContainer {
    
    // The Unlocalized name without any prefixes, so that I don't have to cut them
    public final String unlocalizedNameRaw;
    
    public BlockHPT(Material material, String unlocalizedName) {
        super(material);
        setCreativeTab(HPTanks.tabsHPT);
        unlocalizedNameRaw = unlocalizedName.toLowerCase();
        setBlockName("hpt." + unlocalizedNameRaw);
    }
    
    @Override
    @SideOnly(Side.CLIENT)
    public void registerBlockIcons(IIconRegister register) {
        blockIcon = register.registerIcon(Utils.getTexture(getTextureName()));
    }
    
    public void dropItems(World world, int x, int y, int z) {
        Coords coords = new Coords(x, y, z);
        TileEntity tile = coords.getTileEntity(world);
        if(!world.isRemote) {
            if(tile instanceof IHPTInventory) {
                IHPTInventory inv = (IHPTInventory) tile;
                for(AdvancedSlot slot : inv.getInventoryHandler().getSlots().values()) {
                    if(slot.getSlot() != null && slot.getSlot().dropContents()) {
                        Utils.dropItemStack(world, slot.getContents(), x, y, z);
                    }
                }
            }
            
            if(tile instanceof IUpgradeable) {
                IUpgradeable upgradeable = (IUpgradeable) tile;
                Collection<Upgrade> upgrades = upgradeable.getUpgradeManager().getUpgrades();
                for(Upgrade upgrade : upgrades) {
                    Utils.dropItemStack(world, upgrade.getItemStack(), x, y, z);
                }
            }
        }
    }

    @Override
    public boolean rotateBlock(World worldObj, int x, int y, int z, ForgeDirection axis) {
        Coords coords = new Coords(x, y, z);
        TileEntity tile = coords.getTileEntity(worldObj);

        if(tile instanceof TileEntityHPTBase && ((TileEntityHPTBase) tile).isRotatable()) {
            ((TileEntityHPTBase) tile).rotate();
            return true;
        }

        return false;
    }

    @Override
    public ForgeDirection[] getValidRotations(World worldObj, int x, int y, int z) {
        Coords coords = new Coords(x, y, z);
        TileEntity tile = coords.getTileEntity(worldObj);

        if(tile instanceof TileEntityHPTBase && ((TileEntityHPTBase) tile).isRotatable()) {
            byte[] matrix = ((TileEntityHPTBase) tile).getRotationMatrix();
            ForgeDirection[] rotations = new ForgeDirection[matrix.length];

            for(int i = 0; i < matrix.length; i++) {
                rotations[i] = ForgeDirection.getOrientation(matrix[i]);
            }

            return rotations;
        }

        return super.getValidRotations(worldObj, x, y, z);
    }

    @Override
    public void breakBlock(World world, int x, int y, int z, Block block, int meta) {
        dropItems(world, x, y, z);
        super.breakBlock(world, x, y, z, block, meta);
    }
    
    @Override
    public boolean hasTileEntity(int metadata) {
        return false;
    }
    
    @Override
    public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int side, float par7, float par8, float par9) {
        if(world.isRemote) {
            return false;
        }
        
        Coords coords = new Coords(x, y, z);
        
        if(player != null) {
            ItemStack current = player.getCurrentEquippedItem();
            
            if(hasTileEntity(coords.getBlockMetadata(world)) && !Utils.isItemStackEmpty(current) && Helper.canWrench(current.getItem())) {
                TileEntity tile = coords.getTileEntity(world);
                if(tile != null && tile instanceof TileEntityHPTBase) {
                    ((TileEntityHPTBase) tile).onWrench(player, ForgeDirection.getOrientation(side));
                    return true;
                }
            }

            if(FluidUtils.bucketTile(coords.getTileEntity(world), player)) {
                return true;
            }
        }
        return super.onBlockActivated(world, x, y, z, player, side, par7, par8, par9);
    }
    
    @Override
    public boolean canBeReplacedByLeaves(IBlockAccess world, int x, int y, int z) {
        return false;
    }
    
    /**
     * Gets the comparator input override.
     * This method shouldn't be overriden, just implement the {@link Block#hasComparatorInputOverride()} and implement the
     * {@link TileEntityHPTBase#getComparatorOverride(int)} in the tile, if you want your block to be comparator compatible
     * @param world the world
     * @param x the x
     * @param y the y
     * @param z the z
     * @param side the side
     * @return the comparator input override
     */
    @Override
    public int getComparatorInputOverride(World world, int x, int y, int z, int side) {
        TileEntity tile = world.getTileEntity(x, y, z);
        if(tile instanceof TileEntityHPTBase) {
            TileEntityHPTBase base = (TileEntityHPTBase) tile;
            
            return base.getComparatorOverride(side);
        }
        
        return super.getComparatorInputOverride(world, x, y, z, side);
    }
    
    public ItemStack getStack() {
        return getStack(1, 0);
    }
    
    public ItemStack getStack(int size) {
        return getStack(size, 0);
    }
    
    public ItemStack getStack(int size, int meta) {
        return new ItemStack(this, size, meta);
    }

    public float getBlockHardness() {
        return blockHardness;
    }

    public float getBlockResistance() {
        return blockResistance;
    }

    @Override
    public String getTextureName() {
        // Just a copy of super.getTextureName() because it is client only
        return this.textureName == null ? "MISSING_ICON_BLOCK_" + getIdFromBlock(this) + "_" + this.getUnlocalizedName() : this.textureName;
    }

    @Override
    public TileEntity createNewTileEntity(World p_149915_1_, int p_149915_2_) {
        return null;
    }
}
