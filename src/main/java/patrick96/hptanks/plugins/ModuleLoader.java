/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.plugins;

import cpw.mods.fml.common.Loader;
import patrick96.hptanks.util.LogHelper;

import java.util.ArrayList;

public class ModuleLoader {
    
    public static final ModuleLoader instance = new ModuleLoader();
    
    protected ArrayList<ICompatModule> modules = new ArrayList<ICompatModule>();
    
    private ModuleLoader() {}
    
    public boolean addModule(ICompatModule module) {
        if(module != null) {
            String modId = module.getDependency();
            if(Loader.isModLoaded(modId)) {
                modules.add(module);
                LogHelper.info("Loading Compat Module for " + module.getCompatName());
            }
        }
        
        return false;
    }
    public void preInit() {
        for(ICompatModule module : modules) {
            module.preInit();
        }
    }

    public void init() {
        for(ICompatModule module : modules) {
            module.init();
        }
    }

    public void postInit() {
        for(ICompatModule module : modules) {
            module.postInit();
        }
    }
    
}
