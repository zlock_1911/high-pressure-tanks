/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.client.gui.tooltip;

import net.minecraft.util.EnumChatFormatting;
import patrick96.hptanks.core.power.IPowerTile;
import patrick96.hptanks.core.power.PowerUtils;
import patrick96.hptanks.util.StringUtils;

import java.util.List;

public class GuiToolTipEnergy extends GuiDynamicToolTip {
    
    protected IPowerTile tile;
    
    public GuiToolTipEnergy(IPowerTile tile) {
        this.tile = tile;
    }
    
    @Override
    public List<String> getToolTip() {
        List<String> list = PowerUtils.powerHandlerToList(tile.getPowerHandler());
        list.add(0, EnumChatFormatting.DARK_RED + "" + EnumChatFormatting.ITALIC + StringUtils.localize("hpt.power"));
        return list;
    }
    
}
