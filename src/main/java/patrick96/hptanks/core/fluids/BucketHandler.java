/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.core.fluids;

import cpw.mods.fml.common.eventhandler.Event.Result;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.event.entity.player.FillBucketEvent;
import patrick96.hptanks.util.Coords;

import java.util.HashMap;
import java.util.Map;

public class BucketHandler {

    /**
     * Maps Fluid Blocks to buckets/other fluidContainer filled with this block's fluid
     */
    private Map<Block, Item> buckets = new HashMap<Block, Item>();
    
    public static BucketHandler instance = new BucketHandler();
    
    public BucketHandler() {}
    
    public void addCustomBucket(Block block, Item bucket) {
        if (block == null || bucket == null || buckets.containsKey(block)) {
            return;
        }
        buckets.put(block, bucket);
    }
    
    public Item getBucket(Block block) {
        return buckets.get(block);
    }
    
    @SubscribeEvent
    public void onBucketFill(FillBucketEvent event) {
        
        Coords coords = new Coords(event.target.blockX, event.target.blockY, event.target.blockZ);
        Block block = coords.getBlock(event.world);
        Item bucket = getBucket(block);
        
        if (bucket == null) {
            return;
        }

        event.world.setBlockToAir(coords.x, coords.y, coords.z);

        event.result = new ItemStack(bucket);
        event.setResult(Result.ALLOW);
    }
}
