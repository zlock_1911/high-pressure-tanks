/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.core.fluids;

import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;
import net.minecraftforge.common.util.ForgeDirection;
import net.minecraftforge.fluids.*;
import org.apache.commons.lang3.text.WordUtils;
import patrick96.hptanks.core.inventory.IHPTInventory;
import patrick96.hptanks.lib.Reference;
import patrick96.hptanks.tile.TileEntityHPTBase;
import patrick96.hptanks.upgrades.IUpgradeable;
import patrick96.hptanks.upgrades.UpgradeType;
import patrick96.hptanks.upgrades.types.Upgrade;
import patrick96.hptanks.upgrades.types.UpgradeSpeed;
import patrick96.hptanks.util.Coords;
import patrick96.hptanks.util.StringUtils;
import patrick96.hptanks.util.Utils;

import java.util.*;
import java.util.Map.Entry;

import static patrick96.hptanks.util.StringUtils.*;

public class FluidUtils {
    
    public static <T extends TileEntity & IFluidHandler> void ejectFluid(T tile, int maxDrain) {
        ejectFluid(tile, maxDrain, ForgeDirection.VALID_DIRECTIONS);
    }
    
    public static <T extends TileEntity & IFluidHandler> void ejectFluid(T tile, int maxDrain, ForgeDirection... directions) {
        ejectFluid(tile, maxDrain, new ArrayList<ForgeDirection>(Arrays.asList(directions)));
    }

    // TODO go over this again
    public static <T extends TileEntity & IFluidHandler> void ejectFluid(T tile, int maxDrain, List<ForgeDirection> directions) {
        if(tile == null || isFluidStackEmpty(tile.drain(ForgeDirection.UNKNOWN, Integer.MAX_VALUE, false)) || tile.getWorldObj().isRemote || directions == null || directions.size() == 0) {
            return;
        }
        
        FluidStack contained = tile.drain(ForgeDirection.UNKNOWN, Integer.MAX_VALUE, false);
        int maxEject = Math.min(contained.amount, directions.size() * maxDrain);
        
        
        Map<ForgeDirection, Integer> freeSpacePerSide = new HashMap<ForgeDirection, Integer>();
        Map<ForgeDirection, Integer> ejectionList = new HashMap<ForgeDirection, Integer>();
        
        for(ForgeDirection dir : directions) {
            TileEntity adj = (new Coords(tile)).add(dir).getTileEntity(tile.getWorldObj());
            if(adj instanceof IFluidHandler) {
                int space = ((IFluidHandler) adj).fill(dir.getOpposite(), contained, false);
                if(space > 0){
                    freeSpacePerSide.put(dir, space);
                }
            }
        }
        
        int maxEjectPerSide;
        while(maxEject > 0) {
            if(freeSpacePerSide.size() == 0) {
                break;
            }
            
            Map<ForgeDirection, Integer> freeSpacePerSideCopy = new HashMap<ForgeDirection, Integer>();
            maxEjectPerSide = (int) Math.floor(maxEject / freeSpacePerSide.size());
            if(maxEjectPerSide == 0) {
                break;
            }
            for(Entry<ForgeDirection, Integer> e : freeSpacePerSide.entrySet()) {
                int added;
                if(e.getValue() < maxEjectPerSide) {
                    added = e.getValue();
                }
                else {
                    added = maxEjectPerSide;
                    if(e.getValue() > maxEjectPerSide) {
                        freeSpacePerSideCopy.put(e.getKey(), e.getValue() - maxEjectPerSide);
                    }
                }
                maxEject -= added;
                int alreadyAdded = 0;
                if(ejectionList.containsKey(e.getKey())) {
                    alreadyAdded = ejectionList.get(e.getKey());
                    ejectionList.remove(e.getKey());
                }
                ejectionList.put(e.getKey(), alreadyAdded + added);
            }
        }
        
        
        
        for(Entry<ForgeDirection, Integer> e : ejectionList.entrySet()) {
            TileEntity tileSide = new Coords(tile).add(e.getKey()).getTileEntity(tile.getWorldObj());
            if (!(tileSide instanceof IFluidHandler)) {
                continue;
            }
            
            IFluidHandler handler = (IFluidHandler) tileSide;
            FluidStack fluid = tile.drain(e.getKey(), maxDrain, false);
            if (fluid == null) {
                continue;
            }
            int amount = handler.fill(e.getKey().getOpposite(), fluid, true);
            if (amount > 0) {
                tile.drain(e.getKey(), amount, true);
            }
        } 
        
        /*while(true) {
            Map<ForgeDirection, Integer> freeSpacePerSideCopy = new HashMap<ForgeDirection, Integer>();
            for(Entry<ForgeDirection, Integer> e : freeSpacePerSide.entrySet()) {
                if(ejectionList.get(e.getKey()) < maxDrain && maxEject > 0) {
                    if(e.getValue() > 1) {
                        freeSpacePerSideCopy.put(e.getKey(), e.getValue() - 1);
                    }
                    
                    maxEject--;
                    int currentAmount = ejectionList.get(e.getKey());
                    ejectionList.remove(e.getKey());
                    ejectionList.put(e.getKey(), currentAmount + 1);
                }
            }
            
            freeSpacePerSide = freeSpacePerSideCopy;
            if(maxEject == 0) {
                break;
            }
        }
        
        int freeSpaceSize;
        
        while(true) {
            freeSpaceSize = freeSpacePerSide.size();
            if(freeSpaceSize == 0) {
                return;
            }
            
            Map<ForgeDirection, Integer> freeSpacePerSideCopy = new HashMap<ForgeDirection, Integer>();
            
            maxEjectPerSide = (int) Math.floor(maxEject / freeSpacePerSide.size());
            for(Entry<ForgeDirection, Integer> e : freeSpacePerSide.entrySet()) {
                int amount = e.getValue();
                if(amount < maxEjectPerSide) {
                    maxEject -= amount;
                    ejectionList.put(e.getKey(), amount);
                }
                else {
                    freeSpacePerSideCopy.put(e.getKey(), e.getValue());
                }
            }
            
            freeSpacePerSide = freeSpacePerSideCopy;
            
            if(freeSpaceSize == freeSpacePerSide.size()) {
                for(Entry<ForgeDirection, Integer> e : freeSpacePerSide.entrySet()) {
                    if(e.getValue() >= maxEjectPerSide) {
                        ejectionList.put(e.getKey(), maxEjectPerSide);
                    }
                }
                break;
            }
        }*/
        
        
        /*for (ForgeDirection side : directions) {
            TileEntity tileSide = new Coords(tile).add(side).getTileEntity(tile.worldObj);
            if (!(tileSide instanceof IFluidHandler)) {
                continue;
            }
            
            IFluidHandler handler = (IFluidHandler) tileSide;
            FluidStack fluid = tile.drain(side, maxDrain, false);
            if (fluid == null) {
                continue;
            }
            int amount = handler.fill(side.getOpposite(), fluid, true);
            if (amount > 0) {
                tile.drain(side, amount, true);
            }
            
        }*/
    }
    
    public static IIcon getFluidIcon(Fluid fluid) {
        return getFluidIcon(fluid, false);
    }
    
    public static IIcon getFluidIcon(Fluid fluid, boolean flowing) {
        IIcon icon = flowing ? fluid.getFlowingIcon() : fluid.getStillIcon();
        
        if (icon == null) {
            Block fluidBlock = fluid.getBlock();
            if (fluidBlock != null) {
                icon = flowing ? fluidBlock.getIcon(2, 0) : fluidBlock.getIcon(0, 0);
            }
        }
        
        return icon;
    }
    
    public static boolean isTankEmpty(FluidTank tank) {
        return tank == null || isFluidStackEmpty(tank.getFluid());
    }
    
    public static boolean isFluidStackEmpty(FluidStack fluid) {
        return fluid == null || fluid.amount <= 0 || fluid.getFluid() == null;
    }
    
    public static ArrayList<String> tankToList(int capacity, FluidStack fluid) {
        return tankToList(new FluidTank(fluid, capacity), true);
    }
    
    public static ArrayList<String> tankToList(int capacity, FluidStack fluid, boolean special) {
        return tankToList(new FluidTank(fluid, capacity), special);
    }
    
    public static ArrayList<String> tankToList(FluidTank tank) {
        return tankToList(tank, true);
    }
    
    public static ArrayList<String> tankToList(FluidTank tank, boolean special) {
        
        ArrayList<String> list = new ArrayList<String>();
        
        int amount = tank.getFluidAmount();
        int capacity = tank.getCapacity();
        
        String name = isTankEmpty(tank) ? localize("hpt.tank.empty") : getFluidName(tank.getFluid().getFluid());
        
        list.add(WordUtils.capitalize(name));
        list.add(prettyPrintNumber(amount) + " /" + prettyPrintNumber(capacity) + " mB");
        
        if (special && !isTankEmpty(tank) && Utils.isShiftKeyDown()) {
            Fluid fluid = tank.getFluid().getFluid();
            list.add(localize("hpt.fluid.luminosity") + ": " + fluid.getLuminosity());
            list.add(localize("hpt.fluid.density") + ": " + fluid.getDensity() + "kg/m\u00B3"); // \u00B3 cubed sign
            list.add(localize("hpt.fluid.temperature") + ": " + fluid.getTemperature() + "\u00B0K"); // \u00B0 degree sign
            list.add(localize("hpt.fluid.viscosity") + ": " + fluid.getViscosity());
            list.add(localize("hpt.fluid." + (fluid.isGaseous()? "gaseous" : "notGaseous")));
        }
        
        return list;
    }
    
    /**
     * Method to determine the name of the fluid
     * If the fluid name is localized that will be used if not the {@link FluidRegistry}
     * @param fluid the fluid
     * @return the localized name of the Fluid
     */
    public static String getFluidName(Fluid fluid) {
        if(fluid == null) {
            return "";
        }
        
        String name = StringUtils.localize(fluid.getUnlocalizedName());
        if(name.equals(fluid.getUnlocalizedName())) {
            Block fluidBlock = fluid.getBlock();
            if (fluidBlock != null) {
                name = fluidBlock.getLocalizedName();
            }
            else {
                name = fluid.getName();
            }
        }
        
        return name;
    }
    
    /**
     * 
     * @param currentAmount the amount of mB that was already moved this tick
     * @param amountToPut the amount of mB that is supposed to be moved.
     * @return the amount of mB that can be put trough
     */
    public static int isTroughPutAllowed(int currentAmount, int amountToPut) {
        return Math.max(Math.min(Reference.DEFAULT_TROUGHPUT_PER_TICK_LIMIT - currentAmount, amountToPut), 0);
    }
    
    public static int calcRedstoneFromTank(FluidStack fluid, int cap) {
        return isFluidStackEmpty(fluid)? 0 : calcRedstoneFromTank(new FluidTank(fluid, cap));
    }
    
    public static int calcRedstoneFromTank(IFluidTank tank) {
        return tank.getFluidAmount() > 0? (int) Math.ceil(15F * tank.getFluidAmount() / tank.getCapacity()): 0;
    }
    
    public static int getFluidPumpedPerTick(TileEntityHPTBase tile, int defaultFluidPumpedPerTick) {
        if(tile instanceof IUpgradeable) {
            Upgrade up = ((IUpgradeable) tile).getUpgradeManager().getUpgrade(UpgradeType.SPEED);
            if(up != null) {
                return Math.round(defaultFluidPumpedPerTick * ((UpgradeSpeed) up).getPumpVolumePercentage());
            }
        }
        
        return defaultFluidPumpedPerTick;
    }
    
    public static boolean isWaterSource(Coords coords, World worldObj) {
        return (coords.getBlock(worldObj) == Blocks.water || coords.getBlock(worldObj) == Blocks.flowing_water) && coords.getBlockMetadata(worldObj) == 0;
    }

    public static boolean isFluidContainer(ItemStack container) {
        return !Utils.isItemStackEmpty(container) && (FluidContainerRegistry.isContainer(container) || container.getItem() instanceof IFluidContainerItem);
    }

    public static boolean isEmptyFluidContainer(ItemStack container) {
        if(!isFluidContainer(container)) {
            return false;
        }

        if(FluidContainerRegistry.isContainer(container)) {
            return FluidContainerRegistry.isEmptyContainer(container);
        }

        return isFluidStackEmpty(((IFluidContainerItem) container.getItem()).getFluid(container));
    }

    /**
     * @param container the itemstack to be tested
     * @return only true if the container is completely full
     */
    public static boolean isFilledFluidContainer(ItemStack container) {
        if(isEmptyFluidContainer(container)) {
            return false;
        }

        if(FluidContainerRegistry.isContainer(container)) {
            return FluidContainerRegistry.isFilledContainer(container);
        }

        IFluidContainerItem fluidContainerItem = ((IFluidContainerItem) container.getItem());
        return fluidContainerItem.getFluid(container).amount == fluidContainerItem.getCapacity(container);
    }

    public static void bucketInventory(TileEntity tile) {
        if(!(tile instanceof IBucketableInventory)) {
            return;
        }

        IBucketableInventory bucketableInventory = (IBucketableInventory) tile;
        IHPTInventory inv = bucketableInventory.getInventory();
        IFluidHandler fluidHandler = bucketableInventory.getFluidHandler();

        int inputSlotIndex = bucketableInventory.getFluidContainerInputSlot();
        int outputSlotIndex = bucketableInventory.getFluidContainerOutputSlot();

        ItemStack inputSlot = inv.getStackInSlot(inputSlotIndex);
        ItemStack outputSlot = inv.getStackInSlot(outputSlotIndex);
        BucketResult result = FluidUtils.bucketTile(tile, inputSlot, true);
        if(result == null || !bucketableInventory.canInsertIntoFluidContainerOutputSlot(result.getSecondaryStack())) {
            return;
        }

        FluidUtils.bucketTile(tile, inputSlot, false);

        int outputSlotStackSize = Utils.isItemStackEmpty(outputSlot) ? 0 : outputSlot.stackSize;
        inv.setInventorySlotContents(inputSlotIndex, result.getOriginalStack());
        if(!Utils.isItemStackEmpty(result.getSecondaryStack())) {
            result.getSecondaryStack().stackSize += outputSlotStackSize;
            inv.setInventorySlotContents(outputSlotIndex, result.getSecondaryStack());
        }
    }

    public static boolean bucketTile(TileEntity tile, EntityPlayer player) {
        if(player == null || Utils.isItemStackEmpty(player.getCurrentEquippedItem()) || player.isSneaking()) {
            return false;
        }

        BucketResult result = bucketTile(tile, player.getCurrentEquippedItem(), false);

        if(result == null) {
            return false;
        }

        if(!player.capabilities.isCreativeMode) {
            if(Utils.isItemStackEmpty(result.getOriginalStack())) {
                player.inventory.setInventorySlotContents(player.inventory.currentItem, result.getSecondaryStack());
            } else {
                player.inventory.setInventorySlotContents(player.inventory.currentItem, result.getOriginalStack());
                if(!player.inventory.addItemStackToInventory(result.getSecondaryStack())) {
                    player.dropPlayerItemWithRandomChoice(result.getSecondaryStack(), false);
                }
            }

            player.inventoryContainer.detectAndSendChanges();
        }

        return true;
    }

    /**
     * Uses a FluidContainer on an IFluidHandler
     * @param tile The tile that the container should be used on
     * @param container the FluidContainer
     * @param simulate when true the bucketing will only be simulated
     * @return BucketResult with the ItemStacks or null if the bucketing failed
     */
    public static BucketResult bucketTile(TileEntity tile, ItemStack container, boolean simulate) {
        if(!(tile instanceof IBucketable) || Utils.isItemStackEmpty(container)) {
            return null;
        }

        IBucketable bucketable = (IBucketable) tile;
        BucketType type = bucketable.getBucketType();
        IFluidHandler tank = bucketable.getFluidHandler();
        if(tank == null) {
            return null;
        }

        BucketResult result = new BucketResult();
        result.setOriginalStack(Utils.consumeItem(container, true));

        if(FluidContainerRegistry.isContainer(container)) {
            if(canFill(type) && FluidContainerRegistry.isFilledContainer(container)) {
                FluidStack fluid = FluidContainerRegistry.getFluidForFilledItem(container);
                int mB = tank.fill(ForgeDirection.UNKNOWN, fluid, false);

                if(fluid.amount == mB) {
                    if(!simulate) {
                        tank.fill(ForgeDirection.UNKNOWN, fluid, true);
                    }
                    result.setSecondaryStack(FluidContainerRegistry.drainFluidContainer(container));
                    return result;
                }
            } else if(canDrain(type) && FluidContainerRegistry.isEmptyContainer(container)) {
                FluidStack fluid = tank.drain(ForgeDirection.UNKNOWN, Integer.MAX_VALUE, false);
                ItemStack filledContainer = FluidContainerRegistry.fillFluidContainer(fluid, container);
                if(!Utils.isItemStackEmpty(filledContainer)) {
                    if(!simulate) {
                        tank.drain(ForgeDirection.UNKNOWN, FluidContainerRegistry.getFluidForFilledItem(filledContainer), true);
                    }
                    result.setSecondaryStack(filledContainer);
                    return result;
                }
            }
        }
        else if(container.getItem() instanceof IFluidContainerItem) {
            IFluidContainerItem fluidContainerItem = (IFluidContainerItem) container.getItem();
            ItemStack newStack = container.copy();
            newStack.stackSize = 1;
            FluidStack containedFluid = fluidContainerItem.drain(newStack, Integer.MAX_VALUE, false);

            if(canDrain(type) && (!canFill(type) || isFluidStackEmpty(containedFluid))) {
                FluidStack fluid = tank.drain(ForgeDirection.UNKNOWN, Integer.MAX_VALUE, false);
                if(!isFluidStackEmpty(fluid)) {
                    int amoundFilled = fluidContainerItem.fill(newStack, fluid, true);
                    if(!simulate) {
                        tank.drain(ForgeDirection.UNKNOWN, amoundFilled, true);
                    }
                    result.setSecondaryStack(newStack);
                    return result;
                }
            }
            else if(!isFluidStackEmpty(containedFluid) && canFill(type)) {
                int amountFilled = tank.fill(ForgeDirection.UNKNOWN, containedFluid, false);
                fluidContainerItem.drain(newStack, amountFilled, true);
                result.setSecondaryStack(newStack);
                if(!simulate) {
                    tank.fill(ForgeDirection.UNKNOWN, containedFluid, true);
                }

                return result;
            }
        }

        return null;
    }

    public static boolean canFill(BucketType type) {
        return type == BucketType.BOTH || type == BucketType.FILL;
    }

    public static boolean canDrain(BucketType type) {
        return type == BucketType.BOTH || type == BucketType.EMPTY;
    }

    public static class BucketResult {
        /**
         * The FluidContainer Stack after it was used on the IFluidHandler
         */
        protected ItemStack originalStack;
        /**
         * Any additional Items that would come out of applying the FluidContainer to the IFluidHandler
         */
        protected ItemStack secondaryStack;

        public ItemStack getOriginalStack() {
            return originalStack;
        }

        public ItemStack getSecondaryStack() {
            return secondaryStack;
        }

        public void setOriginalStack(ItemStack originalStack) {
            this.originalStack = Utils.isItemStackEmpty(originalStack)? null: originalStack.copy();
        }

        public void setSecondaryStack(ItemStack secondaryStack) {
            this.secondaryStack = Utils.isItemStackEmpty(secondaryStack) ? null : secondaryStack.copy();
        }
    }
}
