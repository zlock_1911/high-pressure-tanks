/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */

package patrick96.hptanks.plugins.buildcraft;

public class BuildcraftStatements {
    public HPTTrigger triggerFluidBelow75, triggerFluidBelow50, triggerFluidBelow25;

    public HPTTrigger triggerCapacitorBelow75, triggerCapacitorBelow50, triggerCapacitorBelow25;

    public HPTTrigger triggerCapacitorFull, triggerCapacitorEmpty, triggerCapacitorSpace, triggerCapacitorContains;

    public BuildcraftStatements() {
        triggerFluidBelow75 = new HPTTriggerFluidBelow(HPTTriggerBelow.State.Below75);
        triggerFluidBelow50 = new HPTTriggerFluidBelow(HPTTriggerBelow.State.Below50);
        triggerFluidBelow25 = new HPTTriggerFluidBelow(HPTTriggerBelow.State.Below25);

        triggerCapacitorBelow75 = new HPTTriggerCapacitorBelow(HPTTriggerBelow.State.Below75);
        triggerCapacitorBelow50 = new HPTTriggerCapacitorBelow(HPTTriggerBelow.State.Below50);
        triggerCapacitorBelow25 = new HPTTriggerCapacitorBelow(HPTTriggerBelow.State.Below25);

        triggerCapacitorFull = new HPTTriggerCapacitor(HPTTriggerCapacitor.State.Full);
        triggerCapacitorEmpty = new HPTTriggerCapacitor(HPTTriggerCapacitor.State.Empty);
        triggerCapacitorSpace = new HPTTriggerCapacitor(HPTTriggerCapacitor.State.Space);
        triggerCapacitorContains = new HPTTriggerCapacitor(HPTTriggerCapacitor.State.Contains);
    }
}
