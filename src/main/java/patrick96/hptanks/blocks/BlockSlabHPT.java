/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */

package patrick96.hptanks.blocks;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.BlockSlab;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Facing;
import net.minecraft.util.IIcon;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import patrick96.hptanks.HPTanks;

public class BlockSlabHPT extends BlockSlab {

    // The Unlocalized name without any prefixes, so that I don't have to cut them
    public final String unlocalizedNameRaw;

    protected final boolean doubleSlab;

    public final BlockHPT blockBase;

    public BlockSlabHPT(BlockHPT blockBase, boolean doubleSlab, Material material) {
        super(doubleSlab, material);
        this.doubleSlab = doubleSlab;

        setCreativeTab(HPTanks.tabsHPT);

        this.useNeighborBrightness = true;
        this.blockBase = blockBase;

        unlocalizedNameRaw = blockBase.unlocalizedNameRaw.toLowerCase() + ".slab." + (doubleSlab? "double" : "single");
        setBlockName("hpt." + unlocalizedNameRaw);

        setHardness(blockBase.getBlockHardness());
        setResistance(blockBase.getBlockResistance() / 3.0F);
        setHarvestLevel(blockBase.getHarvestTool(0), blockBase.getHarvestLevel(0));
    }

    // Icons are taken from the base block
    @Override
    public void registerBlockIcons(IIconRegister p_149651_1_) {}

    @Override
    public IIcon getIcon(int side, int meta) {
        if(this.doubleSlab && isUpperSlab(meta)) {
            side = 1;
        }

        return blockBase.getBlockTextureFromSide(side);
    }

    @SideOnly(Side.CLIENT)
    public boolean shouldSideBeRendered(IBlockAccess world, int x, int y, int z, int side) {
        if(this.doubleSlab) {
            return super.shouldSideBeRendered(world, x, y, z, side);
        } else if(side != 1 && side != 0 && !super.shouldSideBeRendered(world, x, y, z, side)) {
            return false;
        } else {
            int x1 = x + Facing.offsetsXForSide[Facing.oppositeSide[side]];
            int y1 = y + Facing.offsetsYForSide[Facing.oppositeSide[side]];
            int z1 = z + Facing.offsetsZForSide[Facing.oppositeSide[side]];
            boolean upperSlab = isUpperSlab(world.getBlockMetadata(x1, y1, z1));
            return upperSlab?
                    (side == 0 || (side == 1 && super.shouldSideBeRendered(world, x, y, z, side) || !isSingeSlab(world.getBlock(x, y, z))
                            || !isUpperSlab(world.getBlockMetadata(x, y, z)))) :
                    (side == 1 || (side == 0 && super.shouldSideBeRendered(world, x, y, z, side) || !isSingeSlab(world.getBlock(x, y, z))
                            || isUpperSlab(world.getBlockMetadata(x, y, z))));
        }
    }

    @Override
    public String func_150002_b(int p_150002_1_) {
        return getUnlocalizedName();
    }

    /**
     * Determines if the slab is in the upper part of the block space by its metadata
     * We do it here like this because the vanilla code handles it like this and the ItemSlab class relies on this
     * It basically means if the 4th bit is true the slap is an upper slab
     * @param meta the metadata of the slab
     * @return if it's an upper slab
     */
    public boolean isUpperSlab(int meta) {
        return (meta & 8) != 0;
    }

    public int getUpperSlabMeta(int meta) {
        return meta | 8;
    }

    public int getLowerSlabMeta(int meta) {
        return meta & 7;
    }

    @SideOnly(Side.CLIENT)
    private static boolean isSingeSlab(Block block) {
        return block instanceof BlockSlabHPT && !((BlockSlabHPT) block).doubleSlab;
    }

    /**
     * Gets an item for the block being called on. Args: world, x, y, z
     */
    @SideOnly(Side.CLIENT)
    public Item getItem(World world, int x, int y, int z) {
        Item item = Item.getItemFromBlock(this);
        if(item instanceof ItemBlockSlabHPT) {
            return Item.getItemFromBlock(((ItemBlockSlabHPT) item).singleSlab);
        }

        return null;
    }

    public ItemStack getStack() {
        return getStack(1, 0);
    }

    public ItemStack getStack(int size) {
        return getStack(size, 0);
    }

    public ItemStack getStack(int size, int meta) {
        return new ItemStack(this, size, meta);
    }

}
