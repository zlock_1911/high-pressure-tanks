/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.proxy;

import com.mojang.realmsclient.gui.ChatFormatting;
import cpw.mods.fml.client.registry.RenderingRegistry;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.registry.GameData;
import net.minecraft.block.Block;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.RenderGlobal;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;
import net.minecraftforge.client.MinecraftForgeClient;
import net.minecraftforge.client.event.DrawBlockHighlightEvent;
import net.minecraftforge.client.event.TextureStitchEvent;
import net.minecraftforge.event.entity.player.ItemTooltipEvent;
import org.lwjgl.opengl.GL11;
import patrick96.hptanks.client.renderer.block.BlockResinRenderer;
import patrick96.hptanks.client.renderer.block.FluidPumpRenderer;
import patrick96.hptanks.client.renderer.block.TankRenderer;
import patrick96.hptanks.client.renderer.item.ItemFluidPumpRenderer;
import patrick96.hptanks.client.util.IconProvider;
import patrick96.hptanks.init.ModBlocks;
import patrick96.hptanks.recipe.HPCutterRecipeInfo;
import patrick96.hptanks.recipe.HPTRegistry;
import patrick96.hptanks.tile.hpt.TileEntityTankComponent;
import patrick96.hptanks.util.Coords;
import patrick96.hptanks.util.Dumps;
import patrick96.hptanks.util.StringUtils;
import patrick96.hptanks.util.Utils;

public class ClientProxy extends CommonProxy {
    
    @Override
    public void postInit() {
        // Dumps only run on the client
        Dumps.dump();
    }
    
    @Override
    public void registerRenderer() {
        RenderingRegistry.registerBlockHandler(new TankRenderer());
        RenderingRegistry.registerBlockHandler(new FluidPumpRenderer());
        RenderingRegistry.registerBlockHandler(new BlockResinRenderer());
        MinecraftForgeClient.registerItemRenderer(Item.getItemFromBlock(ModBlocks.fluidPump), new ItemFluidPumpRenderer());
        
        //ClientRegistry.bindTileEntitySpecialRenderer(TileEntityFluidPump.class, new TileEntityFluidPumpRenderer());
        // ClientRegistry.bindTileEntitySpecialRenderer(TileEntityTankValve.class, new TileEntityTankValveRenderer());
    }
    
    @SubscribeEvent
    public void preStitch(TextureStitchEvent.Pre event) {
        // Items
        if(event.map.getTextureType() == 1) {
            IconProvider.registerIcons(event.map);
        }
        // Blocks
        else {
            
        }
    }
    
    @SubscribeEvent
    public void postStitch(TextureStitchEvent.Post event) {
        ModBlocks.fluidResin.setIcons(ModBlocks.blockFluidResin.getBlockTextureFromSide(0), ModBlocks.blockFluidResin.getBlockTextureFromSide(2));
    }

    @SubscribeEvent
    public void onItemTooltip(ItemTooltipEvent event) {
        if(event != null) {
            ItemStack stack = event.itemStack;
            HPCutterRecipeInfo.CutterConfiguration configuration = HPTRegistry.getCutter(stack);
            if(configuration != null) {
                event.toolTip.add(1,
                        ChatFormatting.GREEN + StringUtils.localize("misc.hpt.tier") + " " + StringUtils.numberToRomanNumeral(configuration.getTier()) + " "
                                + StringUtils.localize("misc.hpt.cutter"));
                event.toolTip.add(2, ChatFormatting.GREEN + configuration.getDamage().toString());
            }
        }
    }

    /**
     * Draws the frame around a Tank block as the whole tank
     */
    @SubscribeEvent
    public void onDrawBlockHighlightEvent(DrawBlockHighlightEvent event) {
        if(event.target.typeOfHit != MovingObjectPosition.MovingObjectType.BLOCK) {
            return;
        }

        Coords coords = new Coords(event.target.blockX, event.target.blockY, event.target.blockZ);
        EntityPlayer player = event.player;
        World world = player.getEntityWorld();
        TileEntity tile = coords.getTileEntity(world);

        if(tile instanceof TileEntityTankComponent && ((TileEntityTankComponent) tile).isPartOfMultiBlock()) {
            GL11.glDisable(GL11.GL_ALPHA_TEST);
            GL11.glEnable(GL11.GL_BLEND);
            OpenGlHelper.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA, 1, 0);
            GL11.glColor4f(0.0F, 0.0F, 0.0F, 0.4F);
            GL11.glLineWidth(2.0F);
            GL11.glDisable(GL11.GL_TEXTURE_2D);
            GL11.glDepthMask(false);
            float f1 = 0.002F;
            TileEntityTankComponent component = (TileEntityTankComponent) tile;
            Coords minCoords = ((TileEntityTankComponent) tile).getControl().getCornerBlock();
            Coords maxCoords = minCoords.add(component.getControl().getDimensions());

            AxisAlignedBB aabb = Utils.getAABB(minCoords, maxCoords);

            double d0 = player.lastTickPosX + (player.posX - player.lastTickPosX) * event.partialTicks;
            double d1 = player.lastTickPosY + (player.posY - player.lastTickPosY) * event.partialTicks;
            double d2 = player.lastTickPosZ + (player.posZ - player.lastTickPosZ) * event.partialTicks;
            RenderGlobal.drawOutlinedBoundingBox(aabb.expand(f1, f1, f1).getOffsetBoundingBox(-d0, -d1, -d2), -1);

            GL11.glDepthMask(true);
            GL11.glEnable(GL11.GL_TEXTURE_2D);
            GL11.glDisable(GL11.GL_BLEND);
            GL11.glEnable(GL11.GL_ALPHA_TEST);
            event.setCanceled(true);
        }
    }
    
}
