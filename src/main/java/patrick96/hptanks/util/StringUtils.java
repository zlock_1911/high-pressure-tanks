/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */

package patrick96.hptanks.util;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.ChatComponentTranslation;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.StatCollector;

import java.text.DecimalFormat;

public class StringUtils {

    public static String holdShiftForInfo =
            "< " + localize("hpt.holdShift0") + " " + EnumChatFormatting.DARK_GREEN + localize("hpt.holdShift1") + EnumChatFormatting.GRAY + " "
                    + localize("hpt.holdShift2") + " >";

    public static String localize(String unlocalized) {
        return StatCollector.translateToLocal(unlocalized);
    }

    public static boolean hasLocalization(String unlocalized) {
        String localized = localize(unlocalized);

        return !localized.equals(unlocalized);
    }

    public static boolean isStringEmpty(String string) {
        return isStringEmpty(string, false);
    }

    public static boolean isStringEmpty(String string, boolean trimmed) {
        return string == null || string.equals("") || (trimmed && string.trim().equals(""));
    }

    public static String prettyPrintNumber(double num) {
        return prettyPrintNumber(num, 0, true);
    }

    /**
     * Pretty prints numbers
     *
     * @param num           the number to format
     * @param decimalPlaces the amount of decimal places to be shown
     * @return the formated number as a String
     */
    public static String prettyPrintNumber(double num, int decimalPlaces, boolean trailingZeros) {
        StringBuilder format = new StringBuilder(",##0");
        if(decimalPlaces > 0) {
            format.append(".");
            for(int i = 0; i < decimalPlaces; i++) {
                format.append(trailingZeros ? "0" : "#");
            }
        }
        DecimalFormat form = new DecimalFormat(format.toString());
        return form.format(num);
    }

    /**
     * Converts an arabic numeral (num) to roman numerals
     * Numbers that are equal to or higher than 4000 won't deliver expected result (there will be as many 'M' as there are thousand in the number)
     * @param num the arabic numeral to convert
     * @return The roman numeral as String
     */
    public static String numberToRomanNumeral(int num) {
        if(num == 0) {
            return "0";
        }
        String[] roman = {"M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"};
        int[] arabic = {1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1};
        String result = "";
        for(int i = 0; i < 13; i++) {
            while(num >= arabic[i]) {
                result += roman[i];
                num -= arabic[i];
            }
        }
        return result;
    }

    public static void addChatMessage(EntityPlayer player, String unlocalizedName, Object... vars) {
        player.addChatMessage(new ChatComponentTranslation(unlocalizedName, vars));
    }

    public static void addChatMessage(EntityPlayer player, String msg) {
        addChatMessage(player, msg, false);
    }

    /**
     * @param player    the player to whom's chat the msg should be added
     * @param msg       the message
     * @param localized whether or not msg is already localized, if not it gets localized first
     */
    public static void addChatMessage(EntityPlayer player, String msg, boolean localized) {
        player.addChatMessage(new ChatComponentText(localized ? msg : localize(msg)));
    }
}
