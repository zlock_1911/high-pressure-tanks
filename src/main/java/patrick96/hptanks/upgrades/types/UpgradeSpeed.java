/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.upgrades.types;

import patrick96.hptanks.upgrades.UpgradeType;
import patrick96.hptanks.util.StringUtils;

import java.util.ArrayList;

public class UpgradeSpeed extends Upgrade {
    
    // 10^(-1/10)
    public static final float speedCoefficient = 0.79432823472428150206591828283639F;
    
    // 2^(1/5)
    public static final float pumpVolumeCoefficient = 1.14869835499703500679862694677792F;
    
    public UpgradeSpeed() {
        super(UpgradeType.SPEED);
    }
    
    @Override
    public int getLimit() {
        return 16;
    }

    @Override
    public ArrayList<String> getTooltip() {
        ArrayList<String> tooltip = super.getTooltip();
        tooltip.add(StringUtils.localize("hpt.upgrade.speed.info1") + " " + StringUtils.prettyPrintNumber(getSpeedPercentage() * 100, 2, true) + "%");
        tooltip.add(StringUtils.localize("hpt.upgrade.speed.info2") + " " + StringUtils.prettyPrintNumber(getPumpVolumePercentage() * 100, 2, true) + "%");
        return tooltip;
    }
    
    public float getSpeedPercentage() {
        return (float) Math.pow(speedCoefficient, getAmount());
    }
    
    public float getPumpVolumePercentage() {
        return (float) Math.pow(pumpVolumeCoefficient, getAmount());
    }
    
}
