/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.util;

import cpw.mods.fml.common.registry.FMLControlledNamespacedRegistry;
import cpw.mods.fml.common.registry.GameData;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.inventory.InventoryLargeChest;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntityChest;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.minecraftforge.common.util.ForgeDirection;
import net.minecraftforge.oredict.OreDictionary;
import org.lwjgl.input.Keyboard;
import patrick96.hptanks.lib.Reference;

import java.io.IOException;
import java.util.*;


public class Utils {

    public static double parseDouble(Object obj) {
        if(obj instanceof Number) {
            return (Double) obj;
        }

        if(obj instanceof Boolean) {
            return ((Boolean) obj)? 1 : 0;
        }

        if(obj instanceof String) {
            try {
                return Double.parseDouble((String) obj);
            }
            catch(NumberFormatException e) {}
        }

        return 0;
    }

    public static int parseInt(Object obj) {
        return Double.valueOf(parseDouble(obj)).intValue();
    }

    public static ItemStack consumeItem(ItemStack item) {
        return consumeItem(item, true);
    }

    /**
     * Decreases the stack size of the initial stack by one (doesn't affect the
     * initial stack).
     *
     * @param item initial stack
     * @param clone whether or not the itemStack should be copied
     * @return the initial stack minus 1
     */
    public static ItemStack consumeItem(ItemStack item, boolean clone) {
        if(!clone) {
            if(item == null) {
                return null;
            }

            if(item.stackSize == 0) {
                return item;
            }

            item.stackSize--;

            return item;
        }


        if (isItemStackEmpty(item) || item.stackSize == 1) {
            return null;
        }

        ItemStack res = item.copy();
        res.stackSize--;

        return res;
    }

    public static boolean canMergeItemStack(ItemStack stack1, ItemStack stack2) {
        return canMergeItemStack(stack1, stack2, 64);
    }

    /**
     * Determines whether or not stack1 can be put into stack2
     * @param stack1
     * @param stack2
     * @param stackLimit the max stack size 
     *  (like for inventories that limit the stack size, the limit for the item itself is already contained in the item)
     */
    public static boolean canMergeItemStack(ItemStack stack1, ItemStack stack2, int stackLimit) {
        int amount1 = Utils.isItemStackEmpty(stack1)? 0 : stack1.stackSize;
        int amount2 = Utils.isItemStackEmpty(stack2)? 0 : stack2.stackSize;
        return Utils.isItemStackEmpty(stack1)
                || (Utils.isItemStackEmpty(stack2) && amount1 <= stackLimit)
                || (areItemStacksEqualIgnoreStackSize(stack1, stack2)
                && amount1 + amount2 <= stackLimit
                && amount1 + amount2 <= stack2.getMaxStackSize());
    }

    public static boolean areStringsEqual(String string1, String string2) {
        return (string1 == null && string2 == null) || (string1 != null && string1.equals(string2));
    }

    /**
     * Determines whether or not the two ItemStacks are equal but ignores size of the stack
     *
     * @param stack1
     * @param stack2
     */
    public static boolean areItemStacksEqualIgnoreStackSize(ItemStack stack1, ItemStack stack2) {
        stack1 = stack1 == null ? null : stack1.copy();
        stack2 = stack2 == null ? null : stack2.copy();

        if(stack1 != null) {
            stack1.stackSize = 1;
        }

        if(stack2 != null) {
            stack2.stackSize = 1;
        }

        return ItemStack.areItemStacksEqual(stack1, stack2);
    }

    /**
     * Determines whether or not the two ItemStacks are equal but ignores size of the stack
     *
     * @param stack1
     * @param stack2
     */
    public static boolean areItemsEqualIgnoreStackSize(ItemStack stack1, ItemStack stack2) {
        stack1 = stack1 == null ? null : stack1.copy();
        stack2 = stack2 == null ? null : stack2.copy();

        if(stack1 != null) {
            stack1.stackSize = 1;
        }

        if(stack2 != null) {
            stack2.stackSize = 1;
        }

        return (stack1 == null && stack2 == null) || (stack1 != null && stack2 != null && stack1.isItemEqual(stack2));
    }

    /**
     * Adds the stack to inv.
     *
     * @param inv the IInventory to add the ItemStack to
     * @param stack the stack
     * @param side the side from which the inv is accessed 
     *  NOTE: If Auto-Ejecting Items from a side of a TE you have to get the opposite side from which you were ejecting from.
     * @return an ItemStack with the items that weren't added
     */
    public static ItemStack addStackToInv(IInventory inv, ItemStack stack, ForgeDirection side) {

        if(inv == null || Utils.isItemStackEmpty(stack)) {
            return stack;
        }

        if(inv instanceof TileEntityChest) {
            TileEntityChest chest = (TileEntityChest) inv;
            
            /*
             * Two new variables just that the upper chest in the gui also is the first one that stuff's getting put into.
             */
            TileEntityChest chest1 = chest;
            TileEntityChest chest2 = null;
            Coords chestCoords = new Coords(chest);
            if(!chest.adjacentChestChecked) {
                chest.checkForAdjacentChests();
            }
            if(chest.adjacentChestXNeg != null) {
                chest1 = chest.adjacentChestXNeg;
                chest2 = chest;
            }
            else if(chest.adjacentChestXPos != null) {
                chest2 = chest.adjacentChestXPos;
            }
            else if(chest.adjacentChestZNeg != null) {
                chest1 = chest.adjacentChestZNeg;
                chest2 = chest;
            }
            else if(chest.adjacentChestZPos != null) {
                chest2 = chest.adjacentChestZPos;
            }

            if(chest2 != null) {
                return addStackToInv(new InventoryLargeChest("container.chestDouble", chest1, chest2), stack, side);
            }
        }

        stack = stack.copy();
        List<Integer> accessibleSlots = new ArrayList<Integer>();

        if(inv instanceof ISidedInventory) {
            ISidedInventory iSided = (ISidedInventory) inv;
            int[] slots = iSided.getAccessibleSlotsFromSide(side.ordinal());
            if(slots != null && slots.length > 0) {
                for(int slot : slots) {
                    if(iSided.canInsertItem(slot, stack, side.ordinal())) {
                        accessibleSlots.add(slot);
                    }
                }
            }
        }
        else {
            int size = inv.getSizeInventory();
            for(int i = 0; i < size; i++) {
                if(inv.isItemValidForSlot(i, stack)) {
                    accessibleSlots.add(i);
                }
            }
        }

        int stackSizeLimit = inv.getInventoryStackLimit();

        for(int slot : accessibleSlots) {
            if(Utils.isItemStackEmpty(stack)) {
                break;
            }

            ItemStack slotStack = inv.getStackInSlot(slot);
            if(!Utils.isItemStackEmpty(slotStack)) {
                slotStack = slotStack.copy();
            }

            if(areItemStacksEqualIgnoreStackSize(stack, slotStack)) {
                if(canMergeItemStack(stack, slotStack, stackSizeLimit)) {
                    slotStack.stackSize += stack.stackSize;
                    stack = null;
                }
                else {
                    // either the item stack limit or the inventory one
                    int actualLimit = stackSizeLimit > slotStack.getMaxStackSize() ? slotStack.getMaxStackSize() : stackSizeLimit;
                    stack.stackSize += slotStack.stackSize - actualLimit;
                    slotStack.stackSize = actualLimit;

                }
            }
            else if(isItemStackEmpty(slotStack)) {
                slotStack = stack.copy();
                if(stack.stackSize <= stackSizeLimit) {
                    stack = null;
                }
                else {
                    slotStack.stackSize = stackSizeLimit;
                    stack.stackSize -= stackSizeLimit;
                }
            }

            inv.setInventorySlotContents(slot, slotStack);
            inv.markDirty();
        }

        return stack;
    }

    public static boolean isItemStackEmpty(ItemStack stack) {
        return stack == null || stack.stackSize <= 0 || (stack.getItem() == null);
    }

    public static boolean isItemStackEmpty(ItemStackIdentifier stackID) {
        if(stackID == null) {
            return true;
        }

        for(ItemStack stack : stackID.getStacks()) {
            if(stack != null) {
                return false;
            }
        }
        // A non empty oredict name could possibly lead to a valid itemStack so any oredict name counts as valid
        return stackID.getOreDict() == null;
    }

    public static void dropItemStack(World world, ItemStack item, int x, int y, int z) {
        if (item == null || item.stackSize == 0) {
            return;
        }

        float multiplier = 0.7F;
        double spawnX = x + world.rand.nextFloat() * multiplier + (1.0F - multiplier) * 0.5D;
        double spawnY = y + world.rand.nextFloat() * multiplier + (1.0F - multiplier) * 0.5D;
        double spawnZ = z + world.rand.nextFloat() * multiplier + (1.0F - multiplier) * 0.5D;
        EntityItem entityitem = new EntityItem(world, spawnX, spawnY, spawnZ, item);
        entityitem.delayBeforeCanPickup = 10;

        world.spawnEntityInWorld(entityitem);

    }

    public static String getTexture(String name) {
        return Reference.TEXTURE_LOCATION + ":" + name;
    }

    public static ResourceLocation getResourceLocation(String name) {
        return new ResourceLocation(Reference.TEXTURE_LOCATION, name);
    }

    public static ResourceLocation getBlockResourceLocationFromTexure(String name) {
        return getBlockResourceLocationFromTexure(name, "minecraft");
    }

    /**
     * Creates a resource location from a texture like "hpt:TankGlass" for blocks
     */
    public static ResourceLocation getBlockResourceLocationFromTexure(String name, String mod) {

        // 58 == ':'
        int i = name.indexOf(58);

        if(i >= 0) {
            // If the texture is not like ":name"
            if(i != 0) {
                mod = name.substring(0, i);
            }

            name = name.substring(i + 1, name.length());
        }

        return new ResourceLocation(mod.toLowerCase(), "textures/blocks/" + name + ".png");
    }

    public static boolean resourceExists(ResourceLocation location) {
        try {
            Minecraft.getMinecraft().getResourceManager().getResource(location);
            return true;
        } catch(IOException e) {
            return false;
        }
    }

    public static boolean isShiftKeyDown()
    {
        return Keyboard.isKeyDown(Keyboard.KEY_LSHIFT) || Keyboard.isKeyDown(Keyboard.KEY_RSHIFT);
    }

    public static AxisAlignedBB getAABB(Coords min, Coords max) {
        return AxisAlignedBB.getBoundingBox(min.x, min.y, min.z, max.x, max.y, max.z);
    }

    public static <String> void cleanList(List<String> list) {
        list.removeAll(Collections.singleton(null));
        list.removeAll(Collections.singleton(""));
    }

    @SideOnly(Side.CLIENT)
    public static EntityPlayer getClientPlayer() {
        return Minecraft.getMinecraft().thePlayer;
    }

    /**
     * Gets the direction to your right if you're facing dir
     * @param dir
     * @return
     */
    public static ForgeDirection getRight(ForgeDirection dir) {
        switch(dir) {
            case NORTH:
                //return ForgeDirection.WEST;
                return ForgeDirection.EAST;

            case SOUTH:
                return ForgeDirection.EAST;

            case WEST:
                return ForgeDirection.NORTH;

            case EAST:
                //return ForgeDirection.SOUTH;
                return ForgeDirection.NORTH;

            default:
                return ForgeDirection.UNKNOWN;
        }
    }

    /**
     * Gets the direction to your left if you're facing dir
     * Actually just call getRight and takes the opposite direction
     * @param dir
     * @return
     */
    public static ForgeDirection getLeft(ForgeDirection dir) {
        return getRight(dir).getOpposite();
    }

    public static List<String> getOreDictEntries(ItemStack stack) {
        int[] IDs = OreDictionary.getOreIDs(stack);
        List<String> entries = new ArrayList<String>();
        for(int ID : IDs) {
            String entry = OreDictionary.getOreName(ID);
            if(!StringUtils.isStringEmpty(entry, true)) {
                entries.add(entry);
            }
        }

        return entries;
    }

    public static String getMod(ItemStack stack) {
        if(stack == null) {
            return "";
        }

        return getMod(stack.getItem());
    }

    public static String getMod(Item item) {
        String name;
        Block block = Block.getBlockFromItem(item);
        if(block != Blocks.air) {
            name = GameData.getBlockRegistry().getNameForObject(block);
        }
        else {
            name = GameData.getItemRegistry().getNameForObject(item);
        }

        String mod = name == null? null : name.split(":")[0];

        if(StringUtils.isStringEmpty(mod)) {
            mod = "minecraft";
        }

        return mod.toLowerCase();
    }

    public static boolean belongsToMod(ItemStack stack) {
        return belongsToMod(stack, Reference.MOD_ID);
    }


    public static boolean belongsToMod(Item item, String modId) {
        return belongsToMod(new ItemStack(item), modId);
    }

    public static boolean belongsToMod(Block block, String modId) {
        return belongsToMod(new ItemStack(block), modId);
    }

    /**
     * Checks if the given itemstack belongs to the mod with the given modId.
     * Case insensitive
     * @param stack The stack to check
     * @param modId The mod the stack should belong to
     * @return If stack belongs to modId
     */
    public static boolean belongsToMod(ItemStack stack, String modId) {
        return modId != null && stack != null && Utils.getMod(stack).equals(modId.toLowerCase());
    }

    public static Map<String, List<ItemStack>> getBlocksFromMod() {
        return getBlocksFromMod(Reference.MOD_ID);
    }

    public static Map<String, List<ItemStack>> getBlocksFromMod(String modId) {
        Map<String, List<ItemStack>> stacks = new HashMap<String, List<ItemStack>>();
        FMLControlledNamespacedRegistry<Block> blockRegistry = GameData.getBlockRegistry();
        FMLControlledNamespacedRegistry<Item> itemRegistry = GameData.getItemRegistry();

        Set<String> keys = blockRegistry.getKeys();
        for(String key : keys) {
            Block block = blockRegistry.getObject(key);
            if(belongsToMod(block, modId)) {
                stacks.putAll(getKeyStackMap(key, block));
            }
        }

        return stacks;
    }

    public static Map<String, List<ItemStack>> getItemsFromMod() {
        return getItemsFromMod(Reference.MOD_ID);
    }

    public static Map<String, List<ItemStack>> getItemsFromMod(String modId) {
        Map<String, List<ItemStack>> stacks = new HashMap<String, List<ItemStack>>();
        FMLControlledNamespacedRegistry<Item> itemRegistry = GameData.getItemRegistry();

        Set<String> keys = itemRegistry.getKeys();
        for(String key : keys) {
            Item item = itemRegistry.getObject(key);
            if(belongsToMod(item, modId)) {
                stacks.putAll(getKeyStackMap(key, item));
            }
        }

        return stacks;
    }

    public static Map<String, List<ItemStack>> getKeyStackMap(String key, Block block) {
        return getKeyStackMap(key, Item.getItemFromBlock(block));
    }

    public static Map<String, List<ItemStack>> getKeyStackMap(String key, Item item) {
        Map<String, List<ItemStack>> stacks = new HashMap<String, List<ItemStack>>();
        stacks.put(key, getAllStacks(item));

        return stacks;
    }

    public static List<ItemStack> getAllStacks(Block block) {
        return getAllStacks(Item.getItemFromBlock(block));
    }

    public static List<ItemStack> getAllStacks(Item item) {
        List<ItemStack> list = new ArrayList<ItemStack>();

        if(item.getHasSubtypes()) {
            List<ItemStack> items = new ArrayList<ItemStack>();
            item.getSubItems(item, null, items);
            for(ItemStack stack : items) {
                if(!Utils.isItemStackEmpty(stack)) {
                    list.add(stack);
                }
            }
        }
        else {
            list.add(new ItemStack(item));
        }

        return list;
    }

}
