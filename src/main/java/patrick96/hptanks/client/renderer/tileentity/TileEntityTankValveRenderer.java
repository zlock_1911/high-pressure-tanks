/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.client.renderer.tileentity;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;
import patrick96.hptanks.lib.Reference;
import patrick96.hptanks.tile.hpt.TileEntityTankValve;

public class TileEntityTankValveRenderer extends TileEntitySpecialRenderer {
    
    static final ResourceLocation resource = new ResourceLocation(Reference.TEXTURE_LOCATION, "textures/blocks/TankValveOpaqueMk1.png");
    
    // static final ResourceLocation resource = new
    // ResourceLocation(Settings.TEXTURE_LOCATION,
    // "textures/blocks/Overlay/ValveOverlayOrange.png");
    
    @Override
    public void renderTileEntityAt(TileEntity tile, double x, double y, double z, float f) {
        TileEntityTankValve valve = (TileEntityTankValve) tile;
        
        Minecraft.getMinecraft().getTextureManager().bindTexture(resource);
        // Minecraft.getMinecraft().renderEngine.func_110577_a(TextureMap.field_110575_b);
        
        GL11.glPushMatrix();
        GL11.glColor3f(1.0F, 0F, 0F);
        GL11.glTranslated(x, y, z);
        GL11.glEnable(GL11.GL_BLEND);
        
        Tessellator t = Tessellator.instance;
        t.startDrawingQuads();
        
        /*
         * t.addVertexWithUV(3/16D, 3/16D, 0, 1, 1); t.addVertexWithUV(3/16D,
         * 13/16D, 0, 1, 0); t.addVertexWithUV(13/16D, 13/16D, 0, 0, 0);
         * t.addVertexWithUV(13/16D, 3/16D, 0, 0, 1);
         */
        
        t.addVertexWithUV(0, 0, 0, 1, 1);
        t.addVertexWithUV(0, 1, 0, 1, 0);
        t.addVertexWithUV(1, 1, 0, 0, 0);
        t.addVertexWithUV(1, 0, 0, 0, 1);
        
        t.draw();
        
        GL11.glPopMatrix();
    }
    
}
