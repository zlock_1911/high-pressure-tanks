/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */

package patrick96.hptanks.plugins.nei;

import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraftforge.fluids.FluidStack;
import patrick96.hptanks.blocks.BlockMachine;
import patrick96.hptanks.client.gui.GuiRubberFormer;
import patrick96.hptanks.init.ModBlocks;
import patrick96.hptanks.recipe.HPTRegistry;
import patrick96.hptanks.recipe.MachineRecipeInfo;
import patrick96.hptanks.recipe.RubberFormerRecipeInfo;

import java.awt.*;
import java.util.List;

public class RubberFormerRecipeHandler extends MachineRecipeHandler {

    @Override
    public void loadUsageRecipes(String inputId, Object... ingredients) {
        if(inputId.equals("liquid") && ingredients != null && ingredients.length > 0
                && ingredients[0] instanceof FluidStack && ((FluidStack) ingredients[0]).getFluid() == ModBlocks.fluidResin) {
            loadCraftingRecipes("rubberformer");
        } else {
            super.loadUsageRecipes(inputId, ingredients);
        }
    }

    @Override
    public Class<? extends GuiContainer> getGuiClass() {
        return GuiRubberFormer.class;
    }

    @Override
    public Point getOutputPosition() {
        return new Point(93, 47);
    }

    @Override
    public Rectangle getProgressArrowRectangle() {
        return getOffsetRectangle(57, 47, 22, 16);
    }

    @Override
    public Rectangle getFluidRectangle() {
        return getOffsetRectangle(152, 8, 16, 59);
    }

    @Override
    public FluidStack getFluidStack(int recipe) {
        return new FluidStack(ModBlocks.fluidResin, 1);
    }

    @Override
    public String getAdditionalFluidInformation(int recipe) {
        return "Used: " + getRecipeInfo(recipe).getResinUsed() + "mB";
    }

    @Override
    public BlockMachine.MachineType getMachineType() {
        return BlockMachine.MachineType.RUBBER_FORMER;
    }

    @Override
    public List<? extends MachineRecipeInfo> getRecipeList() {
        return HPTRegistry.rubberFormerRecipes;
    }

    @Override
    public RubberFormerRecipeInfo getRecipeInfo(int recipe) {
        return (RubberFormerRecipeInfo) super.getRecipeInfo(recipe);
    }
}
