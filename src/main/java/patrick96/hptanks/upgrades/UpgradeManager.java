/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.upgrades;

import io.netty.buffer.ByteBuf;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.tileentity.TileEntity;
import net.minecraftforge.common.util.Constants;
import patrick96.hptanks.core.network.IChangeable;
import patrick96.hptanks.core.power.IPowerTile;
import patrick96.hptanks.upgrades.types.Upgrade;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UpgradeManager implements IChangeable {
    
    private boolean hasChanged = false;
    
    protected TileEntity tile;
    
    // Key Upgrade Type, Value the actual Upgrade
    
    protected Map<UpgradeType, Upgrade> upgrades = new HashMap<UpgradeType, Upgrade>();
    //protected List<Upgrade> upgrades = new ArrayList<Upgrade>();
 
    public UpgradeManager(TileEntity tile) {
        this.tile = tile;
    }

    public static boolean canAcceptUpgrade(UpgradeType type, IUpgradeable upgradeable) {
        List<UpgradeType> list = upgradeable.getSupportedUpgrades();
        return list != null && list.contains(type);

    }

    public int getAmount() {
        return upgrades.size();
    }

    /**
     * @return the upgrades
     */
    public Collection<Upgrade> getUpgrades() {
        return upgrades.values();
    }
    
    public void clearUpgrades() {
        upgrades.clear();
    }
    
    /**
     * @param upgrade
     * @return the amount of upgrades that WERE added
     */
    public int addUpgrade(Upgrade upgrade) {
        if(upgrade == null || upgrade.getLimit() == 0) {
            return 0;
        }

        int added;

        int limit = upgrade.getLimit();

        markTileDirty();

        if(hasUpgrade(upgrade.getType())) {
            // TODO handle adding multiple upgrades at the same time (only add a partial stack if not all can fit)
            if(getUpgrade(upgrade.getType()).tryAddUpgrade(upgrade)) {
                markTileDirty();
                added = upgrade.getAmount();
            }
            else {
                added = 0;
            }
        }
        else if(upgrade.getAmount() > limit && limit != -1) {
            Upgrade newUpgrade = upgrade.copy();
            newUpgrade.setAmount(limit);
            upgrades.put(upgrade.getType(), newUpgrade);
            added = limit;
        }
        else {
            upgrades.put(upgrade.getType(), upgrade);
            added = upgrade.getAmount();
        }

        if(upgrade.getType() == UpgradeType.ENERGY_STORAGE && tile instanceof IPowerTile) {
            if(((IPowerTile) tile).getPowerHandler() != null) {
                ((IPowerTile) tile).getPowerHandler().updatePowerCapacity();
            }
        }

        return added;
    }
    
    public Upgrade removeUpgrade(UpgradeType type) {
        return removeUpgrade(type, 1);
    }
    
    public Upgrade removeUpgrade(UpgradeType type, int amount) {
        Upgrade original = getUpgrade(type);
        
        // if -1 remove all upgrades
        if(amount == -1) {
            amount = original.getAmount();
        }
        
        int removed = Math.min(original.getAmount(), amount);
        if(removed == original.getAmount()) {
            upgrades.remove(original.getType());
            return original.copy();
        }
        else if(removed == 0) {
            return null;
        }
        else {
            Upgrade ret = original.copy();
            ret.setAmount(removed);
            original.setAmount(original.getAmount() - removed);
            
            return ret;
        }
    }
    
    public boolean hasUpgrade(UpgradeType type) {
        return upgrades.containsKey(type);
    }
    
    public Upgrade getUpgrade(UpgradeType type) {
        if(hasUpgrade(type)) {
            return upgrades.get(type);
        }
        
        return null;
    }
    
    public int getUpgradeAmount(UpgradeType type) {
        Upgrade up = getUpgrade(type);
        if(up != null) {
            return up.getAmount();
        }
        
        return 0;
    }
    
    public void writeToNBT(NBTTagCompound compound) {
        NBTTagList types = new NBTTagList();
        
        for(Upgrade up : getUpgrades()) {
            NBTTagCompound upgrade = new NBTTagCompound();
            up.writeToNBT(upgrade);
            types.appendTag(upgrade);
        }
        
        compound.setTag("upgrades", types);
    }
    
    public void readFromNBT(NBTTagCompound compound) {
        // 10 is the type of the NBTTagCompound 
        NBTTagList types = compound.getTagList("upgrades", Constants.NBT.TAG_COMPOUND);
        for(int i = 0; i < types.tagCount(); i++) {
            NBTTagCompound upgrade = types.getCompoundTagAt(i);
            addUpgrade(Upgrade.readFromNBT(upgrade));
        }
        
        markTileDirty();
    }

    public void writePacketData(ByteBuf data) throws IOException {
        // Writes the size so the client knows how many times to read
        data.writeInt(upgrades.size());
        for(Upgrade up : getUpgrades()) {
            up.writePacketData(data);
        }
    }

    public void readPacketData(ByteBuf input) throws IOException {
        upgrades.clear();
        int count = input.readInt();
        for(int i = 0; i < count; i++) {
            addUpgrade(Upgrade.readPacketData(input));
        }
    }

    @Override
    public boolean hasChanged() {
        return hasChanged;
    }

    @Override
    public void markTileDirty() {
        hasChanged = true;
    }

    @Override
    public void markClean() {
        hasChanged = false;
    }
    
}
