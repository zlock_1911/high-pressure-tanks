/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.tile.hpt;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import io.netty.buffer.ByteBuf;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.world.World;
import net.minecraftforge.common.util.ForgeDirection;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.FluidTank;
import patrick96.hptanks.client.gui.IGuiTile;
import patrick96.hptanks.client.gui.tabs.GuiTab.TabSide;
import patrick96.hptanks.core.fluids.FluidUtils;
import patrick96.hptanks.core.fluids.IBucketableInventory;
import patrick96.hptanks.core.fluids.TankManager;
import patrick96.hptanks.core.inventory.IHPTInventory;
import patrick96.hptanks.core.inventory.InventoryHandler;
import patrick96.hptanks.core.inventory.slot.SlotFluidContainerInput;
import patrick96.hptanks.core.inventory.slot.SlotFluidContainerOutput;
import patrick96.hptanks.core.inventory.slot.SlotHPT;
import patrick96.hptanks.core.network.packets.PacketTileUpdate;
import patrick96.hptanks.core.power.EnergySystem;
import patrick96.hptanks.core.power.HPTPowerHandler;
import patrick96.hptanks.core.power.IPowerTile;
import patrick96.hptanks.lib.GuiInfo;
import patrick96.hptanks.lib.Reference;
import patrick96.hptanks.upgrades.IUpgradeable;
import patrick96.hptanks.upgrades.UpgradeManager;
import patrick96.hptanks.upgrades.UpgradeType;
import patrick96.hptanks.upgrades.types.Upgrade;
import patrick96.hptanks.util.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TileEntityTankControl extends TileEntityTankComponent implements IHPTFrameComponent, IBucketableInventory, IPowerTile, IHPTInventory, IGuiTile, IUpgradeable {

    public static final GuiInfo guiInfo = new GuiInfo(GuiInfo.MULTI_BLOCK_TANK_ID, "TankEnergy", true);

    protected UpgradeManager upgradeManager;
    public InventoryHandler inventoryHandler = new InventoryHandler(this);

    /**
     * The amount of fluid that was inside the tank when the last render update was forced
     * to reduce fps lag when rendering large tanks it will only force the rerender if the fluidAmountChanged / fluidPerLevel
     * is high enough
     */
    private int fluidAmountLastRender = 0;
    
    private Coords cornerBlock = new Coords();
    
    private Coords dimensions = new Coords();
    
    public HPTPowerHandler powerHandler = new HPTPowerHandler(this, Reference.BASIC_ENERGY_STORAGE_CAPACITY);
    
    /* Only for validation */
    private Coords iterateCoords = new Coords();

    /**
     * The coords the control block was the last time it was placed down
     * If it was never placed down before (or was picked up while completely empty) this is not initialized
     * Used to ensure that you can't move the control and this way the whole tank somewhere far away
     */
    public Coords originalCoords = new Coords();
    public ArrayList<TileEntityTankComponent> tankBlocks;
    
    /** the tabs that are currently open on each side */
    protected static Map<TabSide, Integer> openTabs = new HashMap<TabSide, Integer>();
    
    /* -- For Moving the control block -- */
    private boolean overrideWarningShown = false;
    
    public TileEntityTankControl() {
        tankManager = new TankManager(new FluidTank(0));

        inventoryHandler.setName("hptank");
        addSlot(new SlotFluidContainerInput(this, inventoryHandler.getNextAvailableId(), 94, 8));
        addSlot(new SlotFluidContainerOutput(this, inventoryHandler.getNextAvailableId(), 142, 8));
        
        upgradeManager = new UpgradeManager(this);
    }
    
    @Override
    public void updateEntity() {
        super.updateEntity();
        if (worldObj.isRemote) {
            return;
        }

        updateInventory();
        powerHandler.update();
    }

    @Override
    public GuiInfo getGuiInfo() {
        return guiInfo;
    }

    @Override
    public void validate() {
        super.validate();
        powerHandler.addToIC2Net();
    }
    
    @Override
    public void invalidate() {
        super.invalidate();
        powerHandler.removeFromIC2Net();
    }
    
    @Override
    public void markClean() {
        super.markClean();
        powerHandler.markClean();
    }
    
    @Override
    public boolean hasChanged() {
        return super.hasChanged() || powerHandler.hasChanged();
    }
    
    /**
     * Checks if there are any entites inside the tank
     *
     * @return true, if it found at least one
     */
    public boolean checkForEntities() {
        AxisAlignedBB airBlocks = AxisAlignedBB.getBoundingBox(cornerBlock.x + 0.99D, cornerBlock.y + 0.99D, cornerBlock.z + 0.99D, cornerBlock.x + dimensions.x - 0.99D, cornerBlock.y + dimensions.y - 0.99D, cornerBlock.z + dimensions.z - 0.99D);
        List entityList = worldObj.getEntitiesWithinAABB(Entity.class, airBlocks);
        
        if(entityList.size() > 0) {
            LogHelper.debug("There are Entities inside the tank!");
            return true;
        }
        
        return false;
    }
    
    public void onWrench(EntityPlayer player, ForgeDirection from) {
        super.onWrench(player, from);
        
        if (isPartOfMultiBlock() || worldObj.isRemote) {
            return;
        }

        Coords dimensionsOld = dimensions.copy();
        Coords cornerBlockOld = cornerBlock.copy();
        Coords originalCoordsOld = originalCoords.copy();

        cornerBlock.reset();

        /**
         * Checks if the controller is inside a valid multiblock
         */
        if(determineDimensions() && validateMultiBlock()) {

            /**
             * If it is and the controller was moved
             */
            if(originalCoords.initialized() && !getCoords().equals(originalCoords)) {
                // Check if the controller is still in the same tank
                if(cornerBlock.initialized() && cornerBlock.equals(cornerBlockOld) && dimensions.initialized() && dimensions.equals(dimensionsOld)) {
                    StringUtils.addChatMessage(player, "hpt.tank.controller.moved");
                }
                else {
                    // else tell the player that there is still data in the controller
                    if (!overrideWarningShown) {
                        StringUtils.addChatMessage(player, "hpt.tank.controller.containsFluid");
                        overrideWarningShown = true;
                        dimensions = dimensionsOld.copy();
                        originalCoords = originalCoordsOld.copy();
                        cornerBlock = cornerBlockOld.copy();
                        return;
                    } else {
                        overrideWarningShown = false;

                        originalCoords.reset();
                        cornerBlock.reset();
                        dimensions.reset();

                        getTankManager().reset();
                        powerHandler.reset();
                        markTileDirty();
                    }
                }
            }

            originalCoords = getCoords();

            for (TileEntityTankComponent comp : tankBlocks) {
                comp.addToMultiBlock(getCoords());
            }

            for (TileEntityTankComponent comp : tankBlocks) {
                comp.getCoords().markBlockForUpdate(worldObj);
                comp.getCoords().notifyBlockChange(worldObj, comp.getBlockType());
            }
            addToMultiBlock(getCoords());
            getCoords().markBlockForUpdate(worldObj);

            StringUtils.addChatMessage(player, "hpt.tank.construction.formed", getDimensions().x, getDimensions().z, getDimensions().y,
                                              StringUtils.prettyPrintNumber(calculateTankCapacity() / 1000F, 3, false));

        }
        else {
            StringUtils.addChatMessage(player, "hpt.tank.construction.failed");
            dimensions = dimensionsOld.copy();
            originalCoords = originalCoordsOld.copy();
            cornerBlock = cornerBlockOld.copy();
        }

    }
    
    /**
     * Checks if the block contains info that should persist when picked up
     */
    public boolean hasInfoToBePickedUp() {
        return (isPartOfMultiBlock() || originalCoords.initialized()) && !getTankManager().isEmpty();
    }

    @Override
    public void addToMultiBlock(Coords controlCoords) {
        super.addToMultiBlock(controlCoords);
        getTankManager().setCapacity(calculateTankCapacity());
        markTileDirty();
    }

    @Override
    public void removeFromMultiBlock() {
        super.removeFromMultiBlock();
        cornerBlock.reset();
        dimensions.reset();
        getTankManager().setCapacity(0);
    }

    public int getFluidPerAirBlock() {
        return Reference.TANK_STORAGE_PER_AIR_BLOCK;
    }

    public int calculateTankCapacity() {
        return (getDimensions().x - 2) * (getDimensions().y - 2) * (getDimensions().z - 2) * getFluidPerAirBlock();
    }

    @Override
    public int getTankCapacity() {
        return getTankManager().getCapacity();
    }

    // This has to stay here because super.getTankManager refers to this method
    @Override
    public TankManager getTankManager() {
        return tankManager;
    }

    @Override
    public void writeToNBT(NBTTagCompound compound) {
        super.writeToNBT(compound);
        upgradeManager.writeToNBT(compound);
        powerHandler.writeToNBT(compound);
        dimensions.writeToNBT(compound, "dimensions");

        originalCoords.writeToNBT(compound, "originalCoords");

        compound.setBoolean("overrideWarningShown", overrideWarningShown);

        cornerBlock.writeToNBT(compound, "cornerBlock");

        inventoryHandler.writeToNBT(compound);
    }

    @Override
    public void readFromNBT(NBTTagCompound compound) {
        super.readFromNBT(compound);

        upgradeManager.readFromNBT(compound);
        powerHandler.loadFromNBT(compound);
        dimensions.loadFromNBT(compound, "dimensions");

        originalCoords.loadFromNBT(compound, "originalCoords");

        cornerBlock = cornerBlock.loadFromNBT(compound, "cornerBlock");
        if(cornerBlock.initialized() && dimensions.initialized()) {
            getTankManager().setCapacity(calculateTankCapacity());
        }
        else {
            getTankManager().setCapacity(0);
        }

        overrideWarningShown = compound.getBoolean("overrideWarningShown");


        inventoryHandler.readFromNBT(compound);
    }

    /* -- Control Mechanics -- */
    public void updateInventory() {
        if (!isPartOfMultiBlock()) {
            return;
        }

        FluidUtils.bucketInventory(this);
    }

    @Override
    public void handleUpdatePacket(PacketTileUpdate packet) throws IOException {
        super.handleUpdatePacket(packet);

        ByteBuf data = packet.input;
        upgradeManager.readPacketData(data);
        powerHandler.readData(data);
        getTankManager().setCapacity(data.readInt());
        dimensions.readData(data);
        cornerBlock.readData(data);
        originalCoords.readData(data);

        if(isPartOfMultiBlock() && (Math.abs(getTankManager().getFluidAmount() - fluidAmountLastRender) / getFluidPerLevel() > 1 / 160D || (getTankManager().getFluidAmount() != 0 && fluidAmountLastRender == 0) || (getTankManager().getFluidAmount() == 0 && fluidAmountLastRender != 0))) {
            getCoords().markBlockForRenderUpdate(worldObj);
            fluidAmountLastRender = getTankManager().getFluidAmount();
        }
    }

    @Override
    public void writePacketData(ByteBuf data) throws IOException {
        super.writePacketData(data);

        upgradeManager.writePacketData(data);
        powerHandler.writeData(data);
        data.writeInt(getTankCapacity());
        dimensions.writeData(data);
        cornerBlock.writeData(data);
        originalCoords.writeData(data);
    }

    /* IInventory Functions */
    public void addSlot(SlotHPT slot) {
        inventoryHandler.addSlot(slot);
    }

    public InventoryHandler getInventoryHandler() {
        return inventoryHandler;
    }

    @Override
    public int getSizeInventory() {
        return inventoryHandler.getSizeInventory();
    }

    @Override
    public ItemStack getStackInSlot(int i) {
       return inventoryHandler.getStackInSlot(i);
    }

    @Override
    public ItemStack decrStackSize(int i, int amount) {
        return inventoryHandler.decrStackSize(i, amount);
    }

    @Override
    public ItemStack getStackInSlotOnClosing(int i) {
        return inventoryHandler.getStackInSlotOnClosing(i);
    }

    @Override
    public void setInventorySlotContents(int i, ItemStack itemstack) {
        inventoryHandler.setInventorySlotContents(i, itemstack);
    }

    @Override
    public String getInventoryName() {
        return inventoryHandler.getInvName();
    }

    @Override
    public boolean hasCustomInventoryName() {
        return inventoryHandler.isInvNameLocalized();
    }

    @Override
    public int getInventoryStackLimit() {
        return inventoryHandler.getInventoryStackLimit();
    }

    @Override
    public void openInventory() {}

    @Override
    public void closeInventory() {}

    @Override
    public boolean isUseableByPlayer(EntityPlayer entityplayer) {
        return isPartOfMultiBlock() && entityplayer.getDistanceSq(xCoord + 0.5, yCoord + 0.5, zCoord + 0.5) <= 860;
    }

    @Override
    public boolean isItemValidForSlot(int i, ItemStack itemStack) {
        return false;
    }

    /* -- IFluidHandler Methods */
    public int fill(ForgeDirection from, FluidStack resource, boolean doFill) {
        if (FluidUtils.isFluidStackEmpty(resource) || !canFill(from, resource.getFluid())) {
            return 0;
        }

        int amount = 0;
        double energyConsumed, energyDrained;

        if (!doFill) {
            amount = getTankManager().fill(from, resource, false);
            energyConsumed = amount * Reference.TANK_ENERGY_PER_MB_IN;
            energyDrained = powerHandler.drainEnergy(energyConsumed, false);
            if (Math.abs(energyDrained - energyConsumed) > Reference.TANK_ENERGY_PER_MB_IN) {
                return 0;
            }
        } else if (fill(from, resource, false) > 0) {
            amount = getTankManager().fill(from, resource, true);
            energyConsumed = amount * Reference.TANK_ENERGY_PER_MB_IN;
            powerHandler.drainEnergy(energyConsumed, true);
        }
        
        return amount;
        
    }

    public FluidStack drain(ForgeDirection from, int maxDrain, boolean doDrain) {
        if(!canDrain(from, null)) {
            return null;
        }

        return getTankManager().drain(from, maxDrain, doDrain);
    }

    public FluidStack drain(ForgeDirection from, FluidStack resource, boolean doDrain) {
        if(!FluidUtils.isFluidStackEmpty(resource) && !canDrain(from, resource.getFluid())) {
            return null;
        }

        return getTankManager().drain(from, resource, doDrain);
    }

    public boolean canDrain(ForgeDirection from, Fluid fluid) {
        return isPartOfMultiBlock();
    }

    public boolean canFill(ForgeDirection from, Fluid fluid) {
        return isPartOfMultiBlock();
    }

    @Override
    public boolean shouldHandleTankManager() {
        return true;
    }

    /* Validation Methods */
    public boolean validateMultiBlock() {
        boolean checkHullSuccess = checkHull();
        
        if (!checkHullSuccess || checkForEntities()) {
            invalidateMultiBlock();
            return false;
        }
        
        return true;
    }
    
    public void invalidateMultiBlock() {
        
        tankBlocks = new ArrayList<TileEntityTankComponent>();
        
        for (int ix = 0; ix < getDimensions().x; ix++) {
            for (int iy = 0; iy < getDimensions().y; iy++) {
                for (int iz = 0; iz < getDimensions().z; iz++) {
                    Coords blockCoords = getCornerBlock().x(ix).y(iy).z(iz);
                    
                    boolean[] hullPostionArray = new boolean[] { iy == 0, ix == 0, iz == 0, iy == getDimensions().y - 1, ix == getDimensions().x - 1,
                            iz == getDimensions().z - 1 };
                    int truesHull = 0;

                    for (boolean hullPostion : hullPostionArray) {
                        if (hullPostion) {
                            truesHull++;
                        }
                    }
                    
                    if (truesHull >= 1 || truesHull <= 3) {
                        if (Helper.isComponent(worldObj, blockCoords) && !Helper.isControl(worldObj, blockCoords)) {
                            tankBlocks.add((TileEntityTankComponent) blockCoords.getTileEntity(worldObj));
                        }
                    }
                    
                }
            }
        }
        
        for (int i = 0; i < getSizeInventory(); i++) {
            ItemStack stack = getStackInSlotOnClosing(i);
            
            Utils.dropItemStack(worldObj, stack, xCoord, yCoord, zCoord);
        }
        
        removeFromMultiBlock();
        
        for (TileEntityTankComponent comp : tankBlocks) {
            comp.removeFromMultiBlock();
        }
        
        for (TileEntityTankComponent comp : tankBlocks) {
            comp.getCoords().markBlockForUpdate(worldObj);
            comp.getCoords().notifyBlockChange(worldObj, comp.getBlockType());
        }
        
    }
    
    private Coords determineCornerBlock() {
        
        iterateCoords = getCoords();
        
        if (checkXAxis(getCoords())) {
            iterateXAxis(Reference.TANK_MAX_SIZE);
            iterateYAxis(Reference.TANK_MAX_SIZE);
            iterateZAxis(Reference.TANK_MAX_SIZE);
        } else if (checkYAxis(getCoords())) {
            iterateYAxis(Reference.TANK_MAX_SIZE);
            iterateXAxis(Reference.TANK_MAX_SIZE);
            iterateZAxis(Reference.TANK_MAX_SIZE);
        } else if (checkZAxis(getCoords())) {
            iterateZAxis(Reference.TANK_MAX_SIZE);
            iterateXAxis(Reference.TANK_MAX_SIZE);
            iterateYAxis(Reference.TANK_MAX_SIZE);
        }
        
        return iterateCoords;
        
    }
    
    private boolean checkHull() {
        tankBlocks = new ArrayList<TileEntityTankComponent>();
        
        for (int iy = -1; iy < getDimensions().y + 1; iy++) {
            for (int ix = -1; ix < getDimensions().x + 1; ix++) {
                for (int iz = -1; iz < getDimensions().z + 1; iz++) {
                    Coords blockCoords = getCornerBlock().x(ix).y(iy).z(iz);
                    
                    boolean[] surroundingsPostionArray = new boolean[] { iy == -1, ix == -1, iz == -1, iy == getDimensions().y, ix == getDimensions().x,
                            iz == getDimensions().z };
                    int truesSurrounding = 0;

                    for (boolean surroundingsPostion : surroundingsPostionArray) {
                        if (surroundingsPostion) {
                            truesSurrounding++;
                        }
                    }
                    
                    if (truesSurrounding == 1) {
                        /*
                         * If a block is right next to the tank check if it is a
                         * tank component
                         */
                        if (Helper.isComponent(worldObj, blockCoords)) {
                            LogHelper.debug(blockCoords.getBlock(worldObj).getLocalizedName() + " is in the way");
                            return false;
                        }
                        continue;
                    } else if (truesSurrounding == 2 || truesSurrounding == 3) {
                        continue;
                    }
                    
                    boolean[] hullPostionArray = new boolean[] { iy == 0, ix == 0, iz == 0, iy == getDimensions().y - 1, ix == getDimensions().x - 1,
                            iz == getDimensions().z - 1 };
                    int truesHull = 0;

                    for (boolean hullPostion : hullPostionArray) {
                        if (hullPostion) {
                            truesHull++;
                        }
                    }
                    
                    if (truesHull == 1) {
                        // The Block is in the walls
                        if (Helper.isAllowedInWalls(worldObj, blockCoords)) {
                            tankBlocks.add((TileEntityTankComponent) blockCoords.getTileEntity(worldObj));
                        } else {
                            if (blockCoords.getBlock(worldObj) != null) {
                                LogHelper.debug(blockCoords.getBlock(worldObj).getLocalizedName() + " is not allowed in walls");
                            }
                            return false;
                        }
                        continue;
                    } else if (truesHull == 2 || truesHull == 3) {
                        // The Block is in the frame
                        if (Helper.isAllowedInFrame(worldObj, blockCoords)) {
                            if (Helper.isControl(worldObj, blockCoords)) {
                                if (!getCoords().equals(blockCoords)) {
                                    LogHelper.debug("Multiple Controls");
                                    return false;
                                }
                            } else {
                                tankBlocks.add((TileEntityTankComponent) blockCoords.getTileEntity(worldObj));
                            }
                            
                        } else {
                            if (blockCoords.getBlock(worldObj) != null) {
                                LogHelper.debug(blockCoords.getBlock(worldObj).getLocalizedName() + " is not allowed in the frame");
                            }
                            return false;
                        }
                        
                        continue;
                    }
                    
                    if (ix > 0 && ix < getDimensions().x - 1 && ix > 0 && iy < getDimensions().y - 1 && iz > 0 && iz < getDimensions().z - 1
                            && !worldObj.isAirBlock(blockCoords.x, blockCoords.y, blockCoords.z)) {
                        // There is something else than air inside the tank
                        
                        if (blockCoords.getBlock(worldObj) != null) {
                            LogHelper.debug(blockCoords.getBlock(worldObj).getLocalizedName() + " is not allowed inside the tank");
                        }
                        return false;
                    }
                    
                }
            }
        }
        
        return true;
    }
    
    private boolean checkXAxis(Coords coords) {
        return Helper.isComponent(worldObj, coords.x(-1));
    }
    
    private boolean checkYAxis(Coords coords) {
        return Helper.isComponent(worldObj, coords.y(-1));
    }
    
    private boolean checkZAxis(Coords coords) {
        return Helper.isComponent(worldObj, coords.z(-1));
    }
    
    private void iterateXAxis(int maxSize) {
        for (int i = 0; i < maxSize - 1; i++) {
            Coords coords = iterateCoords.x(-1);
            if (Helper.isComponent(worldObj, coords)) {
                iterateCoords = iterateCoords.x(-1);
                continue;
            }
            break;
        }
    }
    
    private void iterateYAxis(int maxSize) {
        for (int i = 0; i < maxSize - 1; i++) {
            Coords coords = iterateCoords.y(-1);
            if (Helper.isComponent(worldObj, coords)) {
                iterateCoords = iterateCoords.y(-1);
                continue;
            }
            break;
        }
    }
    
    private void iterateZAxis(int maxSize) {
        for (int i = 0; i < maxSize - 1; i++) {
            Coords coords = iterateCoords.z(-1);
            if (Helper.isComponent(worldObj, coords)) {
                iterateCoords = iterateCoords.z(-1);
                continue;
            }
            break;
        }
    }
    
    private boolean determineDimensions() {
        dimensions.init(1, 1, 1);
        
        for (int i = 1; i < Reference.TANK_MAX_SIZE; i++) {
            
            if (dimensions.x == i && Helper.isAllowedInFrame(worldObj, getCornerBlock().x(i))) {
                dimensions = dimensions.x(1);
            }
            
            if (dimensions.y == i && Helper.isAllowedInFrame(worldObj, getCornerBlock().y(i))) {
                dimensions = dimensions.y(1);
            }
            
            if (dimensions.z == i && Helper.isAllowedInFrame(worldObj, getCornerBlock().z(i))) {
                dimensions = dimensions.z(1);
            }
        }
        
        return ValidationHelper.hasProperDimensions(dimensions, Reference.TANK_MIN_SIZE, Reference.TANK_MAX_SIZE);
        
    }
    
    public double getFluidPerLevel() {
        return (double) getTankCapacity() / (double) (getDimensions().y - 2);
    }
    
    public Coords getDimensions() {
        if (!dimensions.initialized()) {
            determineDimensions();
        }
        return dimensions;
    }
    
    public Coords getCornerBlock() {
        if(!cornerBlock.initialized()) {
            cornerBlock = determineCornerBlock();
        } 
        return cornerBlock;
    }
    
    public int getFluidAmount() {
        if (!isPartOfMultiBlock() || getTankManager().isEmpty()) {
            return 0;
        }
        
        return getTankManager().getFluidAmount();
    }
    
    public int getFluidLevel() {
        return (int) Math.ceil(getFluidAmount() / getFluidPerLevel());
    }

    @Override
    public boolean acceptsEnergyFrom(TileEntity emitter, ForgeDirection direction) {
        return true;
    }
    
    @Override
    public double getDemandedEnergy() {
        return powerHandler.getEnergyRequested(EnergySystem.EU);
    }
    
    @Override
    public double injectEnergy(ForgeDirection directionFrom, double amount, double voltage) {
        return powerHandler.addEu(amount);
    }
    
    @Override
    public int getSinkTier() {
        return powerHandler.getIC2Voltage();
    }
    
    @Override
    public HPTPowerHandler getPowerHandler() {
        return powerHandler;
    }

    /* -- CoFH Core IEnergyHandler -- */
    
    @Override
    public int receiveEnergy(ForgeDirection from, int maxReceive, boolean simulate) {
        return (int) powerHandler.addEnergy(EnergySystem.RF, maxReceive, !simulate);
    }

    @Override
    public int extractEnergy(ForgeDirection from, int maxExtract, boolean simulate) {
        /* No Energy should be extracted */
        return 0;
    }

    @Override
    public boolean canConnectEnergy(ForgeDirection from) {
        return true;
    }

    @Override
    public int getEnergyStored(ForgeDirection from) {
        return (int) powerHandler.getEnergyStored(EnergySystem.RF);
    }

    @Override
    public int getMaxEnergyStored(ForgeDirection from) {
        return (int) powerHandler.getMaxEnergyStored(EnergySystem.RF);
    }

    @Override
    public void handleGuiButtonClick(short buttonId) {}

    @Override
    public int getComparatorOverride(int side) {
        return isPartOfMultiBlock()? FluidUtils.calcRedstoneFromTank(getTankManager().getTank()) : 0;
    }

    @Override
    public List<UpgradeType> getSupportedUpgrades() {
        return new ArrayList<UpgradeType>() {{
            add(UpgradeType.ENERGY_STORAGE);
        }};
    }

    @Override
    public int applyUpgrade(Upgrade upgrade) {
        return upgradeManager.addUpgrade(upgrade);
    }

    @Override
    public UpgradeManager getUpgradeManager() {
        return upgradeManager;
    }
    
    @Override
    public Map<TabSide, Integer> getOpenTabs() {
        return openTabs;
    }
    
    @Override
    public void setOpenTabs(Map<TabSide, Integer> tabs) {
        openTabs = tabs;
    }

    @Override
    public World getWorld() {
        return getWorldObj();
    }

    @Override
    public IHPTInventory getInventory() {
        return this;
    }

    @Override
    public int getFluidContainerInputSlot() {
        return 0;
    }

    @Override
    public int getFluidContainerOutputSlot() {
        return 1;
    }

    @Override
    public boolean canInsertIntoFluidContainerOutputSlot(ItemStack item) {
        ItemStack stack = getStackInSlot(1);
        return Utils.canMergeItemStack(item, stack, getInventoryStackLimit());
    }
}
