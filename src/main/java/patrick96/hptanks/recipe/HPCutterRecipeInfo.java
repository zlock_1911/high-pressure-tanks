/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */

package patrick96.hptanks.recipe;

import net.minecraft.item.ItemStack;
import patrick96.hptanks.util.ItemStackIdentifier;
import patrick96.hptanks.util.StringUtils;

public class HPCutterRecipeInfo extends MachineRecipeInfo  {

    public final int minimalCutterTier;

    public HPCutterRecipeInfo(ItemStack input, ItemStack result, int minimalCutterTier, int ept, int time) {
        this(new ItemStackIdentifier(input), result, minimalCutterTier, ept, time);
    }

    public HPCutterRecipeInfo(ItemStackIdentifier input, ItemStack result, int minimalCutterTier, int ept, int time) {
        super(input, result, ept, time);

        this.minimalCutterTier = Math.max(minimalCutterTier, 1);
    }

    @Override
    public boolean hasStackInput() {
        return true;
    }

    @Override
    public boolean hasStackResult() {
        return true;
    }

    public static class CutterConfiguration {

        /**
         * The Cutter Tier of this item should be higher than 0
         */
        protected final int tier;
        /**
         * How the Cutter takes damage when used.
         */
        protected CutterDamage damage;

        /**
         * Coefficient for the Energy Per tick and the time it takes for the completion of the recipe for this cutter
         */
        public final float baseEnergyCoefficient, baseTimeCoefficient;

        public CutterConfiguration(int tier, CutterDamage damage, float baseEnergyCoefficient, float baseTimeCoefficient) {
            this.tier = Math.max(tier, 1);

            this.damage = damage;

            this.baseEnergyCoefficient = baseEnergyCoefficient > 0? baseEnergyCoefficient : 1;
            this.baseTimeCoefficient = baseTimeCoefficient > 0? baseTimeCoefficient : 1;
        }

        public CutterDamage getDamage() {
            return damage;
        }

        public void setDamage(CutterDamage damage) {
            this.damage = damage;
        }

        public int getTier() {
            return tier;
        }

        @Override
        public boolean equals(Object obj) {
            if(obj instanceof CutterConfiguration) {
                CutterConfiguration cutter = (CutterConfiguration) obj;
                return cutter.getTier() == getTier() && cutter.getDamage() == getDamage() && cutter.baseEnergyCoefficient == baseEnergyCoefficient && cutter.baseTimeCoefficient == baseTimeCoefficient;
            }

            return false;
        }
    }

    /**
     * Enum describing how a cutter takes damage
     */
    public enum CutterDamage {
        // The Cutter takes damage
        DAMAGE,
        // The Cutter is destroyed after use
        DESTROY,
        // The Cutter isn't used up at all
        UNBREAKABLE;

        public String toString() {
            return StringUtils.localize("misc.hpt.cutter." + name().toLowerCase());
        }
    }
}
