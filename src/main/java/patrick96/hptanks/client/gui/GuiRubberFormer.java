/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */

package patrick96.hptanks.client.gui;

import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;
import patrick96.hptanks.client.gui.rect.GuiRectangleFluid;
import patrick96.hptanks.client.gui.rect.GuiRectangleProgressArrow;
import patrick96.hptanks.inventory.ContainerRubberFormer;
import patrick96.hptanks.recipe.HPTRegistry;
import patrick96.hptanks.tile.machine.TileEntityRubberFormer;

public class GuiRubberFormer extends GuiMachine {

    public final TileEntityRubberFormer rubberFormer;

    protected float currentScroll = 0F;
    /**
     * True if the scrollbar is being dragged
     */
    protected boolean isScrolling;
    /**
     * True if the left mouse button was held down last time drawScreen was called.
     */
    protected boolean wasClicking;

    public GuiRubberFormer(InventoryPlayer invPlayer, TileEntityRubberFormer machine) {
        super(invPlayer, machine, new ContainerRubberFormer(invPlayer, machine));

        xSize = 216;

        rubberFormer = machine;

        GuiRectangleProgressArrow progressArrow = new GuiRectangleProgressArrow(this, machine, 57, 47);
        GuiRectangleFluid fluidRect = new GuiRectangleFluid(this, rubberFormer.getTankManager(), 152, 8, GuiRectangleFluid.FluidScaleType.SMALL);

        rectManager.addRect(progressArrow);
        rectManager.addRect(fluidRect);
    }

    public int getNumVisibleSlots() {
        return ((IInventory) inventorySlots).getSizeInventory();
    }

    public int getNumRecipes() {
        return HPTRegistry.rubberFormerRecipes.size();
    }

    public boolean needsScrollBars() {
        return getNumRecipes() > getNumVisibleSlots();
    }

    public int getNumScrollPositions() {
        return getNumRecipes() - getNumVisibleSlots();
    }

    public float getCurrentScroll() {
        return currentScroll;
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float opacity, int x, int y) {
        super.drawGuiContainerBackgroundLayer(opacity, x, y);
        bindDefaultTexture();
        drawTexturedModalRect(guiLeft + 196, guiTop + 12 + (int) (currentScroll * 127), 216 + (needsScrollBars()? 0 : 12), 0, 12, 15);
    }

    @Override
    protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY) {
        ContainerRubberFormer container = (ContainerRubberFormer) inventorySlots;
        Slot slot = container.getSelectedStackSlot();
        if(slot != null) {
            GL11.glEnable(GL11.GL_LIGHTING);
            drawRect(slot.xDisplayPosition, slot.yDisplayPosition, slot.xDisplayPosition + 16, slot.yDisplayPosition + 16, 0xFFFF7F00);
            GL11.glDisable(GL11.GL_LIGHTING);
        }
        super.drawGuiContainerForegroundLayer(mouseX, mouseY);
    }

    public void drawScreen(int mouseX, int mouseY, float opacity) {
        boolean mouseClicked = Mouse.isButtonDown(0);
        int xPos = this.guiLeft + 196;
        int yPos = this.guiTop + 12;
        int xPos2 = xPos + 12;
        int yPos2 = yPos + 142;

        if(!this.wasClicking && mouseClicked && mouseX >= xPos && mouseY >= yPos && mouseX <= xPos2 && mouseY <= yPos2) {
            this.isScrolling = this.needsScrollBars();
        }

        if(!mouseClicked) {
            this.isScrolling = false;
        }

        this.wasClicking = mouseClicked;

        if(this.isScrolling) {
            this.currentScroll = ((float) (mouseY - yPos) - 7.5F) / ((float) (yPos2 - yPos) - 15.0F);

            if(this.currentScroll < 0.0F) {
                this.currentScroll = 0.0F;
            }

            if(this.currentScroll > 1.0F) {
                this.currentScroll = 1.0F;
            }

            ((ContainerRubberFormer) this.inventorySlots).scrollTo(currentScroll, getNumScrollPositions());
        }

        super.drawScreen(mouseX, mouseY, opacity);
    }

    @Override
    public void handleMouseInput() {
        super.handleMouseInput();
        int dWheel = Mouse.getEventDWheel();

        if(dWheel != 0 && this.needsScrollBars()) {
            int numScrollPositions = getNumScrollPositions();

            if(dWheel > 0) {
                dWheel = 1;
            }

            if(dWheel < 0) {
                dWheel = -1;
            }

            this.currentScroll = (this.currentScroll - dWheel * 1.0F / numScrollPositions);

            if(this.currentScroll < 0.0F) {
                this.currentScroll = 0.0F;
            }

            if(this.currentScroll > 1.0F) {
                this.currentScroll = 1.0F;
            }

            ((ContainerRubberFormer) this.inventorySlots).scrollTo(currentScroll, numScrollPositions);
        }
    }
}