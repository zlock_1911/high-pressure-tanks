/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.upgrades.types;

import cpw.mods.fml.common.network.ByteBufUtils;
import io.netty.buffer.ByteBuf;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import patrick96.hptanks.core.network.IChangeable;
import patrick96.hptanks.init.ModItems;
import patrick96.hptanks.upgrades.IUpgradeable;
import patrick96.hptanks.upgrades.UpgradeType;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import static patrick96.hptanks.util.StringUtils.localize;

/** TODO Upgrade Overhaul:
 * Packets should only contain ordinal value UpgradeType.values[ordinal], NBT should still contain the name incase the order changes
 * Find a better way to do getUpgrade
 * UpgradeType, ItemUpgrade and Upgrade is a bit much, find a way to merge them
 * Maybe remove IChangeable
 */
public class Upgrade implements IChangeable {
    
    /**
     * The name of the upgrade, used as identifier
     */
    protected final UpgradeType type;
    
    protected ItemStack tempStack = null;
    
    protected NBTTagCompound info;
    
    protected int amount = 1;
    
    protected boolean hasChanged = false;
    
    public Upgrade(UpgradeType type) {
        this.type = type;
        this.info = new NBTTagCompound();
    }
    
    public Upgrade copy() {
        Upgrade up = getUpgrade(getType());

        if(up == null) {
            return null;
        }

        up.setAmount(getAmount());
        up.setInfo(getInfo());
        
        return up;
    }
    
    /**
     * Creates a list of Strings to be used as tooltips
     * It should only contain the info the name should not be in there
     * @return only the info not the actual name
     */
    public ArrayList<String> getTooltip() {
        int limit = getLimit();
        String limitInfo = "";
        
        if(limit == -1) {
            limitInfo = " (" + localize("hpt.upgrade.limit.infinite") + ")";
        }
        else if(limit == 0) {
            limitInfo = " (" + localize("hpt.upgrade.limit.cannotBeApplied") + ")";
        }
        
        return new ArrayList<String>(Arrays.asList(localize("hpt.upgrade.amount") + ": " + amount, localize("hpt.upgrade.limit") + ": " + limit + limitInfo));
    }
            
    /**
     * 
     * @return the max amount of this upgrade allowed in one manager default 1, -1 for infinite, 
     * 0 for none (used for upgrades that can't directly be applied to machines but are used in other upgrades like the side upgrade). 
     * This limits only how many of the exact same upgrade can go into one block. (Different info counts as different upgrade).
     */
    public int getLimit() { 
        return 1;
    }
    
    /**
     * @return the amount
     */
    public int getAmount() {
        return amount;
    }
    
    /**
     * @return the info
     */
    public NBTTagCompound getInfo() {
        return info;
    }
    
    /**
     * @param amount the amount to set
     */
    public void setAmount(int amount) {
        this.amount = amount;
        markTileDirty();
    }
    
    /**
     * @param info the info to set
     */
    public void setInfo(NBTTagCompound info) {
        this.info = info;
        markTileDirty();
    }
    
    public void add() {
        amount++;
        markTileDirty();
    }
    
    public void remove() {
        amount--;
        markTileDirty();
    }
           
    public void writeToNBT(NBTTagCompound compound) {
        compound.setString("name", getName());
        compound.setInteger("amount", getAmount());
        compound.setTag("info", info);
    }
        
    /**
     * Method to read the info of an upgrade.
     * This upgrade needs to be implemented in all Upgrade classes of upgrades that add info
     */
    public void readInfoFromNBT(NBTTagCompound compound) {
        markTileDirty();
    }
    
    public static Upgrade readFromNBT(NBTTagCompound compound) {
        Upgrade upgrade = getUpgrade(UpgradeType.valueOf(compound.getString("name")));

        if(upgrade == null) {
            return null;
        }

        upgrade.amount = compound.getInteger("amount");
        upgrade.info = compound.getCompoundTag("info");
        
        return upgrade;
    }
    
    public static Upgrade getUpgrade(UpgradeType type) {
        switch(type) {
            case SPEED:
                return new UpgradeSpeed();
            
            case ENERGY_STORAGE:
                return new UpgradeEnergyStorage();
                
            case IC2:
                return new UpgradeIC2();
                
            case FLUID_AUTO_EJECT:
            case ITEM_AUTO_EJECT:
                return new UpgradeAutoEject(type);
        }

        return null;
    }
    
    public ItemStack getItemStack() {
        if(tempStack == null) {
            ItemStack stack = new ItemStack(ModItems.upgrade, amount, type.ordinal());
            if(!info.hasNoTags()) {
                stack.setTagCompound(getInfo());
            }
            return stack;
            }
        else {
            return tempStack;
        }
    }
            
    /**
     * Whether or not this upgrade can be configured
     */
    public boolean isConfigurable() {
        return false;
    }

    /**
     * called by ItemUpgrade when shift right-clicked used to open GUIs and stuff. 
     * Parameters should be about the same as the onItemUse method so the upgrade can use the player instance and other variables.
     */
    public void configure() {} 

    public String getName() {
        return type.name();
    }
    
    public UpgradeType getType() {
        return UpgradeType.valueOf(getName());
    }
    
    /**
     * Used for upgrades that have some special rules for being able to be added to a tile
     * @param tile the tileentity that should be tested
     */
    public boolean canApplyToTile(IUpgradeable tile) {
        return true;
    }
    
    
    public boolean isTheSame(Upgrade upgrade) {
        return upgrade != null && isTheSame(upgrade.type);
    }
    
    public boolean isTheSame(UpgradeType type) {
        return type != null && type == getType();
    }
    
    /**
     * Determines if the given upgrade is the same and checks that the amount wouldn't exceed the limit.
     * Also if an upgrade is configurable it CANNOT have more than one
     * @param upgrade the upgrade to add
     * @return whether or not the upgrade can be added to this upgrade
     */
    public boolean canUpgradeBeAdded(Upgrade upgrade) {
        return isTheSame(upgrade) && !isConfigurable() && getAmount() + upgrade.getAmount() <= getLimit();
    }
    
    public boolean tryAddUpgrade(Upgrade upgrade) {
        if(canUpgradeBeAdded(upgrade)) {
            for(int i = 0; i < upgrade.getAmount(); i++) {
                add();
            }
            return true;
        }
        
        return false;
    }
    
    
    public void writePacketData(ByteBuf data) throws IOException {
        ByteBufUtils.writeUTF8String(data, getName());
        data.writeInt(getAmount());
    }

    /**
     * Method to read the info for an upgrade
     * This needs to be inherited by upgrades that add info
     */
    public void readPacketDataInfo(ByteBuf input) throws IOException {
        setAmount(input.readInt());
        markTileDirty();
    }
    
    /**
     * Is static because the client doesn't know what upgrades are being sent
     * @return A new instance of that Upgrade with all the info
     */
    public static Upgrade readPacketData(ByteBuf input) throws IOException {
        Upgrade upgrade = Upgrade.getUpgrade(UpgradeType.valueOf(ByteBufUtils.readUTF8String(input)));
        if(upgrade == null) {
            return null;
        }

        upgrade.readPacketDataInfo(input);
        return upgrade;
    }

    @Override
    public boolean hasChanged() {
        return hasChanged;
    }

    @Override
    public void markTileDirty() {
        hasChanged = true;
        tempStack = null;
    }

    @Override
    public void markClean() {
        hasChanged = false;
    }
}
