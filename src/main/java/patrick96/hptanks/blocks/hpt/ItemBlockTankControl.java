/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.blocks.hpt;

import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.FluidTank;
import patrick96.hptanks.core.fluids.FluidUtils;
import patrick96.hptanks.lib.Reference;
import patrick96.hptanks.tile.hpt.TileEntityTankControl;
import patrick96.hptanks.util.Coords;

import java.util.List;

import static patrick96.hptanks.util.StringUtils.localize;

public class ItemBlockTankControl extends ItemBlock {
    
    public ItemBlockTankControl(Block block) {
        super(block);
    }

    @Override
    public boolean placeBlockAt(ItemStack stack, EntityPlayer player, World world, int x, int y, int z, int side, float hitX, float hitY, float hitZ,
            int metadata) {
        if (super.placeBlockAt(stack, player, world, x, y, z, side, hitX, hitY, hitZ, metadata)) {
            
            if (stack.hasTagCompound()) {
                Coords coords = new Coords(x, y, z);
                TileEntity tile = coords.getTileEntity(world);
                if (tile instanceof TileEntityTankControl) {
                    TileEntityTankControl control = (TileEntityTankControl) tile;
                    
                    NBTTagCompound compound = stack.getTagCompound();
                    control.readFromNBT(compound);
                    
                    // Overwrites the block coordinates because in the NBTCompound the old coords were set
                    control.xCoord = x;
                    control.yCoord = y;
                    control.zCoord = z;
                }
            }
            
            return true;
        }
        
        return false;
    }
    
    @Override
    public void addInformation(ItemStack stack, EntityPlayer player, List list, boolean par4) {
        if (stack.hasTagCompound()) {
            NBTTagCompound compound = stack.getTagCompound();
            Coords dimensions = new Coords().loadFromNBT(compound, "dimensions");
            
            int tankCapacity = compound.getInteger("tankCapacity");
            FluidTank tank = new FluidTank(tankCapacity).readFromNBT(compound);
            
            Coords thisCoords = new Coords().loadFromTileNBT(compound);
            
            FluidStack fluid = tank.getFluid();
            if (fluid != null) {
                String[] stringList = FluidUtils.tankToList(tankCapacity, fluid).toArray(new String[0]);
                for (String listItem : stringList) {
                    list.add(listItem);
                }
            }
            
            NBTTagCompound handlerCompound = compound.getCompoundTag("hptPowerHandler");

            list.add(localize("hpt.power.stored") + ": " + Math.round(handlerCompound.getFloat("energyStored")) + " /" + Reference.BASIC_ENERGY_STORAGE_CAPACITY);
            list.add(localize("hpt.tank.dimensions") + ": " + dimensions.x + "x" + dimensions.z + "x" + dimensions.y);
            
            list.add(thisCoords.toString());
        }
    }
}
