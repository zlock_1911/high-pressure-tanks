/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */

package patrick96.hptanks.init;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.FurnaceRecipes;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.oredict.OreDictionary;
import net.minecraftforge.oredict.ShapedOreRecipe;
import net.minecraftforge.oredict.ShapelessOreRecipe;
import patrick96.hptanks.blocks.BuildingBlock;
import patrick96.hptanks.init.ModItems.ItemCutter;
import patrick96.hptanks.init.ModItems.ItemPlain;
import patrick96.hptanks.recipe.*;
import patrick96.hptanks.upgrades.UpgradeType;
import patrick96.hptanks.upgrades.types.Upgrade;
import patrick96.hptanks.util.ItemStackIdentifier;

import java.util.ArrayList;
import java.util.List;

import static patrick96.hptanks.recipe.CVDFurnaceRecipeInfo.Catalyst;
import static patrick96.hptanks.recipe.CVDFurnaceRecipeInfo.CatalystConfiguration;

public class Recipes {

    public static void preInit() {
        addOreDictEntries();
    }

    public static void init() {
        initSmeltingRecipes();
        initRecipes();
        initMachineRecipes();
    }

    public static void initMachineRecipes() {
        HPTRegistry.addWireMillRecipe(new WireMillRecipeInfo(new ItemStackIdentifier("ingotCopper"), ModItems.wire.getStack(16), 2000, 400));

        // Spruce wood to 250mB resin
        HPTRegistry.addResinExtractorRecipe(new ResinExtractorRecipeInfo(new ItemStackIdentifier(new ItemStack(Blocks.log, 1, 1)), 250, 300));

        HPTRegistry.addResinExtractorRecipe(new ResinExtractorRecipeInfo(new ItemStackIdentifier("woodRubber"), 250, 300));
        HPTRegistry.addResinExtractorRecipe(new ResinExtractorRecipeInfo(new ItemStackIdentifier("itemRawRubber"), 500, 200));

        List<CatalystConfiguration> catalysts = new ArrayList<CatalystConfiguration>() {{
            this.add(new CatalystConfiguration(Catalyst.IRON, 0.1F, false));
            this.add(new CatalystConfiguration(Catalyst.AL2O3, 0.25F, true));
        }};

        HPTRegistry.addCvdFurnaceRecipe(
                new CVDFurnaceRecipeInfo(new ItemStackIdentifier(ItemPlain.Graphite.getStack()), ItemPlain.CNT.getStack(16), 32, 800, 8000, catalysts, 1.25F));
        HPTRegistry.addCvdFurnaceRecipe(
                new CVDFurnaceRecipeInfo(new ItemStackIdentifier(new ItemStack(Items.coal)), ItemPlain.CNT.getStack(10), 32, 1250, 10000, catalysts, 1.25F));
        HPTRegistry.addCvdFurnaceRecipe(
                new CVDFurnaceRecipeInfo(new ItemStackIdentifier(ItemPlain.GraphiteBulk.getStack()), ItemPlain.DiamondFilm.getStack(6), 64, 1000, 16000));

        HPTRegistry.addHpCutterRecipe(new HPCutterRecipeInfo(ItemPlain.CNT.getStack(), ItemPlain.Graphene.getStack(), 3, 32, 400));
        HPTRegistry.addHpCutterRecipe(new HPCutterRecipeInfo(new ItemStack(Blocks.diamond_block), ItemPlain.DiamondFilm.getStack(12), 2, 128, 800));
        HPTRegistry.addHpCutterRecipe(new HPCutterRecipeInfo(new ItemStack(Items.nether_star), ItemCutter.NetherStarShard.getStack(8), 2, 32, 400));

        HPTRegistry.addRubberFormerRecipe(new RubberFormerRecipeInfo(ModBlocks.resinBrick.getBlock().getStack(1), 1000, 160));
        HPTRegistry.addRubberFormerRecipe(new RubberFormerRecipeInfo(ModBlocks.resinBrickDark.getBlock().getStack(1), 1000, 160));
        HPTRegistry.addRubberFormerRecipe(new RubberFormerRecipeInfo(ModBlocks.smoothResin.getBlock().getStack(1), 1000, 160));
        HPTRegistry.addRubberFormerRecipe(new RubberFormerRecipeInfo(ModBlocks.smoothResinDark.getBlock().getStack(1), 1000, 160));
        HPTRegistry.addRubberFormerRecipe(new RubberFormerRecipeInfo(ModBlocks.resin.getStack(1), 2000, 160));
    }

    public static void addOreDictEntries() {
        OreDictionary.registerOre("obsidianRod", ItemPlain.ObsidianStick.getStack());

        OreDictionary.registerOre("oreBauxite", ModBlocks.ore.getStack(1, 0));
        OreDictionary.registerOre("oreCopper", ModBlocks.ore.getStack(1, 1));
        OreDictionary.registerOre("oreGraphite", ModBlocks.ore.getStack(1, 2));
        OreDictionary.registerOre("ingotAluminium", ModItems.ingot.getStack(1, 0));
        OreDictionary.registerOre("ingotAluminum", ModItems.ingot.getStack(1, 0));
        OreDictionary.registerOre("ingotCopper", ModItems.ingot.getStack(1, 1));
        OreDictionary.registerOre("blockAluminium", ModBlocks.metal.getStack(1, 0));
        OreDictionary.registerOre("blockAluminum", ModBlocks.metal.getStack(1, 0));
        OreDictionary.registerOre("blockCopper", ModBlocks.metal.getStack(1, 1));

        OreDictionary.registerOre("dustAluminum", ModItems.dust.getStack(1, 0));

        OreDictionary.registerOre("iron_ingot", Items.iron_ingot);
        OreDictionary.registerOre("ingotGold", Items.gold_ingot);
        OreDictionary.registerOre("glass", Blocks.glass);
    }

    public static void initSmeltingRecipes() {
        // Bauxite
        FurnaceRecipes.smelting().func_151394_a(ModBlocks.ore.getStack(1, 0), ModItems.ingot.getStack(1, 0), 1F);
        FurnaceRecipes.smelting().func_151394_a(ModItems.dust.getStack(1, 0), ModItems.ingot.getStack(1, 0), 1F);

        // Copper
        FurnaceRecipes.smelting().func_151394_a(ModBlocks.ore.getStack(1, 1), ModItems.ingot.getStack(1, 1), 1F);

        // Graphite
        FurnaceRecipes.smelting().func_151394_a(ModBlocks.ore.getStack(1, 2), ItemPlain.Graphite.getStack(), 1F);

        // TODO in 1.8 add 1 Red Sandstone slab to 1 Wafer
        // Stone Slab
        FurnaceRecipes.smelting().func_151394_a(new ItemStack(Blocks.stone_slab, 1, 0), ItemPlain.Wafer.getStack(2), 1F);
        // Sandstone Slab
        FurnaceRecipes.smelting().func_151394_a(new ItemStack(Blocks.stone_slab, 1, 1), ItemPlain.Wafer.getStack(1), 1F);
        // Cobblestone Slab
        FurnaceRecipes.smelting().func_151394_a(new ItemStack(Blocks.stone_slab, 1, 3), ItemPlain.Wafer.getStack(1), 1F);
        // Quartz Slab
        FurnaceRecipes.smelting().func_151394_a(new ItemStack(Blocks.stone_slab, 1, 7), ItemPlain.Wafer.getStack(4), 1F);
    }

    public static void initRecipes() {

        /* ======== Vanilla Recipes ======== */

        GameRegistry.addRecipe(new ItemStack(Blocks.torch, 8), "G", "S", "S",
                'S', Items.stick,
                'G', ItemPlain.Graphite.getStack());

        GameRegistry.addShapelessRecipe(new ItemStack(Items.fire_charge, 4), Items.gunpowder, Items.blaze_powder, ItemPlain.Graphite.getStack());

        /* ======== Metal/Compressed Block Recipes ======== */

        GameRegistry.addRecipe(new ShapelessOreRecipe(new ItemStack(ModItems.ingot, 9, 0), "blockAluminum"));
        GameRegistry.addRecipe(new ShapelessOreRecipe(new ItemStack(ModItems.ingot, 9, 1), "blockCopper"));

        GameRegistry.addRecipe(new ShapedOreRecipe(ModBlocks.metal.getStack(1, 0), "AAA", "AAA", "AAA",
                'A', "ingotAluminum"));
        GameRegistry.addRecipe(new ShapedOreRecipe(ModBlocks.metal.getStack(1, 1), "CCC", "CCC", "CCC",
                'C', "ingotCopper"));
        GameRegistry.addRecipe(ModBlocks.compressed.getStack(), "GGG", "GGG", "GGG", 'G', ItemPlain.Graphite.getStack());

        /* ======== Building Blocks Recipes ======== */

        for(BuildingBlock block : ModBlocks.buildingBlocks) {
            block.addRecipes();
        }



        GameRegistry.addRecipe(ItemPlain.Graphene.getStack(1), "NNN", "NCN", "NNN",
                'C', ItemCutter.Cutter.getStack(),
                'N', ItemPlain.CNT.getStack());

        GameRegistry.addShapelessRecipe(ItemPlain.DiamondFilm.getStack(8), ItemCutter.HardenedCutter.getStack(1, OreDictionary.WILDCARD_VALUE), Blocks.diamond_block);

        GameRegistry.addRecipe(ItemPlain.ObsidianStick.getStack(2), "O", "O", 'O', Blocks.obsidian);

        GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(ModBlocks.obsidianBars, 16), "ISI", "ISI",
                'I', Items.iron_ingot,
                'S', "obsidianRod"));

        GameRegistry.addRecipe(new ShapedOreRecipe(ItemPlain.HardenedRod.getStack(3), "CHC", "CHC", "CHC",
                'C', ItemPlain.CNT.getStack(),
                'H', "obsidianRod"));

        GameRegistry.addRecipe(ItemPlain.ReinforcedBars.getStack(8), "BQB", "SDS", "BQB",
                'Q', Items.quartz,
                'S', ItemPlain.HardenedRod.getStack(),
                'B', ModBlocks.obsidianBars,
                'D', Items.diamond);
        GameRegistry.addRecipe(ItemPlain.ReinforcedBars.getStack(8), "BSB", "QDQ", "BSB",
                'Q', Items.quartz,
                'S', ItemPlain.HardenedRod.getStack(),
                'B', ModBlocks.obsidianBars,
                'D', Items.diamond);

        GameRegistry.addShapedRecipe(ItemCutter.DiamondCoatedNetherStarShard.getStack(), " D ", "DND", " D ",
                'D', ItemPlain.DiamondFilm.getStack(),
                'N', ItemCutter.NetherStarShard.getStack());

        // Wrench with Emerald
        GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(ModItems.wrench, 1), "I I", " E ", " L ",
                'I', Items.iron_ingot,
                'L', new ItemStack(Items.dye, 1, 4),
                'E', Items.emerald));

        // Wrench with Diamond
        GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(ModItems.wrench, 1), "I I", " E ", " L ",
                'I', Items.iron_ingot,
                'L', new ItemStack(Items.dye, 1, 4),
                'E', Items.diamond));

        // Wrench with Ruby
        GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(ModItems.wrench, 1), "I I", " E ", " L ",
                'I', Items.iron_ingot,
                'L', new ItemStack(Items.dye, 1, 4),
                'E', "gemRuby"));

        // Wrench with Sapphire
        GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(ModItems.wrench, 1), "I I", " E ", " L ",
                'I', Items.iron_ingot,
                'L', new ItemStack(Items.dye, 1, 4),
                'E', "gemSapphire"));

        // Wrench with Peridot
        GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(ModItems.wrench, 1), "I I", " E ", " L ",
                'I', Items.iron_ingot,
                'L', new ItemStack(Items.dye, 1, 4),
                'E', "gemPeridot"));

        GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(ModBlocks.bulletproofGlass, 16), "GGG", "GRG", "GGG",
                'R', ItemPlain.ReinforcedBars.getStack(),
                'G', "glass"));

        GameRegistry.addRecipe(new ItemStack(ModBlocks.tankGlass, 8), "LGL", "GCG", "LGL",
                'L', new ItemStack(Items.dye, 1, 4),
                'C', new ItemStack(ModItems.circuitBoard, 1, 0),
                'G', ModBlocks.bulletproofGlass);

        GameRegistry.addRecipe(new ItemStack(ModBlocks.tankValve, 4), "LIL", "TRT", "LCL",
                'R', ItemPlain.ReinforcedBars.getStack(),
                'T', ItemPlain.InternalTank.getStack(),
                'C', ModBlocks.tankCasing,
                'L', new ItemStack(Items.dye, 1, 4),
                'I', new ItemStack(ModItems.circuitBoard, 1, 0));

        GameRegistry.addRecipe(new ItemStack(ModBlocks.tankControl), "CDC", "CEC", "IWI",
                'C', ModBlocks.tankCasing,
                'E', ItemPlain.EnergyCore.getStack(),
                'W', ModItems.wrench,
                'D', Items.diamond,
                'I', new ItemStack(ModItems.circuitBoard, 1, 0));

        ClearControllerDataRecipe clearControllerDataRecipe = new ClearControllerDataRecipe();
        GameRegistry.addRecipe(clearControllerDataRecipe);
        MinecraftForge.EVENT_BUS.register(clearControllerDataRecipe);

        GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(ModBlocks.tankCasing, 16), "ICI", "ARA", "ILI",
                'I', Items.iron_ingot,
                'R', ItemPlain.ReinforcedBars.getStack(),
                'A', ItemPlain.HardenedRod.getStack(),
                'L', new ItemStack(Items.dye, 1, 4),
                'C', new ItemStack(ModItems.circuitBoard, 1, 0)));

        GameRegistry.addRecipe(ItemPlain.EnergyCore.getStack(), "GRG", "LFL", "GRG",
                'R', Blocks.redstone_block,
                'L', Blocks.lapis_block,
                'G', ModBlocks.bulletproofGlass,
                'F', Items.fire_charge);

        GameRegistry.addRecipe(ItemPlain.EnergyCore.getStack(), "GLG", "RFR", "GLG",
                'R', Blocks.redstone_block,
                'L', Blocks.lapis_block,
                'G', ModBlocks.bulletproofGlass,
                'F', Items.fire_charge);

        // Internal Tank
        GameRegistry.addRecipe(new ShapedOreRecipe(ItemPlain.InternalTank.getStack(), "GGG", "GCG", "GBG",
                'G', "glass",
                'B', Items.bucket,
                'C', ModItems.circuitBoard.getStack(1, 0)));

        GameRegistry.addRecipe(new ShapedOreRecipe(ModItems.circuitBoard.getStack(2, 0), " W ", "GRG", "QAQ",
                'A', "ingotAluminum",
                'W', new ItemStack(ModItems.wire, 1, 0),
                'R', Items.repeater,
                'Q', Items.quartz,
                'G', Items.glowstone_dust));

        GameRegistry.addRecipe(new ShapedOreRecipe(ModItems.wire.getStack(3, 0), "WWW", "CCC", "WWW",
                'W', Blocks.wool, 'C', "ingotCopper"));

        // Machine Block
        GameRegistry.addRecipe(new ShapedOreRecipe(ModBlocks.machine.getStack(1, 0), "INI", "CRC", "GEG",
                'I', Items.iron_ingot, 'C', "ingotCopper",
                'R', new ItemStack(ModItems.circuitBoard, 1, 0),
                'E', ItemPlain.EnergyCore.getStack(),
                'N', Items.netherbrick,
                'G', ItemPlain.Graphene.getStack()));

        /* ======== Machine Recipes ======== */

        // WireMill
        GameRegistry.addRecipe(ModBlocks.machine.getStack(1, 1), "GTG", "GBG", "SHP",
                'S', Blocks.sticky_piston,
                'P', Blocks.piston,
                'T', ItemPlain.InternalTank.getStack(),
                'B', new ItemStack(ModBlocks.machine, 1, 0),
                'H', ItemCutter.HardenedCutter.getStack(),
                'G', ItemPlain.Graphene.getStack());

        // ResinExtractor
        GameRegistry.addRecipe(ModBlocks.machine.getStack(1, 2), "GTG", "GBG", "PCP",
                'P', Blocks.piston,
                'T', ItemPlain.InternalTank.getStack(),
                'B', new ItemStack(ModBlocks.machine, 1, 0),
                'C', ItemCutter.Cutter.getStack(),
                'G', ItemPlain.Graphene.getStack());

        // CVDFurnace
        GameRegistry.addRecipe(ModBlocks.machine.getStack(1, 3), "TUT", "EBE", "FRF",
                'R', ItemPlain.ReinforcedBars.getStack(),
                'T', ItemPlain.InternalTank.getStack(),
                'B', new ItemStack(ModBlocks.machine, 1, 0),
                'F', Blocks.furnace,
                'U', ModBlocks.fluidPump.getStack(),
                'E', Upgrade.getUpgrade(UpgradeType.ENERGY_STORAGE).getItemStack());

        // HPCutter
        GameRegistry.addRecipe(ModBlocks.machine.getStack(1, 4), "GGG", "EBE", "CRC",
                'G', ModBlocks.bulletproofGlass.getStack(),
                'R', ItemPlain.ReinforcedBars.getStack(),
                'B', new ItemStack(ModBlocks.machine, 1, 0),
                'C', ModItems.circuitBoard.getStack(),
                'E', Upgrade.getUpgrade(UpgradeType.ENERGY_STORAGE).getItemStack());


        // Rubber Former
        GameRegistry.addRecipe(ModBlocks.machine.getStack(1, 5), "GTG", "GBG", "FRF",
                'T', ItemPlain.InternalTank.getStack(),
                'G', ModBlocks.bulletproofGlass.getStack(),
                'R', ItemPlain.ReinforcedBars.getStack(),
                'B', new ItemStack(ModBlocks.machine, 1, 0),
                'F', Blocks.furnace,
                'G', ItemPlain.Graphene.getStack());


        GameRegistry.addRecipe(new ShapedOreRecipe(ModBlocks.fluidPump, "ENE", "GTG", "ARA",
                'G', ItemPlain.Graphene.getStack(),
                'T', ItemPlain.InternalTank.getStack(),
                'N', ItemPlain.HardenedRod.getStack(),
                'R', Blocks.redstone_block,
                'A', "ingotAluminum",
                'E', Items.ender_pearl));

        GameRegistry.addRecipe(new ShapedOreRecipe(ItemCutter.Cutter.getStack(16), "IAD",
                'I', Items.iron_ingot,
                'A', "ingotAluminum",
                'D', Items.diamond));

        GameRegistry.addRecipe(ItemCutter.HardenedCutter.getStack(), " GC", "GOG", "OG ",
                'G', ItemPlain.Graphene.getStack(),
                'C', ItemCutter.Cutter.getStack(),
                'O', ItemPlain.HardenedRod.getStack());

        GameRegistry.addRecipe(ItemCutter.LaserCutter.getStack(), "BBB", "RDR", "GCG",
                'B', ModBlocks.bulletproofGlass,
                'R', Items.redstone,
                'D', Items.diamond,
                'G', Blocks.glowstone,
                'C', ModItems.circuitBoard.getStack(1, 0));

        GameRegistry.addRecipe(ItemPlain.GraphiteBulk.getStack(), "GGG", "G G", "GGG",
                'G', ItemPlain.Graphite.getStack());

        GameRegistry.addRecipe(ItemPlain.UpgradePlate.getStack(4), "DDD", "ERE", "GCG",
                'E', Items.ender_eye,
                'G', ItemPlain.Graphene.getStack(),
                'C', new ItemStack(ModItems.circuitBoard, 1, 0),
                'R', ItemPlain.ReinforcedBars.getStack(),
                'D', ItemPlain.DiamondFilm.getStack());

        // Speed Upgrade
        GameRegistry.addRecipe(new ItemStack(ModItems.upgrade, 2, 0), "ASA", "CPC", "AEA",
                'S', Items.sugar,
                'E', Items.ender_pearl,
                'A', ItemPlain.HardenedRod.getStack(),
                'C', new ItemStack(ModItems.circuitBoard, 1, 0),
                'P', ItemPlain.UpgradePlate.getStack());

        GameRegistry.addRecipe(new ItemStack(ModItems.upgrade, 4, 0), "ASA", "CPC", "AEA",
                'S', new ItemStack(Items.potionitem, 1, 8194),
                'E', Items.ender_pearl,
                'A', ItemPlain.HardenedRod.getStack(),
                'C', new ItemStack(ModItems.circuitBoard, 1, 0),
                'P', ItemPlain.UpgradePlate.getStack());

        GameRegistry.addRecipe(new ItemStack(ModItems.upgrade, 6, 0), "ASA", "CPC", "AEA",
                'S', new ItemStack(Items.potionitem, 1, 8226),
                'E', Items.ender_pearl,
                'A', ItemPlain.HardenedRod.getStack(),
                'C', new ItemStack(ModItems.circuitBoard, 1, 0),
                'P', ItemPlain.UpgradePlate.getStack());

        // Energy Storage Upgrade
        GameRegistry.addRecipe(new ItemStack(ModItems.upgrade, 2, 1), "AEA", "CPC", "AEA",
                'A', ItemPlain.HardenedRod.getStack(),
                'E', ItemPlain.EnergyCore.getStack(),
                'C', new ItemStack(ModItems.circuitBoard, 1, 0),
                'P', ItemPlain.UpgradePlate.getStack());

        // Fluid Auto-Ejection Upgrade
        GameRegistry.addRecipe(new ItemStack(ModItems.upgrade, 1, 3), "AIA", "CPC", "ABA",
                'I', ItemPlain.InternalTank.getStack(),
                'B', Blocks.piston,
                'A', ItemPlain.HardenedRod.getStack(),
                'C', new ItemStack(ModItems.circuitBoard, 1, 0),
                'P', ItemPlain.UpgradePlate.getStack());

        // Item Auto-Ejection Upgrade
        GameRegistry.addRecipe(new ItemStack(ModItems.upgrade, 1, 4), "AIA", "CPC", "ABA",
                'I', Blocks.trapped_chest,
                'B', Blocks.piston,
                'A', ItemPlain.HardenedRod.getStack(),
                'C', new ItemStack(ModItems.circuitBoard, 1, 0),
                'P', ItemPlain.UpgradePlate.getStack());
    }
}
