/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */

package patrick96.hptanks.plugins.cc;

import dan200.computercraft.api.lua.ILuaContext;
import dan200.computercraft.api.lua.LuaException;
import dan200.computercraft.api.peripheral.IComputerAccess;
import dan200.computercraft.api.peripheral.IPeripheral;
import dan200.computercraft.api.turtle.ITurtleAccess;
import dan200.computercraft.api.turtle.TurtleAnimation;
import dan200.computercraft.api.turtle.TurtleSide;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ChunkCoordinates;
import net.minecraftforge.common.util.ForgeDirection;
import patrick96.hptanks.tile.TileEntityHPTBase;
import patrick96.hptanks.util.Coords;

public class WrenchingTurtlePeripheral implements IPeripheral {

    private ITurtleAccess turtle;
    private TurtleSide side;

    public WrenchingTurtlePeripheral(ITurtleAccess turtle, TurtleSide side) {
        this.turtle = turtle;
        this.side = side;
    }

    @Override
    public String getType() {
        return "WrenchingTurtle";
    }

    @Override
    public String[] getMethodNames() {
        return new String[] {"rotate", "dismantle"};
    }

    @Override
    public Object[] callMethod(IComputerAccess computer, ILuaContext context, int method, Object[] arguments)
            throws LuaException, InterruptedException {

        if(turtle == null) {
            return null;
        }

        ChunkCoordinates chCoords = turtle.getPosition();
        Coords coords = new Coords(chCoords.posX, chCoords.posY, chCoords.posZ);
        Coords facing = coords.add(ForgeDirection.getOrientation(turtle.getDirection()));
        TileEntity tile = facing.getTileEntity(turtle.getWorld());


        switch(method) {
            case 0:
                if(tile instanceof TileEntityHPTBase && ((TileEntityHPTBase) tile).isRotatable()) {
                    ((TileEntityHPTBase) tile).rotate();
                    swing();
                }
                break;

            case 1:
                if(tile instanceof TileEntityHPTBase && ((TileEntityHPTBase) tile).canDismantle()) {
                    ((TileEntityHPTBase) tile).dismantle();
                    facing.markBlockForUpdate(turtle.getWorld());
                    swing();
                }
                break;
        }

        return new Object[0];
    }

    public void swing() {
        if(turtle.getPeripheral(TurtleSide.Left) != null) {
            turtle.playAnimation(TurtleAnimation.SwingLeftTool);
        }
        else if(turtle.getPeripheral(TurtleSide.Right) != null) {
            turtle.playAnimation(TurtleAnimation.SwingRightTool);
        }
    }

    @Override
    public void attach(IComputerAccess computer) {}

    @Override
    public void detach(IComputerAccess computer) {}

    @Override
    public boolean equals(IPeripheral other) {
        return other == this;
    }
}
