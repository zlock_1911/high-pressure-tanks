/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */

package patrick96.hptanks.util;

import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.OreDictionary;

import java.util.ArrayList;
import java.util.List;

/**
 * Way to group one or more equal ItemStacks
 * equal meaning with a certain oreDict name
 */
public class ItemStackIdentifier {

    private ItemStack stack = null;
    private String oreDict = null;

    public ItemStackIdentifier(ItemStack stack) {
        this(stack, null);
    }

    public ItemStackIdentifier(String oreDict) {
        this(null, oreDict);
    }

    public ItemStackIdentifier(ItemStack stack, String oreDict) {
        this.stack = stack;
        this.oreDict = oreDict;
    }

    public ItemStack getStack() {
        return stack;
    }

    public String getOreDict() {
        return oreDict;
    }

    public List<ItemStack> getStacks() {
        List<ItemStack> stacks = new ArrayList<ItemStack>();

        if(!Utils.isItemStackEmpty(stack)) {
            stacks.add(stack);
        }

        if(oreDict != null) {
            List<ItemStack> ores = OreDictionary.getOres(oreDict);
            for(ItemStack ore : ores) {
                if(!Utils.areItemStacksEqualIgnoreStackSize(ore, stack)) {
                    stacks.add(ore.copy());
                }
            }
        }

        return stacks;
    }

    public boolean matches(ItemStackIdentifier match) {
        return matches(match, true);
    }

    public boolean matches(ItemStackIdentifier match, boolean checkNBT) {
        if(match == null) {
            return false;
        }

        for(ItemStack stack : match.getStacks()) {
            if(matches(stack, checkNBT)) {
                return true;
            }
        }
        return false;
    }

    public boolean matches(ItemStack match) {
        return matches(match, true);
    }

    public boolean matches(ItemStack match, boolean checkNBT) {
        for(ItemStack stack : getStacks()) {
            if(checkNBT) {
                if(Utils.areItemStacksEqualIgnoreStackSize(stack, match)) {
                    return true;
                }
            }
            else {
                if(Utils.areItemsEqualIgnoreStackSize(stack, match)) {
                    return true;
                }
            }
        }

        return false;
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof ItemStackIdentifier && Utils.areItemStacksEqualIgnoreStackSize(stack, ((ItemStackIdentifier) obj).getStack()) && Utils.areStringsEqual(oreDict, ((ItemStackIdentifier) obj).getOreDict());
    }

    public ItemStackIdentifier copy() {
        return new ItemStackIdentifier(stack, oreDict);
    }

    @Override
    public String toString() {
        if(!Utils.isItemStackEmpty(stack)) {
            return stack.getUnlocalizedName();
        }
        else if(!StringUtils.isStringEmpty(oreDict)) {
            return oreDict;
        }

        return "<Empty>";
    }
}
