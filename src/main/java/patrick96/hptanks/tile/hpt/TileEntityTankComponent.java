/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.tile.hpt;

import cpw.mods.fml.common.network.internal.FMLNetworkHandler;
import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraftforge.common.util.ForgeDirection;
import net.minecraftforge.fluids.IFluidHandler;
import patrick96.hptanks.HPTanks;
import patrick96.hptanks.core.fluids.BucketType;
import patrick96.hptanks.core.fluids.IBucketable;
import patrick96.hptanks.core.fluids.TankManager;
import patrick96.hptanks.core.network.packets.PacketTileUpdate;
import patrick96.hptanks.lib.GuiInfo;
import patrick96.hptanks.tile.TileEntityHPTBase;
import patrick96.hptanks.util.Coords;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class TileEntityTankComponent extends TileEntityHPTBase implements IBucketable {

    /**
     * This variable defines if the the block is part of a multi block structure
     * if this points to a valid controller the block is inside a multiblock structure
     */
    protected Coords controlCoords = new Coords();
    
    /**
     * A map of TEs that are adjacent to this block
     */
    protected Map<ForgeDirection, TileEntity> adjacentBlocks = new HashMap<ForgeDirection, TileEntity>();
    
    /**
     * the side that is facing out of the tank
     * This will be @Link {@link ForgeDirection#UNKNOWN} for all TEs that can be in the frame (also for those that can be everywhere)
     */
    protected ForgeDirection facingOut = ForgeDirection.UNKNOWN;
    
	public TileEntityTankComponent() {}
    
	@Override
	protected void init() {
	    super.init();
	    if(!worldObj.isRemote) {
	        updateAdjacentBlocks();
	    }

        markTileDirty();
	}
	
	@Override
	protected boolean shouldRequestInfoFromServer() {
	    return true;
	}
	
    @Override
    public void writeToNBT(NBTTagCompound compound) {
        super.writeToNBT(compound);
        
        controlCoords.writeToNBT(compound, "tankControl");
    }
    
    @Override
    public void readFromNBT(NBTTagCompound compound) {
        super.readFromNBT(compound);
        controlCoords.loadFromNBT(compound, "tankControl");
    }
    
    public Coords getControlCoords() {
        return controlCoords.copy();
    }
    
    public TileEntityTankControl getControl() {
        return isPartOfMultiBlock() ? this instanceof TileEntityTankControl? (TileEntityTankControl) this : (TileEntityTankControl) controlCoords.getTileEntity(worldObj) : null;
    }
    
    /**
     * Checks if this TE and the control block is part of a multiblock
     */
    public boolean isPartOfMultiBlock() {
        return controlCoords.getTileEntity(worldObj) instanceof TileEntityTankControl &&
                       (this instanceof TileEntityTankControl || ((TileEntityTankControl) controlCoords.getTileEntity(worldObj)).isPartOfMultiBlock());
    }

    public void addToMultiBlock(Coords controlCoords) {
        updateAdjacentBlocks();
        this.controlCoords = controlCoords;
        markTileDirty();
    }
    
    public void removeFromMultiBlock() {
        controlCoords.reset();
        markTileDirty();
    }
    
    /**
     * @return int distance from bottom layer. For the lowest block this would
     *         be 0
     */
    public int getDistanceFromBottom() {
        return isPartOfMultiBlock() ? getCoords().y - getControl().getCornerBlock().y : 0;
    }
    
    public void updateAdjacentBlocks() {
        adjacentBlocks.clear();
        for(ForgeDirection dir : ForgeDirection.VALID_DIRECTIONS) {
            adjacentBlocks.put(dir, getCoords().add(dir).getTileEntity(worldObj));
        }
    }
    
    /**
     * Determines which side is facing out
     */
    public ForgeDirection getFacingOut() {
        if(facingOut != null && facingOut != ForgeDirection.UNKNOWN) {
            return facingOut;
        }
        
        ForgeDirection facingOut = ForgeDirection.UNKNOWN;
        
        if (isPartOfMultiBlock() && this instanceof IHPTWallComponent && !(this instanceof IHPTFrameComponent)) {
            Coords diff = getCoords().diff(getControl().getCornerBlock());
            Coords dimensions = getControl().getDimensions();
            
            if (diff.y == 0) {
                facingOut = ForgeDirection.DOWN;
            } else if (diff.y == 1 - dimensions.y) {
                facingOut = ForgeDirection.UP;
            } else if (diff.x == 0) {
                facingOut = ForgeDirection.WEST;
            } else if (diff.x == 1 - dimensions.x) {
                facingOut = ForgeDirection.EAST;
            } else if (diff.z == 0) {
                facingOut = ForgeDirection.NORTH;
            } else if (diff.z == 1 - dimensions.z) {
                facingOut = ForgeDirection.SOUTH;
            }
        }
        
        this.facingOut = facingOut;
        return facingOut;
    }
    
    /**
     * Should give you the tankManager which the TileEntity should get from the
     * control block
     * 
     * @return {@link TankManager} of the tank
     */
    public TankManager getTankManager() {
        return isPartOfMultiBlock() ? getControl().getTankManager() : null;
    }
    
    public int getTankCapacity() {
        return isPartOfMultiBlock() ? getControl().getTankCapacity() : 0;
    }

    /**
     * Opens the default Gui.
     * TEs that want to implement their own GUI just override this method
     *
     * @param player the player
     * @return true, if the GUI was opened, used for Blocks that don't want to open GUIs
     */
    public boolean openGui(EntityPlayer player) {
        if(getControl() == null) {
            return false;
        }

        FMLNetworkHandler.openGui(player, HPTanks.instance, GuiInfo.MULTI_BLOCK_TANK_ID, worldObj,
                controlCoords.x, controlCoords.y, controlCoords.z);
        
        return true;
    }

    public void onChanged() {
        if (isPartOfMultiBlock()) {
            for(Map.Entry<ForgeDirection, TileEntity> entry : adjacentBlocks.entrySet()) {
                if(getCoords().add(entry.getKey()).getTileEntity(worldObj) != entry.getValue()) {
                    getControl().validateMultiBlock();
                    break;
                }
            }
        }
    }
    
    @Override
    public void writePacketData(ByteBuf data) throws IOException {
        super.writePacketData(data);
        controlCoords.writeData(data);
    }
    
    @Override
    public void handleUpdatePacket(PacketTileUpdate packet) throws IOException {
        super.handleUpdatePacket(packet);

        Coords controlCoordsOld = controlCoords.copy();

        controlCoords.readData(packet.input);

        if(!controlCoordsOld.equals(controlCoords)) {
            getCoords().markBlockForUpdate(worldObj);
            getCoords().notifyBlockChange(worldObj, getBlockType());
        }
    }

    @Override
    public void onRequestPacket() {
        super.onRequestPacket();

        getCoords().markBlockForUpdate(worldObj);
        getCoords().notifyBlockChange(worldObj, getBlockType());
    }

    @Override
    public BucketType getBucketType() {
        return isPartOfMultiBlock()? BucketType.BOTH : BucketType.NONE;
    }

    @Override
    public IFluidHandler getFluidHandler() {
        return isPartOfMultiBlock()? getTankManager() : null;
    }
}