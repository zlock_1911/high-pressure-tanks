/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.upgrades.types;

import patrick96.hptanks.upgrades.UpgradeType;
import patrick96.hptanks.util.StringUtils;

import java.util.ArrayList;

public class UpgradeIC2 extends Upgrade {
    
    public UpgradeIC2() {
        super(UpgradeType.IC2);
    }

    @Override
    public int getLimit() {
        return 1;
    }
    
    @Override
    public ArrayList<String> getTooltip() {
        ArrayList<String> toolTip = super.getTooltip();
        toolTip.add(StringUtils.localize("hpt.upgrade.ic2.info"));
        
        return toolTip;
    }
}
