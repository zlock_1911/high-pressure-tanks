/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */

package patrick96.hptanks.client.renderer.ctm;

public class CTMConfiguration {

    /**
     * Array for what part is where
     * The Index are the sides:
     * 0: Top / Top-Left
     * 1: Right / Top-Right
     * 2: Bottom / Bottom-Right
     * 3: Left / Bottom-Left
     * The first value is for borders and the second for corners
     */
    protected Type[] sides = new Type[4];

    public enum Type {
        CORNER, BORDER, NONE
    }

    public enum TextureType {
        /**
         * Linear means that all the details on the border of the texture have the same width
         */
        LINEAR,

        /**
         * CORNER means that the corner details have another width than the normal borders and that the corner details should only be in the corner of a structure
         * Not just the Type.CORNER means that the corner pixels get taken, but also if two Type.BORDER are next to each other
         * WARNING: This is not finished, it looks like crap if you don't have straight edges, so no cross-sections
         */
        CORNER
    }

    protected TextureType type;

    private ICTM block;

    public CTMConfiguration(ICTM block) {
        this.block = block;
        for(int i = 0; i < sides.length; i++) {
            sides[i] = Type.NONE;
        }

        this.type = block.getType();
    }

    public void removeOverlaps() {
        for(int i = 0; i < sides.length; i++) {
            if(get(i) == Type.CORNER && getBefore(i) == Type.BORDER) {
                set(i, Type.NONE);
            }
        }
    }

    public int getBorderWidth() {
        return block.getBorderWidth();
    }

    public Type get(int index) {
        return sides[index % 4];
    }

    public void set(int i, Type type) {
        sides[i % 4] = type;
    }

    public Type getBefore(int index) {
        return get(index == 0? 3 : index - 1);
    }

    public Type[] getSides() {
        return sides;
    }

    private void setSides(Type[] sides) {
        this.sides = sides;
    }

    public ICTM getBlock() {
        return block;
    }

    public TextureType getType() {
        return type;
    }

    public CTMConfiguration copy() {
        CTMConfiguration config = new CTMConfiguration(block);
        config.setSides(getSides().clone());

        return config;
    }

    @Override
    public String toString() {
        String borders = "B";
        String corners = "C";
        String none = "N";

        for(int i = 0; i < sides.length; i++) {
            Type type = sides[i];
            switch(type) {
                case CORNER:
                    corners += i;
                    break;
                case BORDER:
                    borders += i;
                    break;
                case NONE:
                    none += i;
                    break;
            }
        }

        return borders + corners + none;
    }

    public String getName() {
        return CTMHelper.getTextureNames(this)[0] + toString();
    }
}
