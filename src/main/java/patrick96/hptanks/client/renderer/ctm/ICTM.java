/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */

package patrick96.hptanks.client.renderer.ctm;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.util.IIcon;
import net.minecraft.world.IBlockAccess;
import patrick96.hptanks.util.Coords;

/**
 * An Interface that has to be implemeted by blocks that want connected textures
 * To implement create a file without the borders and name it like the normal block texture and just add "NoBorders" at the end
 * In the registerBlockIcons method you have to call CTMGenerator.generateTextures and save that to an Array
 * In getIcon you call CTMHelper.getConnectedBlockTexture to return the proper Icon
 */
public interface ICTM {

    @SideOnly(Side.CLIENT)
    int getBorderWidth();

    @SideOnly(Side.CLIENT)
    boolean shouldConnectToBlock(IBlockAccess blockAccess, Coords coords, int meta);

    @SideOnly(Side.CLIENT)
    IIcon[] getIcons();

    @SideOnly(Side.CLIENT)
    CTMConfiguration.TextureType getType();

    @SideOnly(Side.CLIENT)
    /**
     * This is only called if getType == CORNER
     */
    int getCornerWidth();

}
