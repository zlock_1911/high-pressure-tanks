/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.core.network.packets;

import io.netty.buffer.ByteBuf;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import patrick96.hptanks.core.network.HPTPacket;
import patrick96.hptanks.util.Coords;

import java.io.IOException;

public class PacketCoords extends HPTPacket {
    
    Coords coords = new Coords();
    
    public PacketCoords(byte id) {
        super(id);
    }
    
    /**
     * @return the coords
     */
    public Coords getCoords() {
        return coords;
    }
    
    /**
     * @param coords the coords to set
     */
    public void setCoords(Coords coords) {
        this.coords = coords;
    }
    
    public TileEntity getTile(World world) {
        return coords.getTileEntity(world);
    }
    
    @Override
    public void writeData(ByteBuf data) throws IOException {
        super.writeData(data);
        coords.writeData(data);
    }
    
    @Override
    public void readData(ByteBuf data) throws IOException {
        super.readData(data);
        coords.readData(data);
    }
    
}
