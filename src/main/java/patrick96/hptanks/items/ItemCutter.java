/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */

package patrick96.hptanks.items;

import net.minecraft.item.ItemStack;
import patrick96.hptanks.recipe.HPCutterRecipeInfo;
import patrick96.hptanks.recipe.HPCutterRecipeInfo.CutterDamage;
import patrick96.hptanks.recipe.HPTRegistry;
import patrick96.hptanks.util.ItemStackIdentifier;

public class ItemCutter extends ItemHPT {

    public final HPCutterRecipeInfo.CutterConfiguration config;

    public ItemCutter(String unlocalizedName, HPCutterRecipeInfo.CutterConfiguration config) {
        this(unlocalizedName, config, 0);
    }

    public ItemCutter(String unlocalizedName, HPCutterRecipeInfo.CutterConfiguration config, int maxDamage) {
        super(unlocalizedName);

        this.config = config;

        if(this.config.getDamage() == CutterDamage.DAMAGE) {
            this.setMaxDamage(maxDamage);
            this.setMaxStackSize(1);
        }

        /**
         * The cutter should only take damage if the Item can actually take damage because otherwise it would be possible to shift through
         * the different metadata items in the same item
         */
        if(!this.isDamageable() && this.config.getDamage() == CutterDamage.DAMAGE) {
            this.config.setDamage(CutterDamage.DESTROY);
            this.setMaxStackSize(64);
        }

        HPTRegistry.registerCutter(new ItemStackIdentifier(getStack()), this.config);
    }

    @Override
    public int getItemEnchantability(ItemStack stack) {
        return 10;
    }
}
