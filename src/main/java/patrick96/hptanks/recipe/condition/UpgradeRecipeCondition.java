/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */

package patrick96.hptanks.recipe.condition;

import codechicken.lib.gui.GuiDraw;
import codechicken.nei.recipe.GuiCraftingRecipe;
import codechicken.nei.recipe.GuiUsageRecipe;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.util.IIcon;
import patrick96.hptanks.client.util.GuiUtils;
import patrick96.hptanks.recipe.MachineRecipeInfo;
import patrick96.hptanks.tile.machine.TileEntityMachine;
import patrick96.hptanks.upgrades.UpgradeType;
import patrick96.hptanks.upgrades.types.Upgrade;
import patrick96.hptanks.util.StringUtils;

import java.util.List;

/**
 * Condition that requires an IC2 recipe to be installed in the machine
 */
public class UpgradeRecipeCondition extends BaseRecipeCondition {

    public final Upgrade upgrade;

    public UpgradeRecipeCondition(UpgradeType upgrade) {
        this.upgrade = Upgrade.getUpgrade(upgrade);
    }

    @Override
    public boolean meetsCondition(TileEntityMachine tile, MachineRecipeInfo recipe) {
        return tile.getUpgradeManager().hasUpgrade(upgrade.getType());
    }

    @Override
    public List<String> getTooltip() {
        List<String> list = super.getTooltip();
        list.add(upgrade.getItemStack().getDisplayName() + " " + StringUtils.localize("hpt.upgrade.required"));
        return list;
    }

    @Override
    public void drawExtras() {
        super.drawExtras();
        GuiUtils.bindTexture(TextureMap.locationItemsTexture);
        IIcon icon = upgrade.getItemStack().getIconIndex();
        GuiDraw.gui.drawTexturedModelRectFromIcon(0, 0, icon, 16, 16);
    }

    @Override
    public boolean hasTransferRect() {
        return true;
    }

    @Override
    public void transfer(boolean usage) {
        super.transfer(usage);
        if(usage) {
            GuiUsageRecipe.openRecipeGui("item", upgrade.getItemStack());
        }
        else {
            GuiCraftingRecipe.openRecipeGui("item", upgrade.getItemStack());
        }
    }

    @Override
    public boolean affectsEquality() {
        return true;
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj) && ((UpgradeRecipeCondition) obj).upgrade == upgrade;
    }
}
