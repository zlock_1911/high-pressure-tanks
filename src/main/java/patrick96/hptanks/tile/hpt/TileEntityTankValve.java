/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.tile.hpt;

import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.common.util.ForgeDirection;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.FluidTankInfo;
import patrick96.hptanks.core.fluids.FluidUtils;
import patrick96.hptanks.core.network.packets.PacketTileUpdate;
import patrick96.hptanks.tile.IFluidTile;

import java.io.IOException;

public class TileEntityTankValve extends TileEntityTankComponent implements IHPTWallComponent, IFluidTile {
    
    private boolean doEject = false;
    
    private int troughPutThisTick = 0;
    
    public TileEntityTankValve() {
        super();
    }
    
    @Override
    public void updateEntity() {
        super.updateEntity();

        if(worldObj.isRemote) {
            return;
        }
        
        if (doEject && isPartOfMultiBlock()) {
            FluidUtils.ejectFluid(this, 500, getFacingOut());
        }
        
        troughPutThisTick = 0;
    }

    @Override
    public void onWrench(EntityPlayer player, ForgeDirection from) {
        if (isPartOfMultiBlock() && player.isSneaking()) {
            doEject = !doEject;
            markTileDirty();
        }
    }

    @Override
    public void writeToNBT(NBTTagCompound compound) {
        super.writeToNBT(compound);
        compound.setBoolean("doEject", doEject);
    }
    
    @Override
    public void readFromNBT(NBTTagCompound compound) {
        super.readFromNBT(compound);
        doEject = compound.getBoolean("doEject");
    }
    
    public boolean canEject() {
        if (!isPartOfMultiBlock()) {
            return false;
        }
        if (getTankManager().isEmpty()) {
            return false;
        }
        int fluidLevel = getControl().getFluidLevel();
        return fluidLevel >= getDistanceFromBottom();
    }
    
    /* -- IFluidHandler Methods */

    @Override
    public boolean shouldHandleTankManager() {
        return false;
    }

    @Override
    public int fill(ForgeDirection from, FluidStack resource, boolean doFill) {
        // Can't fill if the valve auto-ejects
        if (!isPartOfMultiBlock() || doEject() || FluidUtils.isFluidStackEmpty(resource)) {
            return 0;
        }
        
        int troughputAllowed = FluidUtils.isTroughPutAllowed(troughPutThisTick, resource.amount);
        if(doFill) {
            troughPutThisTick += troughputAllowed;
        }
        FluidStack limitedStack = resource.copy();
        limitedStack.amount = troughputAllowed;
        
        return getControl().fill(from, limitedStack, doFill);
    }
    
    @Override
    public FluidStack drain(ForgeDirection from, int maxDrain, boolean doDrain) {
        if(maxDrain <= 0 || !canDrain(from, null)) {
            return null;
        }
        
        int troughputAllowed = FluidUtils.isTroughPutAllowed(troughPutThisTick, maxDrain);
        if(doDrain) {
            troughPutThisTick += troughputAllowed;
        }
        
        return canEject() ? getControl().drain(from, troughputAllowed, doDrain) : null;
    }
    
    @Override
    public FluidStack drain(ForgeDirection from, FluidStack resource, boolean doDrain) {
        if (FluidUtils.isFluidStackEmpty(resource) || !canDrain(from, resource.getFluid())) {
            return null;
        }
        
        int troughputAllowed = FluidUtils.isTroughPutAllowed(troughPutThisTick, resource.amount);
        troughPutThisTick += troughputAllowed;
        FluidStack limitedStack = resource.copy();
        limitedStack.amount = troughputAllowed;
        
        return canEject() ? getControl().drain(from, limitedStack, doDrain) : null;
    }
    
    @Override
    public boolean canDrain(ForgeDirection from, Fluid fluid) {
        return isPartOfMultiBlock();
    }
    
    @Override
    public boolean canFill(ForgeDirection from, Fluid fluid) {
        return isPartOfMultiBlock() && !doEject();
    }
    
    @Override
    public FluidTankInfo[] getTankInfo(ForgeDirection from) {
        if (!isPartOfMultiBlock()) {
            return null;
        }
        return getControl().getTankInfo(from);
    }
    
    @Override
    public void handleUpdatePacket(PacketTileUpdate packet) throws IOException {
        super.handleUpdatePacket(packet);
        if(packet.input.readBoolean() != doEject) {
            doEject = !doEject;
            getCoords().markBlockForUpdate(worldObj);
        }
    }
    
    @Override
    public void writePacketData(ByteBuf data) throws IOException {
        super.writePacketData(data);
        data.writeBoolean(doEject);
    }
    
    public boolean doEject() {
        return doEject;
    }
    
    public void setDoEject(boolean doEject) {
        this.doEject = doEject;
    }
}
