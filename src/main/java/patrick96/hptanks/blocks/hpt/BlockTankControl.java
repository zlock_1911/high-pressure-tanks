/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.blocks.hpt;

import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.common.util.ForgeDirection;
import patrick96.hptanks.client.renderer.block.TankRenderer;
import patrick96.hptanks.lib.BlockInfo;
import patrick96.hptanks.tile.hpt.TileEntityTankControl;
import patrick96.hptanks.util.Coords;

import java.util.ArrayList;
import java.util.Random;

public class BlockTankControl extends BlockTankComponent {
    
    /**
     * These variables are only used while rendering the fluid inside the tank
     * and have nothing to do with rendering the controller itself
     */
    public static boolean[] renderedSides = new boolean[6];
    public static boolean customRenderSides = false;
    
    public BlockTankControl() {
        super(BlockInfo.TANK_CONTROL_UNLOCALIZED_NAME);
        setBlockTextureName(BlockInfo.TANK_CONTROL_TEXTURE);
    }
    
    @Override
    public TileEntity createNewTileEntity(World world, int meta) {
        return new TileEntityTankControl();
    }
    
    @Override
    public boolean isOpaqueCube() {
        return false;
    }
    
    @Override
    public boolean renderAsNormalBlock() {
        return false;
    }

    @Override
    public void breakBlock(World world, int x, int y, int z, Block block, int meta) {
        Coords coords = new Coords(x, y, z);
        TileEntity tile = coords.getTileEntity(world);
        
        if (tile instanceof TileEntityTankControl) {
            TileEntityTankControl control = (TileEntityTankControl) tile;

            if (control.isPartOfMultiBlock()) {
                control.invalidateMultiBlock();
            }
        }
        
        super.breakBlock(world, x, y, z, block, meta);
    }
    
    @Override
    public ItemStack getPickBlock(MovingObjectPosition mop, World world, int x, int y, int z, EntityPlayer player) {
        Coords coords = new Coords(x, y, z);
        TileEntityTankControl control = (TileEntityTankControl) coords.getTileEntity(world);
        if (!control.hasInfoToBePickedUp()) {
            return super.getPickBlock(mop, world, x, y, z, player);
        }
        
        return createItem(control, coords.getBlockMetadata(world));
    }
    
    @Override
    public boolean removedByPlayer(World world, EntityPlayer player, int x, int y, int z, boolean willHarvest) {
       
        if (!player.capabilities.isCreativeMode && !world.isRemote) {
            dropBlockAsItem(world, x, y, z, world.getBlockMetadata(x, y, z), 0);
        }
        
        return super.removedByPlayer(world, player, x, y, z, willHarvest);
    }
    
    @Override
    public ArrayList<ItemStack> getDrops(World world, int x, int y, int z, int meta, int fortune) {
        ArrayList<ItemStack> list = new ArrayList<ItemStack>();
        
        if (world.isRemote) {
            return list;
        }
        
        Coords coords = new Coords(x, y, z);
        TileEntity tile = coords.getTileEntity(world);
        if(tile instanceof TileEntityTankControl) {
            list.add(createItem((TileEntityTankControl) tile, meta));
        }
        
        return list;
    }
    
    private ItemStack createItem(TileEntityTankControl control, int meta) {
        ItemStack droppedItemStack = new ItemStack(this, 1, 0);
        if(control.hasInfoToBePickedUp()) {
            NBTTagCompound compound = new NBTTagCompound();
            control.writeToNBT(compound);
            
            compound.setBoolean("overrideWarningShown", false);
            compound.setInteger("tankCapacity", control.getTankCapacity());
            compound.removeTag("upgrades");
            compound.removeTag("tankControl");

            // Overrides all the items in the Block (they should get dropped anyways, only used in creative pickBlock)
            compound.setTag("invItems", new NBTTagList());
            droppedItemStack.stackTagCompound = compound;
        }
        
        return droppedItemStack;
    }
    
    @Override
    public Item getItemDropped(int meta, Random par2Random, int fortune)
    {
        return null;
    }
    
    @Override
    public int quantityDropped(int meta, int fortune, Random random) {
        return 0;
    }
    
    
    /**
     * Also tells which sides of the fluid blocks should be rendered
     *
     * @param blockAccess the block access
     * @param x the x
     * @param y the y
     * @param z the z
     * @param side the side
     * @return true, if successful
     */
    @Override
    public boolean shouldSideBeRendered(IBlockAccess blockAccess, int x, int y, int z, int side) {
        if(customRenderSides) {
            return renderedSides[side];
        }
        
        return super.shouldSideBeRendered(blockAccess, x, y, z, side);
    }
    
    public static void resetRenderSides() {
        renderedSides = new boolean[6];
        customRenderSides = false;
    }
    
    public static void renderSide(ForgeDirection side, boolean render) {
        renderSide(side.ordinal(), render);
    }
    
    /**
     * Modifies if the side of the fluid block should be rendered
     *
     * @param side the side
     * @param render whether or not that side should be rendered
     */
    public static void renderSide(int side, boolean render) {
        if(customRenderSides) {
            renderedSides[side] = render;
        }
    }
    
    @Override
    public int getRenderType() {
        return TankRenderer.renderId;
    }
    
    @Override
    public int getRenderBlockPass() {
        return 1;
    }
    
    @Override
    public boolean hasComparatorInputOverride() {
        return true;
    }
}
