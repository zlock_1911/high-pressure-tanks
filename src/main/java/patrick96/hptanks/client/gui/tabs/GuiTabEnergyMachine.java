/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.client.gui.tabs;

import patrick96.hptanks.client.gui.HPTGui;
import patrick96.hptanks.client.util.Color;
import patrick96.hptanks.client.util.GuiResizeHelper.ResizeType;
import patrick96.hptanks.client.util.IconTexture;
import patrick96.hptanks.init.ModItems;

import java.util.List;

import static patrick96.hptanks.util.StringUtils.*;

public class GuiTabEnergyMachine extends GuiTab {
    
    public GuiTabEnergyMachine(HPTGui gui, int maxH, TabSide side) {
        this(gui, 96, maxH, side);
    }
    
    public GuiTabEnergyMachine(HPTGui gui, TabSide side) {
        this(gui, 0, 0, side);
    }
    
    public GuiTabEnergyMachine(HPTGui gui, int w, int h, int maxW, int maxH, TabSide side) {
        this(gui, w, h, maxW, maxH, side, ResizeType.normal);
    }
    
    public GuiTabEnergyMachine(HPTGui gui, int maxW, int maxH, TabSide side) {
        this(gui, 24, 24, maxW, maxH, side, ResizeType.normal);
    }
    
    public GuiTabEnergyMachine(HPTGui gui, int w, int h, int maxW, int maxH, TabSide side, ResizeType resizeType) {
        super(gui, w, h, maxW, maxH, side, resizeType, new Color(0xFFFCB514));
        setIcon(new IconTexture(ModItems.ItemPlain.EnergyCore.getStack().getIconIndex(), true));
        setTitle("hpt.energy", new Color(0xFF954408));
        //setTitleShadow();
        addToolTip(localize("hpt.energy"));
    }
    
    @Override
    public void drawForeground(int mouseX, int mouseY) {
        if(resizeState == ResizeState.OPEN) {
            
            int textXPos = getX() + 24;
            int currentY = getY() + 24;
            gui.drawString(localize("hpt.energy.used"), textXPos, currentY, 0xFFFFFFFF);
            currentY += gui.getFontRenderer().FONT_HEIGHT + 3;
            if(dynamicToolTip != null) {
                List<String> energyUse = dynamicToolTip.getToolTip();
                for(String str : energyUse) {
                    gui.drawString(str, textXPos, currentY, textColor.toInt());
                    currentY += gui.getFontRenderer().FONT_HEIGHT + 3;
                }
            }
        }
        super.drawForeground(mouseX, mouseY);
    }
}
