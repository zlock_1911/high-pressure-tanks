/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.tile.machine;

import net.minecraft.item.ItemStack;
import net.minecraftforge.common.util.ForgeDirection;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.FluidTank;
import net.minecraftforge.fluids.IFluidHandler;
import patrick96.hptanks.blocks.BlockMachine.MachineType;
import patrick96.hptanks.core.fluids.BucketType;
import patrick96.hptanks.core.fluids.FluidUtils;
import patrick96.hptanks.core.fluids.IBucketable;
import patrick96.hptanks.core.fluids.TankManager;
import patrick96.hptanks.core.inventory.slot.SlotInputRestricted;
import patrick96.hptanks.init.ModBlocks;
import patrick96.hptanks.lib.GuiInfo;
import patrick96.hptanks.recipe.HPTRegistry;
import patrick96.hptanks.recipe.ResinExtractorRecipeInfo;
import patrick96.hptanks.tile.IFluidTile;
import patrick96.hptanks.upgrades.UpgradeType;
import patrick96.hptanks.util.Utils;

import java.util.List;

public class TileEntityResinExtractor extends TileEntityMachine implements IFluidTile, IBucketable {

    public static final GuiInfo guiInfo = new GuiInfo(GuiInfo.RESIN_EXTRACTOR_ID, "ResinExtractor", true);

    private int defaultFluidPumpedPerTick = 100;
    
    public TileEntityResinExtractor() {
        super(MachineType.RESIN_EXTRACTOR);
        inventoryHandler.addSlot(new SlotInputRestricted(this, inventoryHandler.getNextAvailableId(), 61, 29));

        tankManager = new TankManager(new FluidTank(getTankCapacity()));
    }
    
    @Override
    public void updateEntity() {
        super.updateEntity();

        if (worldObj.isRemote) {
            return;
        }
        
        if(upgradeManager.hasUpgrade(UpgradeType.FLUID_AUTO_EJECT)) {
            FluidUtils.ejectFluid(this, FluidUtils.getFluidPumpedPerTick(this, defaultFluidPumpedPerTick));
        }
    }
    
    @Override
    public void processFinished() {
        int fluidAmount = ((ResinExtractorRecipeInfo) currentRecipe).getResinProduced();
        if (fluidAmount == 0) {
            return;
        }

        decrStackSize(getInputSlotNumber(), 1);
        getTankManager().fill(ForgeDirection.UNKNOWN, new FluidStack(ModBlocks.fluidResin, fluidAmount), true);
    }

    public boolean couldProcess() {
        if (Utils.isItemStackEmpty(getInputStack())) {
            return false;
        }
        
        int result = ((ResinExtractorRecipeInfo) currentRecipe).getResinProduced();
        int filled = getTankManager().fill(ForgeDirection.UNKNOWN, new FluidStack(ModBlocks.fluidResin, result), false);
        return !(result == 0 || filled != result);
    }

    @Override
    public int getOutputSlotNumber() {
        return -1;
    }

    @Override
    protected ResinExtractorRecipeInfo getRecipeInfo() {
        return HPTRegistry.getResinExtractorRecipeInfo(getInputStack(), this);
    }

    @Override
    public List<UpgradeType> getSupportedUpgrades() {
        List<UpgradeType> list = super.getSupportedUpgrades();
        list.add(UpgradeType.FLUID_AUTO_EJECT);

        return list;
    }

    @Override
    public boolean canFill(ForgeDirection from, Fluid fluid) {
        return false;
    }
    
    @Override
    public boolean canDrain(ForgeDirection from, Fluid fluid) {
        return true;
    }

    @Override
    public boolean canStackBeInput(ItemStack stack) {
        return HPTRegistry.getResinExtractorFluidAmount(stack, this) > 0;
    }

    @Override
    public BucketType getBucketType() {
        return BucketType.EMPTY;
    }

    @Override
    public IFluidHandler getFluidHandler() {
        return this;
    }
}
