/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */

package patrick96.hptanks.plugins.nei;

import codechicken.nei.api.API;
import codechicken.nei.api.IConfigureNEI;
import codechicken.nei.recipe.DefaultOverlayHandler;
import codechicken.nei.recipe.RecipeInfo;
import net.minecraft.client.gui.inventory.GuiContainer;
import patrick96.hptanks.client.gui.*;
import patrick96.hptanks.lib.Reference;
import patrick96.hptanks.plugins.ICompatModule;

/**
 * Class needs to have a name like "NEI<someting>Config" to get recognized by NEI
 */
public class NEIHPTConfig implements ICompatModule, IConfigureNEI {
    @Override
    public void preInit() {}

    @Override
    public void init() {}

    @Override
    public void postInit() {}

    @Override
    public String getDependency() {
        return Reference.PLUGIN_NEI_DEP;
    }

    @Override
    public String getCompatName() {
        return Reference.PLUGIN_NEI_NAME;
    }

    @Override
    public void loadConfig() {
        ResinExtractorRecipeHandler resinExtractorRecipeHandler = new ResinExtractorRecipeHandler();
        registerHandler(resinExtractorRecipeHandler);

        WireMillRecipeHandler wireMillRecipeHandler = new WireMillRecipeHandler();
        registerHandler(wireMillRecipeHandler);

        CVDFurnaceRecipeHandler cvdFurnaceRecipeHandler = new CVDFurnaceRecipeHandler();
        registerHandler(cvdFurnaceRecipeHandler);

        CatalystRecipeHandler catalystRecipeHandler = new CatalystRecipeHandler();
        API.registerRecipeHandler(catalystRecipeHandler);
        API.registerUsageHandler(catalystRecipeHandler);

        HPCutterRecipeHandler hpCutterRecipeHandler = new HPCutterRecipeHandler();
        registerHandler(hpCutterRecipeHandler);

        RubberFormerRecipeHandler rubberFormerRecipeHandler = new RubberFormerRecipeHandler();
        registerHandler(rubberFormerRecipeHandler);
    }

    public void registerHandler(BaseRecipeHandler handler) {
        Class<? extends GuiContainer> clazz = handler.getGuiClass();

        RecipeInfo.setGuiOffset(clazz, handler.getOffsetX(), handler.getOffsetY());
        API.registerRecipeHandler(handler);
        API.registerUsageHandler(handler);
        API.registerGuiOverlay(clazz, handler.getOverlayIdentifier());
        API.registerGuiOverlayHandler(clazz, new DefaultOverlayHandler(), handler.getOverlayIdentifier());
    }

    @Override
    public String getName() {
        return Reference.MOD_NAME + " NEI Plugin";
    }

    @Override
    public String getVersion() {
        return Reference.VERSION;
    }
}
