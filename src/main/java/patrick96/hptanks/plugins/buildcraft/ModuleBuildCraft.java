/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.plugins.buildcraft;

import buildcraft.api.statements.StatementManager;
import cpw.mods.fml.common.Optional;
import net.minecraft.block.Block;
import net.minecraft.item.ItemStack;
import patrick96.hptanks.lib.Reference;
import patrick96.hptanks.plugins.ICompatModule;
import patrick96.hptanks.recipe.HPCutterRecipeInfo;
import patrick96.hptanks.recipe.HPTRegistry;
import patrick96.hptanks.util.ItemStackIdentifier;

public class ModuleBuildCraft implements ICompatModule {

    public static BuildcraftStatements statements;

    @Override
    @Optional.Method(modid=Reference.PLUGIN_BC_DEP)
    public void init() {
        statements = new BuildcraftStatements();
        StatementManager.registerTriggerProvider(new HPTTriggerProvider());
        HPTRegistry.registerCutter(new ItemStackIdentifier(new ItemStack(Block.getBlockFromName("BuildCraft|Silicon:laserBlock"))), new HPCutterRecipeInfo.CutterConfiguration(2, HPCutterRecipeInfo.CutterDamage.UNBREAKABLE, 1F, 1F));
    }

    @Override
    public String getDependency() {
        return Reference.PLUGIN_BC_DEP;
    }
    
    @Override
    public String getCompatName() {
        return Reference.PLUGIN_BC_NAME;
    }

    @Override
    public void preInit() {}

    @Override
    public void postInit() {}
}