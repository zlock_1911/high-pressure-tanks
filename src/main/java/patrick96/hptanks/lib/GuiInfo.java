/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.lib;

import net.minecraft.util.ResourceLocation;
import patrick96.hptanks.client.util.GuiUtils;

public class GuiInfo {
    
    public static final int ENERGY_BACKGROUND_COLOR = 0xFF0000 | 63 << 24;
    public static final int ENERGY_FOREGROUND_COLOR = 0xFF0000 | 255 << 24;
    
    public static final int MULTI_BLOCK_TANK_ID = 0;
    public static final int MINI_TANK_ID = 1;
    public static final int WIRE_MILL_ID = 2;
    public static final int RESIN_EXTRACTOR_ID = 3;
    public static final int CVD_FURNACE_ID = 4;
    public static final int HP_CUTTER_ID = 5;
    public static final int RUBBER_FORMER_ID = 6;

    private int ID;
    // This is the name of the texture file w/o the Gui prefix
    private String textureName;

    private boolean hasGuiInformation;

    /**
     * @param ID a unique ID for this GUI in this mod
     * @param textureName the name of the gui texture
     * @param hasGuiInformation if the TE's gui should show an information tab
     */
    public GuiInfo(int ID, String textureName, boolean hasGuiInformation) {
        this.ID = ID;
        this.textureName = textureName;
        this.hasGuiInformation = hasGuiInformation;
    }

    public int getID() {
        return ID;
    }

    public String getTextureName() {
        return textureName;
    }

    public ResourceLocation getGuiTexture() {
        return GuiUtils.getGuiTexture(textureName);
    }

    public boolean hasGuiInformation() {
        return hasGuiInformation;
    }
}