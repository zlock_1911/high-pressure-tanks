/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.items;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;
import net.minecraft.util.MathHelper;
import patrick96.hptanks.util.Utils;

import java.util.List;

public class ItemMulti extends ItemHPT {
    
    protected String[] unlocalizedSuffixes;
    protected String[] textureSuffixes;
    @SideOnly(Side.CLIENT)
    protected IIcon[] icons;
    
    public ItemMulti(String unlocalizedName) {
        super(unlocalizedName);
        setHasSubtypes(true);
    }
    
    public void setTextureSuffixes(String... textures) {
        textureSuffixes = textures;
    }
    
    public void setSuffixes(String... names) {
        unlocalizedSuffixes = names;
    }
    
    @Override
    @SideOnly(Side.CLIENT)
    public IIcon getIconFromDamage(int damage) {
        return icons[MathHelper.clamp_int(damage, 0, icons.length - 1)];
    }
    
    @Override
    public int getMetadata(int meta) {
        return meta;
    }
    
    @Override
    public void getSubItems(Item item, CreativeTabs par2CreativeTabs, List list) {
        for (int i = 0; i < unlocalizedSuffixes.length; i++) {
            list.add(new ItemStack(item, 1, i));
        }
    }
    
    @Override
    public String getUnlocalizedName(ItemStack item) {
        int damage = MathHelper.clamp_int(item.getItemDamage(), 0, unlocalizedSuffixes.length - 1);
        return this.getUnlocalizedName() + "." + unlocalizedSuffixes[damage];
    }
    
    @Override
    @SideOnly(Side.CLIENT)
    public void registerIcons(IIconRegister register) {
        if(textureSuffixes == null) {
            return;
        }
        
        icons = new IIcon[textureSuffixes.length];
        
        for (int i = 0; i < icons.length; i++) {
            icons[i] = register.registerIcon(Utils.getTexture(getIconString() + textureSuffixes[i]));
        }
    }
}
