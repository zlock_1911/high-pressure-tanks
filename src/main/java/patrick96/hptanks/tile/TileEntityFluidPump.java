/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.tile;

import io.netty.buffer.ByteBuf;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraftforge.common.util.ForgeDirection;
import net.minecraftforge.fluids.*;
import patrick96.hptanks.core.fluids.FluidUtils;
import patrick96.hptanks.core.fluids.TankManager;
import patrick96.hptanks.core.network.packets.PacketTileUpdate;
import patrick96.hptanks.upgrades.IUpgradeable;
import patrick96.hptanks.upgrades.UpgradeManager;
import patrick96.hptanks.upgrades.UpgradeType;
import patrick96.hptanks.upgrades.types.Upgrade;
import patrick96.hptanks.util.Coords;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class TileEntityFluidPump extends TileEntityHPTBase implements IFluidTile, IUpgradeable {
    
    protected UpgradeManager upgradeManager = new UpgradeManager(this);
    
    private int defaultFluidPumpedPerTick = 250;
    
    private boolean scheduleUpdateConnections = true;
    
    private ArrayList<ForgeDirection> connections = new ArrayList<ForgeDirection>();
    
    /* Is this tile locked (Powered by Redstone) */
    private boolean locked = false;
    
    public TileEntityFluidPump() {
        tankManager = new TankManager(new FluidTank(getTankCapacity()));
    }

    @Override
    public void updateEntity() {
        super.updateEntity();

        if(!worldObj.isRemote) {
            updateLock();
            
            if(scheduleUpdateConnections) {
                scheduleUpdateConnections = false;
                updateConnections();
            }
            
            if(!locked) {
                pumpFluid();
                FluidUtils.ejectFluid(this, getFluidPumpedPerTick(), connections);
            }
        }
    }
    
    public int getFluidPumpedPerTick() {
        return FluidUtils.getFluidPumpedPerTick(this, defaultFluidPumpedPerTick);
    }
    
    @Override
    protected void init() {
        super.init();

        if(!worldObj.isRemote){
            updateConnections();
        }
    }
    
    private void pumpFluid() {
        int fluidPerTick = FluidUtils.getFluidPumpedPerTick(this, defaultFluidPumpedPerTick);
        Coords coords = getCoords().add(getRotation());
        TileEntity tile = coords.getTileEntity(worldObj);
        if(tile != null && tile instanceof IFluidHandler && !(tile instanceof TileEntityFluidPump)) {
            IFluidHandler fluidHandler = (IFluidHandler) tile;
            FluidStack drained = fluidHandler.drain(getRotation().getOpposite(), fluidPerTick, false);
            int filled = getTankManager().fill(getRotation(), drained, false);
            
            getTankManager().fill(getRotation(), fluidHandler.drain(getRotation().getOpposite(), filled, true), true);
        }
        else {
            if(FluidUtils.isWaterSource(coords, worldObj) && tick % 4 == 0) {
                ForgeDirection[] cardinalDirections = {ForgeDirection.NORTH, ForgeDirection.SOUTH, ForgeDirection.WEST, ForgeDirection.EAST};
                int waterCount = 0;
                for(ForgeDirection dir : cardinalDirections) {
                    if(waterCount >= 2) {
                        break;
                    }
                    
                    if(FluidUtils.isWaterSource(coords.add(dir), worldObj)) {
                        waterCount++;
                    }
                }
                
                if(waterCount >= 2) {
                    getTankManager().fill(getRotation(), new FluidStack(FluidRegistry.WATER, fluidPerTick * 4), true);
                }
            }
        }
    }
    
    /**
     * @return the connections
     */
    public ArrayList<ForgeDirection> getConnections() {
        return connections;
    }
    
    public boolean isLocked() {
        return locked;
    }
    
    public void setLocked(boolean locked) {
        if(locked != this.locked) {
            this.locked = locked;
            if(worldObj.isRemote) {
                getCoords().markBlockForRenderUpdate(worldObj);
            }
            markTileDirty();
        }
    }
    
    public void updateLock() {
        setLocked(worldObj.isBlockIndirectlyGettingPowered(xCoord, yCoord, zCoord));
    }
    
    public void updateConnections() {
        ArrayList<ForgeDirection> tempConnections = new ArrayList<ForgeDirection>();
        tempConnections.addAll(connections);

        connections.clear();
        for(ForgeDirection dir : ForgeDirection.VALID_DIRECTIONS) {
            Coords adjacent = getCoords().add(dir);
            TileEntity tile = adjacent.getTileEntity(worldObj);
            if(tile instanceof IFluidHandler && getRotation() != dir && !(tile instanceof TileEntityFluidPump)) {
                FluidTankInfo[] info = ((IFluidHandler) tile).getTankInfo(getRotation().getOpposite());
                if(info != null && info.length > 0) {
                    connections.add(dir);
                }
            }
        }
        
        if(worldObj.isRemote && !(tempConnections.size() == connections.size() && connections.containsAll(tempConnections))) {
            getCoords().markBlockForUpdate(worldObj);
        }
        
        markTileDirty();
    }
    
    
    /**
     * Schedules an updateConnections call, so that it is called one tick later
     */
    public void scheduleUpdateConnections() {
        scheduleUpdateConnections = true;
    }
    
    @Override
    public int fill(ForgeDirection from, FluidStack resource, boolean doFill) {
        return 0;
    }

    @Override
    public FluidStack drain(ForgeDirection from, FluidStack resource, boolean doDrain) {
        if(locked) {
            return null;
        }
        
        return getTankManager().drain(from, resource, doDrain);
    }

    @Override
    public FluidStack drain(ForgeDirection from, int maxDrain, boolean doDrain) {
        if(locked) {
            return null;
        }
        
        return getTankManager().drain(from, maxDrain, doDrain);
    }

    @Override
    public boolean canFill(ForgeDirection from, Fluid fluid) {
        return false;
    }

    @Override
    public boolean canDrain(ForgeDirection from, Fluid fluid) {
        return !locked;
    }

    @Override
    public FluidTankInfo[] getTankInfo(ForgeDirection from) {
        return getTankManager().getTankInfo(from);
    }
    
    @Override
    public void writeToNBT(NBTTagCompound compound) {
        super.writeToNBT(compound);
        upgradeManager.writeToNBT(compound);
    }
    
    @Override
    public void readFromNBT(NBTTagCompound compound) {
        super.readFromNBT(compound);
        upgradeManager.readFromNBT(compound);
    }
    
    @Override
    public boolean canDismantle() {
        return true;
    }
    
    @Override
    public boolean isRotatable() {
        return true;
    }

    @Override
    public void rotate() {
        super.rotate();
        updateConnections();
    }

    @Override
    public void handleUpdatePacket(PacketTileUpdate packet) throws IOException {
        int tempRotationIndex = rotationIndex;
        super.handleUpdatePacket(packet);
        setLocked(packet.input.readBoolean());
        upgradeManager.readPacketData(packet.input);
        if(tempRotationIndex != rotationIndex) {
            getCoords().notifyBlockChange(worldObj, getBlockType());
            getCoords().markBlockForUpdate(worldObj);
        }

        updateConnections();
    }

    @Override
    public void writePacketData(ByteBuf data) throws IOException {
        super.writePacketData(data);
        data.writeBoolean(locked);
        upgradeManager.writePacketData(data);
    }

    @Override
    public List<UpgradeType> getSupportedUpgrades() {
        return new ArrayList<UpgradeType>(){{
            add(UpgradeType.SPEED);
        }};
    }

    @Override
    public int applyUpgrade(Upgrade upgrade) {
        return upgradeManager.addUpgrade(upgrade);
    }

    @Override
    public UpgradeManager getUpgradeManager() {
        return upgradeManager;
    }
    
    @Override
    public int getComparatorOverride(int side) {
        return FluidUtils.calcRedstoneFromTank(getTankManager().getTank());
    }
}
