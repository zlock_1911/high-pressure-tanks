/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.upgrades;


public enum UpgradeType {
    // Machine Process Speed, for pump increase mB/t pumped
    SPEED("Speed"),
    // Increases energy storage, multiple are allowed, every upgrade adds the original amount of capacity.
    ENERGY_STORAGE("EnergyStorage"),
    // Industrialization Upgrades. Allows machines to produce IC2 things
    IC2("IC2"),
    // Fluid auto eject
    FLUID_AUTO_EJECT("FluidAutoEject"), // Compatible with SIDE
    // Items auto eject
    ITEM_AUTO_EJECT("ItemAutoEject"); // Compatible with SIDE
    /*// Basically increases the mB/t ejected/drained
    SPEED_FLUID_OUTPUT("SpeedFluidOutput");
    */
    
    
    
    
    /*
    // Allows to auto pump items out of adjacent fluid handlers.
    FLUID_AUTO_DRAW("FluidAutoDraw"), // Compatible with SIDE
    // Allows to auto draw items out of adjacent inventories.
    ITEM_AUTO_DRAW("ItemAutoDraw"), // Compatible with SIDE
    /**
     * Can be linked to upgrades to specifiy side(s) that should by affected by that upgrade
     * When assigned to no other upgrade it will limit the sides the block can accept things
     * Cannot be applied has to be linked in the GUI of another upgrade
     */
/*    SIDE("Side"),
    // Allows you to control machine by redstone (working or not working normally)
    REDSTONE_CONTROL("RedstoneControl"),
    
    // HPTank upgrade, adds a ruler to the control gui with which you can control the pressure and by that increase the storage capacity
    PRESSURE_CONTROL("PressureControl"),
    // Tank upgrade, adds a filter to the tank to filter fluids (blacklist/whitelist)
    FILTER_FLUID("FilterFluid");*/
    
    public static final UpgradeType[] UPGRADES = UpgradeType.values();
    
    /**
     * TextureName is also used as unlocalized name just all lower case;
     * This name is also used as an identifier of the upgrade just all lower case.
     */
    public final String textureName;
    
    UpgradeType(String name) {
        this.textureName = name;
    }
}
