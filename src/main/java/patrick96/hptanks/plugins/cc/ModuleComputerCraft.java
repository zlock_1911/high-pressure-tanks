/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */

package patrick96.hptanks.plugins.cc;

import cpw.mods.fml.common.Optional;
import cpw.mods.fml.common.registry.GameRegistry;
import dan200.computercraft.api.ComputerCraftAPI;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import patrick96.hptanks.init.ModBlocks;
import patrick96.hptanks.init.ModItems;
import patrick96.hptanks.lib.Reference;
import patrick96.hptanks.plugins.ICompatModule;

/**
 * TODO check all the CC Plugin Functionality
 * Can there be more than one peripheral on a block? Probably not, so no energy/upgrade/IGuiTile/base etc peripheral
 */
public class ModuleComputerCraft implements ICompatModule {

    @Override
    @Optional.Method(modid = Reference.PLUGIN_CC_DEP)
    public void preInit() {
        ComputerCraftAPI.registerPeripheralProvider(new HPTPeripheralProvider());
        //ComputerCraftAPI.registerTurtleUpgrade(new WrenchingTurtle());
    }

    @Override
    @Optional.Method(modid = Reference.PLUGIN_CC_DEP)
    public void init() {
        GameRegistry.addRecipe(new ItemStack(ModBlocks.tankCCperipheral), "WRW", "ACA", "WIW",
                'W', ModBlocks.tankCasing,
                'R', Items.ender_eye,
                'A', ModItems.wire.getStack(1, 0),
                'I', ModItems.circuitBoard.getStack(1, 0),
                'C', ModItems.ItemPlain.ReinforcedBars.getStack());
    }

    @Override
    @Optional.Method(modid = Reference.PLUGIN_CC_DEP)
    public void postInit() {}

    @Override
    public String getDependency() {
        return Reference.PLUGIN_CC_DEP;
    }

    @Override
    public String getCompatName() {
        return Reference.PLUGIN_CC_NAME;
    }
}
