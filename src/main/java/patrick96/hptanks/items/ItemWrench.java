/*
 * Copyright 2016 Patrick Ziegler
 *
 * This file is part of High Pressure Tanks.
 *
 * High Pressure Tanks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * High Pressure Tanks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with High Pressure Tanks.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author patrick96
 */
package patrick96.hptanks.items;

import buildcraft.api.tools.IToolWrench;
import cpw.mods.fml.common.Optional;
import cpw.mods.fml.common.Optional.Interface;
import cpw.mods.fml.common.Optional.Method;
import ic2.api.item.IBoxable;
import ic2.api.tile.IWrenchable;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import net.minecraftforge.common.util.ForgeDirection;
import patrick96.hptanks.lib.ItemInfo;
import patrick96.hptanks.tile.TileEntityHPTBase;
import patrick96.hptanks.util.Coords;
import patrick96.hptanks.util.Helper;
import patrick96.hptanks.util.Utils;

import java.util.List;

@Optional.InterfaceList({
        @Interface(iface = "ic2.api.item.IBoxable", modid = "IC2"),
        @Interface(iface = "buildcraft.api.tools.IToolWrench", modid = "BuildCraft|Core")
})
public class ItemWrench extends ItemHPT implements IToolWrench, IBoxable {
    
    public ItemWrench() {
        super(ItemInfo.WRENCH_UNLOCALIZED_NAME);
        setMaxStackSize(1);
        setTextureName(ItemInfo.WRENCH_TEXTURE);
    }
    
    @Override
    public boolean doesSneakBypassUse(World par2World, int x, int y, int z, EntityPlayer player) {
        return true;
    }
    
    @Override
    public boolean canWrench(EntityPlayer player, int x, int y, int z) {
        return true;
    }
    
    @Override
    public void wrenchUsed(EntityPlayer player, int x, int y, int z) {
        player.swingItem();
    }

    public boolean canBeStoredInToolbox(ItemStack itemstack) {
        return true;
    }
    
    @Override
    public boolean onItemUseFirst(ItemStack stack, EntityPlayer player, World world, int x, int y, int z, int side, float hitX, float hitY, float hitZ) {
        Coords coords = new Coords(x, y, z);
        TileEntity tile = coords.getTileEntity(world);
        
        if(!world.isRemote && player.isSneaking() && Helper.isIC2Loaded()) {
            dismantleIC2Block(tile, player);
        }

        if(!world.isRemote && !player.isSneaking() && coords.getBlock(world).rotateBlock(world, x, y, z, ForgeDirection.getOrientation(side))) {
            return true;
        }

        return super.onItemUseFirst(stack, player, world, x, y, z, side, hitX, hitY, hitZ);
    }

    @Method(modid="IC2")
    public void dismantleIC2Block(TileEntity tile, EntityPlayer player) {
        if(tile instanceof IWrenchable && !(tile instanceof TileEntityHPTBase)) {
            IWrenchable wrenchable = (IWrenchable) tile;
            if(wrenchable.wrenchCanRemove(player)) {
                ItemStack dropped = wrenchable.getWrenchDrop(player);
                List<ItemStack> list = tile.getBlockType().getDrops(tile.getWorldObj(), tile.xCoord, tile.yCoord, tile.zCoord, tile.getBlockMetadata(), 0);
                if(list.isEmpty()) {
                    list.add(dropped);
                }
                else {
                    list.set(0, dropped);
                }
                for(ItemStack stack : list) {
                    Utils.dropItemStack(tile.getWorldObj(), stack, tile.xCoord, tile.yCoord, tile.zCoord);
                }
                tile.getWorldObj().setBlockToAir(tile.xCoord, tile.yCoord, tile.zCoord);
            }
        }
    }
}
